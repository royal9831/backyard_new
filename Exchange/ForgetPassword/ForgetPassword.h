//
//  ForgetPassword.h
//  Exchange
//
//  Created by Debayan Ghosh on 28/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface ForgetPassword : UIViewController
{
    AppDelegate *appDel;
}
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UIView *resetPasswordView;
@property (weak, nonatomic) IBOutlet UITextField *passwordNewText;
@property (nonatomic,retain) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UITextField *confirmPassword;
@property (weak, nonatomic) IBOutlet UITextField *otpText;

@end
