//
//  ForgetPassword.m
//  Exchange
//
//  Created by Debayan Ghosh on 28/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "ForgetPassword.h"
#import "SCLAlertView.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
@interface ForgetPassword ()

@end

@implementation ForgetPassword

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    _emailText.returnKeyType = UIReturnKeyDone;
    _passwordNewText.returnKeyType = UIReturnKeyDone;
    _confirmPassword.returnKeyType = UIReturnKeyDone;
    _otpText.returnKeyType = UIReturnKeyDone;
    
    
    if([ [ UIScreen mainScreen ] bounds ].size.height==480)
    {
        _backgroundView.frame=CGRectMake(0, 0, 320, 568);
        
    }
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    _resetPasswordView.hidden=YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_emailText resignFirstResponder];
    [_passwordNewText resignFirstResponder];
    [_confirmPassword resignFirstResponder];
    [_otpText resignFirstResponder];
    
    
}

-(IBAction)backBtnAction:(id)sender
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}



- (IBAction)sendBtnAction:(id)sender {
    
    
    [_emailText resignFirstResponder];
    
    if([[self checkNull:_emailText.text] isEqualToString:@""])
    {
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Please provide a email id" closeButtonTitle:@"OK" duration:0.0f];
        
    }
    else if([self validateEmail:[_emailText text]] !=1)
    {
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Invalid email id" closeButtonTitle:@"OK" duration:0.0f];
    }
    else
    {
        
        [self forgetPasswordWebservice1];
        
    }
}


#pragma mark webservice

-(void)forgetPasswordWebservice1
{
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/ForgotPasword",appDel.hostUrl];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:                           _emailText.text,@"email",nil];
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        
        NSLog(@"JSON: %@", responseObject);
        
        if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
        {
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            alert.shouldDismissOnTapOutside = NO;
            alert.showAnimationType=SlideInToCenter;
            alert.hideAnimationType=SlideOutToCenter;
            [alert alertIsDismissed:^{
                NSLog(@"SCLAlertView dismissed!");
                
                _resetPasswordView.hidden=NO;
                
            }];
            
            [alert showInfo:self title:@"Forget Password" subTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]] closeButtonTitle:@"OK" duration:0.0f];
        }
        else
        {
            [appDel.alertview showInfo:self title:@"Forget Password" subTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]] closeButtonTitle:@"OK" duration:0.0f];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        NSLog(@"Error: %@", error);
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
        
        
    }];
    
    

}
- (IBAction)resetPasswordBtnAction:(id)sender {
    
    
    [_passwordNewText resignFirstResponder];
    [_confirmPassword resignFirstResponder];
    [_otpText resignFirstResponder];
    
    if([[self checkNull:_passwordNewText.text] isEqualToString:@""] || [[self checkNull:_confirmPassword.text] isEqualToString:@""] || [[self checkNull:_otpText.text] isEqualToString:@""])
    {
        
        [appDel.alertview showInfo:self title:@"Reset Password" subTitle:@"Please provide all the fields" closeButtonTitle:@"OK" duration:0.0f];
        
    }
    else if (![_passwordNewText.text isEqualToString:_confirmPassword.text])
    {
        [appDel.alertview showInfo:self title:@"Reset Password" subTitle:@"Password does not match" closeButtonTitle:@"OK" duration:0.0f];
    }
    else
    {
        [self forgetPasswordWebservice2];
    }
    
    
    
}

-(void)forgetPasswordWebservice2
{
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/ResetPassword",appDel.hostUrl];
    
//    pass, conPass, otp

    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:                           _passwordNewText.text,@"pass",_confirmPassword.text,@"conPass",_otpText.text,@"otp", nil];
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        
        NSLog(@"JSON: %@", responseObject);
        
        if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
        {
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            alert.shouldDismissOnTapOutside = NO;
            alert.showAnimationType=SlideInToCenter;
            alert.hideAnimationType=SlideOutToCenter;
            [alert alertIsDismissed:^{
                NSLog(@"SCLAlertView dismissed!");
                
                _resetPasswordView.hidden=YES;
                [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                
            }];
            
            [alert showInfo:self title:@"Password Reset" subTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]] closeButtonTitle:@"OK" duration:0.0f];
        }
        else
        {
            [appDel.alertview showInfo:self title:@"Password Reset" subTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]] closeButtonTitle:@"OK" duration:0.0f];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        NSLog(@"Error: %@", error);
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
        
        
    }];
    
    
    
}



#pragma mark Textfield delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField{
                                  
      CGRect viewFrame = self.view.frame;
      
      if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
      {
          viewFrame.origin.y = -20;
      }
      else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
      {
          viewFrame.origin.y = 0;
      }
      else
      {
          viewFrame.origin.y = -80;
      }
      
      [UIView beginAnimations:nil context:NULL];
      [UIView setAnimationBeginsFromCurrentState:YES];
      [UIView setAnimationDuration:.5];
      [self.view setFrame:viewFrame];
      [UIView commitAnimations];
  
}

                                  
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGRect viewFrame = self.view.frame;
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        viewFrame.origin.y = +0;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        viewFrame.origin.y = +0;
    }
    else
    {
        viewFrame.origin.y = +0;
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.5];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
                                  
                                  
-(NSString *)checkNull :(NSString *)str
{
    NSString *InputString=str;
    if( (InputString == nil) ||(InputString ==(NSString *)[NSNull null])||([InputString isEqual:nil])||([InputString length] == 0)||[allTrim( InputString ) length] == 0||([InputString isEqualToString:@""])||([InputString isEqualToString:@"(NULL)"])||([InputString isEqualToString:@"<NULL>"])||([InputString isEqualToString:@"<null>"]||([InputString isEqualToString:@"(null)"])||([InputString isEqualToString:@""]))
       
       )
        return @"";
    else
        return str ;
    
}
                                  
- (BOOL) validateEmail: (NSString *) candidate {
  NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
  NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
  //	return 0;
  return [emailTest evaluateWithObject:candidate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
