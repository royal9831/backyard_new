//
//  UIImage+RoundedImage.h
//  KINT
//
//  Created by Kalyan Bhunia on 06/11/14.
//  Copyright (c) 2014 BusinessProDesigns. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UIImage (RoundedImage)

+ (UIImage *)getRoundedRectImageFromImage :(UIImage *)image onReferenceView :(UIImageView*)imageView withCornerRadius :(float)cornerRadius;

@end
