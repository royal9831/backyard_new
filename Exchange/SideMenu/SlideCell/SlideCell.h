//
//  SlideCell.h
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideCell : UITableViewCell
@property(nonatomic,retain) IBOutlet UILabel *itemname;
@property(nonatomic,retain) IBOutlet UIImageView *logoImage;
@end
