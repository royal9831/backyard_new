//
//  SideMenu.m
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "SideMenu.h"
#import "SlideCell.h"
#import "UIImageView+WebCache.h"
#import "UIImage+RoundedImage.h"
#import "UploadProduct.h"
#import "ImageViewController.h"
@interface SideMenu ()

@end

@implementation SideMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifyTheUser) name:@"profilepicchange" object:nil];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults valueForKey:@"status"]!=nil && [[defaults valueForKey:@"status"] isEqualToString:@"1"])
    {
        items=[[NSMutableArray alloc]initWithObjects:@"PROFILE",@"SETTINGS",@"NOTIFICATION",@"MAP",@"PRODUCT UPLOAD",@"PRODUCT LISTING",@"REQUEST LISTING",@"LOGOUT", nil];
    }
    else
    {
        items=[[NSMutableArray alloc]initWithObjects:@"PROFILE",@"SETTINGS",@"NOTIFICATION",@"MAP",@"PRODUCT UPLOAD",@"PRODUCT LISTING",@"REQUEST LISTING", nil];
    }
    
    
    
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _nameLabel.text=[[defaults valueForKey:@"userdetails"] valueForKey:@"name"];
    _emailLabel.text=[[defaults valueForKey:@"userdetails"] valueForKey:@"email"];
    
    NSString *Imageurl=[[defaults valueForKey:@"userdetails"] valueForKey:@"profile_image"];//@"http://www.newyorker.com/wp-content/uploads/2012/07/chris-nolan.jpg";
    [_profileImageView sd_setImageWithURL:[NSURL URLWithString:Imageurl]
                        placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    
    
    _profileImageView.image = [UIImage getRoundedRectImageFromImage:_profileImageView.image onReferenceView:_profileImageView withCornerRadius:80/2];
    _profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    _profileImageView.layer.cornerRadius = 60/2;
    _profileImageView.layer.borderWidth = 0;
    _profileImageView.clipsToBounds = YES;
    
    
}

-(void)notifyTheUser{
//    _nameLabel.text=@"Debayan";
//    _emailLabel.text=@"debayan.ghosh@businessprodesigns.com";
//    NSString *Imageurl=@"http://www.newyorker.com/wp-content/uploads/2012/07/chris-nolan.jpg";
//    [_profileImageView sd_setImageWithURL:[NSURL URLWithString:Imageurl]
//                         placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _nameLabel.text=[[defaults valueForKey:@"userdetails"] valueForKey:@"name"];
    _emailLabel.text=[[defaults valueForKey:@"userdetails"] valueForKey:@"email"];
    
    NSString *Imageurl=[[defaults valueForKey:@"userdetails"] valueForKey:@"profile_image"];//@"http://www.newyorker.com/wp-content/uploads/2012/07/chris-nolan.jpg";
    [_profileImageView sd_setImageWithURL:[NSURL URLWithString:Imageurl]
                         placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [_table reloadData];
}


#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return items.count;
}


-(UITableViewCell *)tableView: (UITableView *)tableView cellForRowAtIndexPath: (NSIndexPath *)indexPath{
    
    
    NSLog(@"%@",[items objectAtIndex:indexPath.row]);
    
    static  NSString *CellIdentifier = @"SlideCell";
    SlideCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[SlideCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(indexPath.row==0)
    {
//        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 14, 14);
        cell.logoImage.image=[UIImage imageNamed:@"name-1.png"];
    }
    else if(indexPath.row==1)
    {
//        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 21, 21);
        cell.logoImage.image=[UIImage imageNamed:@"setting-icon-1.png"];
    }
    else if(indexPath.row==2)
    {
//        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 21, 21);
        cell.logoImage.image=[UIImage imageNamed:@"notification-icon.png"];
    }
    else if(indexPath.row==3)
    {
//        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 13, 20);
        cell.logoImage.image=[UIImage imageNamed:@"point-icon.png"];
    }
    else if(indexPath.row==4)
    {
//        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 19, 21);
        cell.logoImage.image=[UIImage imageNamed:@"upload-icon.png"];
    }
    else if(indexPath.row==5)
    {
//        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 13, 19);
        cell.logoImage.image=[UIImage imageNamed:@"Listing-icon.png"];
    }
    else if(indexPath.row==6)
    {
        //        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 19, 21);
        cell.logoImage.image=[UIImage imageNamed:@"requestlist1.png"];
    }
    else if(indexPath.row==7)
    {
//        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 19, 21);
        cell.logoImage.image=[UIImage imageNamed:@"logout-icon.png"];
    }
    
    
    UIView *customColorView = [[UIView alloc] init];
    customColorView.backgroundColor = [UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1.0];
    cell.selectedBackgroundView =  customColorView;
    cell.itemname.highlightedTextColor = [UIColor colorWithRed:169/255.0 green:17/255.0 blue:34/255.0 alpha:1.0];
    
    
    if(indexPath.row==0)
    {
//        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 14, 14);
        cell.logoImage.highlightedImage=[UIImage imageNamed:@"profile-icon.png"];
    }
    else if(indexPath.row==1)
    {
        //        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 21, 21);
        cell.logoImage.highlightedImage=[UIImage imageNamed:@"setting-icon-2.png"];
    }
    else if(indexPath.row==2)
    {
        //        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 21, 21);
        cell.logoImage.highlightedImage=[UIImage imageNamed:@"notification-icon-1.png"];
    }
    else if(indexPath.row==3)
    {
        //        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 13, 20);
        cell.logoImage.highlightedImage=[UIImage imageNamed:@"point-icon-2.png"];
    }
    else if(indexPath.row==4)
    {
        //        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 19, 21);
        cell.logoImage.highlightedImage=[UIImage imageNamed:@"upload-icon-1.png"];
    }
    else if(indexPath.row==5)
    {
        //        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 13, 19);
        cell.logoImage.highlightedImage=[UIImage imageNamed:@"Listing-icon-1.png"];
    }
    else if(indexPath.row==6)
    {
        //        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 19, 21);
        cell.logoImage.highlightedImage=[UIImage imageNamed:@"requestlist2.png"];
    }
    else if(indexPath.row==7)
    {
        //        cell.logoImage.frame=CGRectMake(cell.logoImage.frame.origin.x, cell.logoImage.frame.origin.y, 19, 21);
        cell.logoImage.highlightedImage=[UIImage imageNamed:@"logout-icon-1.png"];
    }
    
    
    cell.itemname.text=[items objectAtIndex:indexPath.row];
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults valueForKey:@"status"]!=nil && [[defaults valueForKey:@"status"] isEqualToString:@"1"])
    {
    
        if(indexPath.row == 0)
        {
            [self performSegueWithIdentifier:@"profilereveal" sender:self];
        }
        else if (indexPath.row==1)
        {
            
            [self performSegueWithIdentifier:@"settingrevealpush" sender:self];
        }
        else if(indexPath.row == 2)
        {
            [self performSegueWithIdentifier:@"notificationreveal" sender:self];
        }
        else if (indexPath.row == 3)
        {
            [self performSegueWithIdentifier:@"mapreveal" sender:self];
        }
        else if (indexPath.row == 4)
        {
            [self performSegueWithIdentifier:@"productuploadrevealpush" sender:self];
        }
        else if (indexPath.row == 5)
        {
            [self performSegueWithIdentifier:@"productlistreveal" sender:self];
        }
        else if (indexPath.row == 6)
        {
            [self performSegueWithIdentifier:@"requestlistreveal" sender:self]; 
        }
        else if (indexPath.row == [items count]-1)
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:@"0" forKey:@"status"];
            
            [defaults setObject:nil forKey:@"userdetails"];
            [defaults setObject:nil forKey:@"usersettings"];
            [defaults setObject:nil forKey:@"backyard"];
            
            
            [defaults synchronize];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else
        {
            
            
            
            
            
            
            [appDel.alertview showInfo:self.parentViewController title:@"INFO" subTitle:@"UNDER DEVELOPMENT" closeButtonTitle:@"OK" duration:0.0f];
        }
    }
    else
    {
        if (indexPath.row == 3)
        {
            [self performSegueWithIdentifier:@"mapreveal" sender:self];
        }
        else
        {
            
            
            
            UIAlertView *alv=[[UIAlertView alloc]initWithTitle:@"Backyard" message:@"Please login before proceeding" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alv show];
            
            
            
//            SCLAlertView *alert = [[SCLAlertView alloc] init];
////            alert.shouldDismissOnTapOutside = YES;
////            alert.showAnimationType=SlideInToCenter;
////            alert.hideAnimationType=SlideOutToCenter;
//            [alert alertIsDismissed:^{
//                NSLog(@"SCLAlertView dismissed!");
//                
//                
//                [self.navigationController popToRootViewControllerAnimated:YES];
//                
//                
//            }];
//            
//            [alert showInfo:self.parentViewController title:@"Backyard" subTitle:@"You have to login first" closeButtonTitle:@"OK" duration:0.0f];
            
            
            
            
            
        }
        
        
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}



-(IBAction)viewProfileImageBtnAction:(id)sender
{
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    //    [self.navigationController popToViewController:controller animated:YES];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *Imageurl=[[defaults valueForKey:@"userdetails"] valueForKey:@"profile_image"];
    controller.imageUrl=Imageurl;
    [self.navigationController pushViewController:controller animated:NO];
}



//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if([segue.identifier isEqualToString:@"productuploadrevealpush"]){
//        
//        UploadProduct *vc = (UploadProduct*)[segue destinationViewController];
//        vc.fromWhaereTag=1;
//        
//        
//    }
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
