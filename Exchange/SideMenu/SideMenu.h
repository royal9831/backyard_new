//
//  SideMenu.h
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface SideMenu : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    NSMutableArray *items;
    AppDelegate *appDel;
}
@property(nonatomic,retain)IBOutlet UITableView *table;
@property(nonatomic,retain)IBOutlet UILabel *nameLabel,*emailLabel;
@property(nonatomic,retain)IBOutlet UIImageView *profileImageView;
@end
