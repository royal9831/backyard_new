//
//  Login.h
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface Login : UIViewController
{
    int navigationTag;
    AppDelegate *appDel;
    int revealTag;
}
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (nonatomic,retain) IBOutlet UIView *backgroundView;
@end
