//
//  Login.m
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "Login.h"
#import "SCLAlertView.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "Dashboard.h"
#import "Map.h"
#import "SWRevealViewController.h"
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 200;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 100;


@interface Login ()

@end

@implementation Login

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _emailTextField.returnKeyType = UIReturnKeyDone;
    _passwordTextField.returnKeyType = UIReturnKeyDone;
    
    
    if([ [ UIScreen mainScreen ] bounds ].size.height==480)
    {
        _backgroundView.frame=CGRectMake(0, 0, 320, 568);
                                                               
    }
    
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    [_emailTextField setValue:[UIColor colorWithRed:58/255.0 green:61/255.0 blue:70/255.0 alpha:1.0]
                    forKeyPath:@"_placeholderLabel.textColor"];
    [_passwordTextField setValue:[UIColor colorWithRed:58/255.0 green:61/255.0 blue:70/255.0 alpha:1.0]
                    forKeyPath:@"_placeholderLabel.textColor"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults valueForKey:@"status"]!=nil && [[defaults valueForKey:@"status"] isEqualToString:@"1"])
    {
//        [self performSegueWithIdentifier:@"reveal" sender:self];
        revealTag=1;
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"reveal"];
        [self.navigationController pushViewController:controller animated:NO];
    }
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_emailTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];

    
    
}



-(IBAction)loginBtnAction:(id)sender
{
    [_emailTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    
    if([[self checkNull:_emailTextField.text] isEqualToString:@""] || [[self checkNull:_passwordTextField.text] isEqualToString:@""])
    {
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Please provide email id and password" closeButtonTitle:@"OK" duration:0.0f];

    }
    else if([self validateEmail:[_emailTextField text]] !=1)
    {
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Invalid email id" closeButtonTitle:@"OK" duration:0.0f];
    }
    else
    {
        
        [self loginWebservice];
        
    }
    
}


-(IBAction)forgetPasswordBtnAction:(id)sender
{
    
    [self performSegueWithIdentifier:@"forgetpassmodalview" sender:self];
}

-(IBAction)skipBtnAction:(id)sender
{
    
    [_emailTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    
    revealTag=2;
    [self performSegueWithIdentifier:@"reveal" sender:self];
}


#pragma mark Loginservice

-(void)loginWebservice
{
    
      
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/Login",appDel.hostUrl];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:                           _emailTextField.text,@"UserLogin[username]",_passwordTextField.text,@"UserLogin[password]",appDel.token,@"Profile[device_token]",@"IOS",@"Profile[token_type]",
                           nil];
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        
        NSLog(@"JSON: %@", responseObject);
        
        if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"response"]] isEqualToString:@"1"])
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[responseObject valueForKey:@"details"] forKey:@"userdetails"];
            [defaults setObject:[responseObject valueForKey:@"settings"] forKey:@"usersettings"];
            [defaults setObject:[responseObject valueForKey:@"backyard"] forKey:@"backyard"];
            [defaults setObject:@"1" forKey:@"status"];
            [defaults synchronize];
            
            revealTag=1;
            
            [self performSegueWithIdentifier:@"reveal" sender:self];
        }
        else
        {
            [appDel.alertview showInfo:self title:@"Login" subTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]] closeButtonTitle:@"OK" duration:0.0f];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        NSLog(@"Error: %@", error);
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
        
        
    }];
    
    
    
    
    
    
    
}


-(IBAction)registerBtnAction:(id)sender
{
    [_emailTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    
    [self performSegueWithIdentifier:@"registerpush" sender:self];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"reveal"])
    {
        
        SWRevealViewController *vc = (SWRevealViewController*)[segue destinationViewController];
        vc.revealTag=revealTag;
           
    }
}

#pragma mark Textfield delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    
    
//    currTextField = textField;
//    [textField setInputAccessoryView:keyboardToolbar];
//    
//    CGRect textFieldRect = [self.view.superview.window convertRect:textField.bounds fromView:textField];
//    CGRect viewRect = [self.view.superview.window convertRect:self.view.superview.bounds fromView:self.view.superview];
//    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
//    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
//    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
//    CGFloat heightFraction = numerator / denominator;
//    if (heightFraction < 0.0)
//    {
//        heightFraction = 0.0;
//    }
//    else if (heightFraction > 1.0)
//    {
//        heightFraction = 1.0;
//    }
//    UIInterfaceOrientation orientation =
//    [[UIApplication sharedApplication] statusBarOrientation];
//    if (orientation == UIInterfaceOrientationPortrait ||
//        orientation == UIInterfaceOrientationPortraitUpsideDown)
//    {
//        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
//    }
//    else
//    {
//        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
//    }
    
    
    
    
    
    
    
    CGRect viewFrame = self.view.frame;
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        viewFrame.origin.y = -80;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        viewFrame.origin.y = -90;
    }
    else
    {
        viewFrame.origin.y = -80;
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.5];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGRect viewFrame = self.view.frame;
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        viewFrame.origin.y = +0;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        viewFrame.origin.y = +0;
    }
    else
    {
        viewFrame.origin.y = +0;
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.5];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


-(NSString *)checkNull :(NSString *)str
{
    NSString *InputString=str;
    if( (InputString == nil) ||(InputString ==(NSString *)[NSNull null])||([InputString isEqual:nil])||([InputString length] == 0)||[allTrim( InputString ) length] == 0||([InputString isEqualToString:@""])||([InputString isEqualToString:@"(NULL)"])||([InputString isEqualToString:@"<NULL>"])||([InputString isEqualToString:@"<null>"]||([InputString isEqualToString:@"(null)"])||([InputString isEqualToString:@""]))
       
       )
        return @"";
    else
        return str ;
    
}

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    //	return 0;
    return [emailTest evaluateWithObject:candidate];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
