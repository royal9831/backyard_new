//
//  MapRoute.h
//  Exchange
//
//  Created by Debayan Ghosh on 24/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
//#import "GooglePolylineRequest.h"
#import "LRouteController.h"
@interface MapRoute : UIViewController<GMSMapViewDelegate>
{
    float lat,lon;
    
    
    NSMutableArray *_coordinates;
    __weak GMSMapView *_mapView;
    LRouteController *_routeController;
    GMSPolyline *_polyline;
    GMSMarker *_markerStart;
    GMSMarker *_markerFinish;
    
}
@property  float productLat,productLon;
@property (nonatomic,retain) NSString *locationTitle, *subtitle;
@property (retain,nonatomic) CLLocationManager *locationManager;
@end
