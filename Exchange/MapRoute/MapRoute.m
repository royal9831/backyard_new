//
//  MapRoute.m
//  Exchange
//
//  Created by Debayan Ghosh on 24/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "MapRoute.h"
#import "TopNavigation.h"


UIButton *closeBtn;
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))


#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
@interface MapRoute ()<UIViewControllerTransitioningDelegate,ProfileViewPageDetegale,GMSMapViewDelegate>
{
    GMSMapView *mapView_;
}
@end

@implementation MapRoute
@synthesize productLat,productLon,locationTitle,subtitle,locationManager;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    
    
    
    _coordinates = [[NSMutableArray alloc]init];
    _routeController = [[LRouteController alloc]init];
    
    
    
    mapView_ = [[GMSMapView alloc]initWithFrame:CGRectMake(0.0, 64, self.view.frame.size.width, self.view.frame.size.height-64)];
    
    mapView_.delegate=self;
    mapView_.settings.myLocationButton = NO;
    mapView_.myLocationEnabled = YES;
    [self.view addSubview:mapView_];
    
    
    
    closeBtn=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-25, mapView_.frame.size.height-50-30, 50, 50)];
    closeBtn.backgroundColor=[UIColor clearColor];
    [closeBtn setBackgroundImage:[UIImage imageNamed:@"MapClose"] forState:UIControlStateNormal];
    [mapView_ addSubview:closeBtn];
    [closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:UIControlEventAllTouchEvents];

    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self mapShow];
    });
  
    
    
    
    
    
    
    
    
    
    
    
//    lat = [@"22.365487" floatValue];
//    lon = [@"88.125642" floatValue];
//    
//    
//    
////    CGRectMake(0.0, 64, self.view.frame.size.width, self.view.frame.size.height-64)
//    
//    CGRect frame=CGRectMake(0.0, 64, self.view.frame.size.width, self.view.frame.size.height-64);
//    
//    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat longitude:lon zoom:12];
//    mapView_ = [GMSMapView mapWithFrame:frame camera:camera];
//    mapView_.myLocationEnabled = YES;
//    mapView_.delegate = self;
//    [self.view addSubview:_mapView];
//    
//    
//    
//    
//    
//    
//    
//    _markerStart = [GMSMarker new];
//    _markerStart.title = @"Start";
//    
//    _markerFinish = [GMSMarker new];
//    _markerFinish.title = @"Finish";
//    
//    
//    
//    _polyline.map = nil;
//    _markerStart.map = nil;
//    _markerFinish.map = nil;
//    
//    
//    
//    
//    
//    
//    
//    
////    GMSMarker *marker = [[GMSMarker alloc] init];
////    marker.position=CLLocationCoordinate2DMake(lat, lon);
////    marker.title = @"Current Location";
////    marker.snippet = @"";
////    marker.icon=[UIImage imageNamed:@"point-12.png"];
////    marker.map = mapView_;
////    
////    
////    GMSMarker *marker1 = [[GMSMarker alloc] init];
////    marker1.position=CLLocationCoordinate2DMake(productLat, productLon);
////    marker1.title = locationTitle;
////    marker1.snippet = subtitle;
////    marker1.icon=[UIImage imageNamed:@"point-12.png"];
////    marker1.map = mapView_;
//    
//    
////    [_coordinates addObject:[[CLLocation alloc] initWithLatitude:22.573069 longitude:88.358907]];
//    
//    
//    [_coordinates addObject:[[CLLocation alloc] initWithLatitude:lat longitude:lon]];
//    [_coordinates addObject:[[CLLocation alloc] initWithLatitude:productLat longitude:productLon]];
//    
//    
//    
//    NSLog(@"_coordinates %@",_coordinates);
//    
//    if ([_coordinates count] > 1)
//    {
//        
//        [_routeController getPolylineWithLocations:_coordinates travelMode:TravelModeDriving andCompletitionBlock:^(GMSPolyline *polyline, NSError *error) {
//            if (error)
//            {
//                NSLog(@"%@", error);
//            }
//            else if (!polyline)
//            {
//                NSLog(@"No route");
//                [_coordinates removeAllObjects];
//            }
//            else
//            {
//                _markerStart.position = [[_coordinates objectAtIndex:0] coordinate];
//                _markerStart.map = mapView_;
//                
//                _markerFinish.position = [[_coordinates lastObject] coordinate];
//                _markerFinish.map = mapView_;
//                
//                _polyline = polyline;
//                _polyline.strokeWidth = 5;
//                _polyline.strokeColor = [UIColor blueColor];
//                _polyline.map = mapView_;
//            }
//        }];
//        
//    }
//
    
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    _polyline.map = nil;
    _markerStart.map = nil;
    _markerFinish.map = nil;
    _mapView=nil;
    _mapView.delegate=nil;
}

-(IBAction)closeBtnAction:(id)sender
{
//    [self dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)mapShow
{
    self.locationManager = [[CLLocationManager alloc] init];
    
    self.locationManager.delegate = self;
    if(IS_OS_8_OR_LATER){
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                [self.locationManager requestAlwaysAuthorization];
            } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                [self.locationManager  requestWhenInUseAuthorization];
            } else {
                NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
            }
        }
    }
    [self.locationManager startUpdatingLocation];
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"maproutenav"]){
        
        TopNavigation *vc = (TopNavigation*)[segue destinationViewController];
        vc.titleString=@"SEARCH MAP VIEW";
        vc.fromWhere=1;
        vc.delegate = self;
    }
}










#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    
    
    
    
    
    lon = [@"88.125642" floatValue];
    lat = [@"22.365487" floatValue];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                            longitude:lon
                                                                 zoom:12];
    mapView_.camera=camera;
    //plotting the google map
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position=CLLocationCoordinate2DMake(lat, lon);
    marker.title = @"Current Location";
    marker.snippet = @"";
    marker.icon=[UIImage imageNamed:@"point-12.png"];
    marker.map = mapView_;

    
    [self routeDraw];
    
    
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        lon = [[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] floatValue];
        lat = [[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] floatValue];
        
        
        [locationManager stopUpdatingLocation];
        
        
        
        
        
        
        
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                                longitude:lon
                                                                     zoom:12];
        mapView_.camera=camera;
        //plotting the google map
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position=CLLocationCoordinate2DMake(lat, lon);
        marker.title = @"Current Location";
        marker.snippet = @"";
        marker.icon=[UIImage imageNamed:@"point-12.png"];
        marker.map = mapView_;
        
        
        
        
        
        
        [self routeDraw];
        
        
        
    }
    
    
    
    
    
    
}


-(void)routeDraw
{
    
    
//    GMSCameraPosition *camera1 = [GMSCameraPosition cameraWithLatitude:productLat
//                                                             longitude:productLon
//                                                                  zoom:15];
//    mapView_.camera=camera1;
    
    GMSMarker *marker1 = [[GMSMarker alloc] init];
    marker1.position=CLLocationCoordinate2DMake(productLat, productLon);
    marker1.title = locationTitle;
    marker1.snippet = subtitle;
    marker1.icon=[UIImage imageNamed:@"point-12.png"];
    marker1.map = mapView_;
    
    
    
    _coordinates = [[NSMutableArray alloc]init];
    _routeController = [[LRouteController alloc]init];

    
    
    
    [_coordinates addObject:[[CLLocation alloc] initWithLatitude:lat longitude:lon]];
    [_coordinates addObject:[[CLLocation alloc] initWithLatitude:productLat longitude:productLon]];
    
    NSLog(@"_coordinates %@",_coordinates);
    
    if ([_coordinates count] > 1)
    {
        
        [_routeController getPolylineWithLocations:_coordinates travelMode:TravelModeDriving andCompletitionBlock:^(GMSPolyline *polyline, NSError *error) {
            if (error)
            {
                NSLog(@"%@", error);
            }
            else if (!polyline)
            {
                NSLog(@"No route");
                [_coordinates removeAllObjects];
            }
            else
            {
                _markerStart.position = [[_coordinates objectAtIndex:0] coordinate];
                _markerStart.map = mapView_;
                
                _markerFinish.position = [[_coordinates lastObject] coordinate];
                _markerFinish.map = mapView_;
                
                _polyline = polyline;
                _polyline.strokeWidth = 5;
                _polyline.strokeColor = [UIColor blueColor];
                _polyline.map = mapView_;
            }
        }];
        
    }

}


- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
    
    UIView *newview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 150, 80)];
    newview.backgroundColor=[UIColor clearColor];
    
    UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 150, 80)];
    imgView.image=[UIImage imageNamed:@"MarkerView.png"];
    [newview addSubview:imgView];
    
    
    
    UILabel *usernameLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, 150, 20)];
    UILabel *productLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, 150, 30)];
    productLabel.numberOfLines=5;
    
    [self customizeLabel:usernameLabel:1];
    [self customizeLabel:productLabel:2];
    
    usernameLabel.text=marker.title;
    productLabel.text=marker.snippet;
    
    [newview addSubview:usernameLabel];
    [newview addSubview:productLabel];
    
    
    //    [ma addSubview:newview];
    
    return newview;
}



- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    
    
    
    return NO;
}


-(void)customizeLabel:(UILabel *)customLabel :(int)type
{
    if(type==1)
    {
        customLabel.font=[UIFont fontWithName:@"DINCondensed-Bold" size:15.0];
        [customLabel setTextColor:[UIColor blackColor]];
    }
    else
    {
        customLabel.font=[UIFont fontWithName:@"DINCondensed-Bold" size:13.0];
        [customLabel setTextColor:[UIColor darkGrayColor]];
    }
    
    
    [customLabel setTextAlignment:NSTextAlignmentCenter];
    customLabel.backgroundColor=[UIColor clearColor];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
