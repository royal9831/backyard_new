//
//  MapSearch.h
//  Exchange
//
//  Created by Debayan Ghosh on 19/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
//#import <GoogleMapsM4B/GoogleMaps.h>
#import "GooglePolylineRequest.h"
#import "LRouteController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "AppDelegate.h"
#import "ALPickerView.h"
@interface MapSearch : UIViewController<GooglePolylineRequestDelegate,GMSMapViewDelegate,UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIPickerViewDataSource, UIPickerViewDelegate,UIPickerViewDelegate,ALPickerViewDelegate>
{
    float lat,lon,productLat,productLon;
    NSString *locationTitle, *subtitle, *phoneNumber, *emailAddress;
    
    
    AppDelegate *appDel;
    NSMutableArray *_coordinates;
    __weak GMSMapView *_mapView;
    LRouteController *_routeController;
    GMSPolyline *_polyline;
    GMSMarker *_markerStart;
    GMSMarker *_markerFinish;
    
    int directionBtnTag;
    
    
    
    NSMutableArray *productArray;
    
    
    NSMutableArray *entries,*catIds;
    NSString *catIdString;
    NSMutableDictionary *selectionStates;
    ALPickerView *pickerView;
    NSMutableArray *finalList,*finalIdList;
    NSString *currLat, *currLon;
    
}
@property (nonatomic,retain) IBOutlet UIView *searchView;
@property (nonatomic,retain) IBOutlet UIButton *choiceBtn1,*choiceBtn2,*choiceBtn3;
@property (nonatomic,retain) IBOutlet UIImageView *choiceImg1,*choiceImg2,*choiceImg3;
@property (nonatomic,retain)IBOutlet UITextField *mapSearchText,*categoryTextField;
@property (nonatomic,retain) IBOutlet UICollectionView *collection;
@property (nonatomic,retain) IBOutlet UITableView *table;
@property (nonatomic,retain) IBOutlet UIView *backgroundView, *detailsView, *listView;
@property (nonatomic,retain) NSMutableArray *productList;
@property (retain,nonatomic) CLLocationManager *locationManager;
@property (nonatomic,retain) IBOutlet UILabel *label1,*label2,*label3,*Label4,*label5,*label6;
@property (nonatomic,retain) IBOutlet UIImageView *profileImageView;
@property (nonatomic,retain) IBOutlet UIButton *topBtn1,*topBtn2;
@property (nonatomic,retain) IBOutlet UIButton *callBtn, *emailBtn;

@end
