//
//  MapSearchListCell.h
//  Exchange
//
//  Created by Debayan Ghosh on 25/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapSearchListCell : UITableViewCell
@property (nonatomic,retain) IBOutlet UILabel *label1,*label2,*label3,*Label4,*label5,*label6;
@property (nonatomic,retain) IBOutlet UILabel *titlelabel1,*titlelabel2,*titlelabel3,*titleLabel4,*titlelabel5,*titlelabel6;
@property (nonatomic,retain) IBOutlet UILabel *separatorlabel1,*separatorlabel2,*separatorlabel3,*separatorLabel4,*separatorlabel5,*separatorlabel6;
@property (nonatomic,retain) IBOutlet UIImageView *profileImageView, *backgroundImageView,*cellSeparator;
@property (nonatomic,retain) IBOutlet UIButton *callBtn,*emailBtn,*profileImageBtn;
@end
