//
//  MapSearchListCell.m
//  Exchange
//
//  Created by Debayan Ghosh on 25/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "MapSearchListCell.h"

@implementation MapSearchListCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
