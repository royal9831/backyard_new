//
//  MapSearch.m
//  Exchange
//
//  Created by Debayan Ghosh on 19/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

/*
 
 
 searchList": [
 {
 "product_id": "35",
 "user_id": "18",
 "product_name": "product1",
 "product_type": "1",
 "product_desc": "Ghjjhjjkkk",
 "date_added": "2015-08-14 05:08:32",
 "owner": {
 "user_id": "18",
 "name": "santu5012",
 "phone_no": "1234000000122",
 "zip": "",
 "lat": "23.007820",
 "long": "88.542870",
 "address": "kolkata.india",
 "profile_image": "http://192.168.0.233/Backyard/php/images/profile/892eb170348177f8aa2661260abeb564-avatar.png"
 }
 },
 
 */



#import "MapSearch.h"
#import "TopNavigation.h"
#import<GoogleMaps/GoogleMaps.h>
//#import <GoogleMapsM4B/GoogleMaps.h>
#import "MapClass.h"
#import "SCLAlertView.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "UIImage+RoundedImage.h"
#import "CategoryObject.h"
#import "MapSearchClass.h"
#import "MapSearchClass.h"
#import "MapSearchListCell.h"
#import "ProductInfo.h"
#import "GoogleRoute.h"
#import "RouteAnnotation.h"
#import "MapRoute.h"
#import "ProductCollectionCell.h"
#import "ImageViewController.h"

UIButton *closeBtn;
UIStoryboardSegue *topSegue;

#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))


#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.-/:;()&@!#$%^*+?\|\",<>[]{}=~`£€•"

@interface MapSearch ()<UIViewControllerTransitioningDelegate,ProfileViewPageDetegale,GMSMapViewDelegate>
{
    GMSMapView *mapView_;
}
@end

@implementation MapSearch
@synthesize productList,locationManager;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view.
    
    //// Map.h////
    
    appDel.allPickerTag=1;
    
//    catIdString=@"";
    catIdString=@"1,2,3,4";
    _categoryTextField.text=@"All";
    entries=[[NSMutableArray alloc]init];
    catIds=[[NSMutableArray alloc]init];
    
    
    _choiceBtn1.selected=YES;
    _choiceBtn2.selected=YES;
    _choiceBtn3.selected=YES;
    
    
    _choiceImg1.image=[UIImage imageNamed:@"redcheck2.png"];
    _choiceImg2.image=[UIImage imageNamed:@"redcheck2.png"];
    _choiceImg3.image=[UIImage imageNamed:@"redcheck2.png"];
    
    
    _searchView.hidden=YES;
    
    
    ////Map.h////
    
    
    _callBtn.hidden=YES;
    _emailBtn.hidden=YES;
    productArray=[[NSMutableArray alloc]init];
    
    
    
    directionBtnTag=0;
    
    _topBtn1.selected=NO;
    _topBtn2.selected=YES;
    _topBtn1.userInteractionEnabled=NO;
    _topBtn2.userInteractionEnabled=YES;
    _listView.hidden=YES;
    
    
    
    
    if([ [ UIScreen mainScreen ] bounds ].size.height==480)
    {
        _backgroundView.frame=CGRectMake(0, 0, 320, 568);
        _table.frame=CGRectMake(0, 0, 320, 480-64);
        
    }
    
    
    
    NSLog(@"ARRAY %@",productList);
    
    
    
    mapView_ = [[GMSMapView alloc]initWithFrame:CGRectMake(0.0, 64, self.view.frame.size.width, _backgroundView.frame.size.height-64)];
    
    mapView_.delegate=self;
    mapView_.settings.myLocationButton = NO;
    mapView_.myLocationEnabled = YES;
    [_backgroundView addSubview:mapView_];
    
    
    
    
    [_backgroundView addSubview:_detailsView];
    [_backgroundView addSubview:_listView];
    
    [_backgroundView bringSubviewToFront:_searchView];
    
    
    
    
    
    [_detailsView setFrame:CGRectMake(_detailsView.frame.origin.x, self.view.frame.size.height, _detailsView.frame.size.width, _detailsView.frame.size.height)];
    
    
    
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self categoryWebservice];
        
    });
    
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self mapShow];
    });
    
    
    
    
    
    
    
}







-(void)categoryWebservice
{
    
    
    
    //    ProductDetails[product_name], ProductDetails[user_id], ProductDetails[product_type], ProductDetails[product_desc], cat_id,ProductDetails[quantity]
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/Category",appDel.hostUrl];
    
    //    NSLog(@"ID %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"]);
    
    //    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],@"ProductDetails[user_id]",_nameTextField.text, @"ProductDetails[product_name]",_productDescriptionText.text,@"ProductDetails[product_desc]",@"1",@"ProductDetails[product_type]",catIdString,@"cat_id" ,nil];
    //
    //    [manager POST:url parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:                           _emailTextField.text,@"UserLogin[username]",_passwordTextField.text,@"UserLogin[password]",
    //                           nil];
    
    [manager POST:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     
     {
         
         
         //        NSData * imgData;
         //
         //        imgData = UIImagePNGRepresentation(_circleImageView.image);
         //
         //        if(imgData!=NULL){
         //            [formData appendPartWithFileData:imgData name:@"profileImage" fileName:@"avatar.png" mimeType:@"image/png"];
         //        }
         //
         //
         //    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
         
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         
         NSLog(@"JSON: %@", responseObject);
         
         if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
         {
             
             
             entries=[[NSMutableArray alloc]init];
             catIds=[[NSMutableArray alloc]init];
             
             
             
             
             for(int i=0;i<[[responseObject valueForKey:@"category"] count];i++)
             {
                 
                 
                 [catIds addObject:[[[responseObject valueForKey:@"category"] objectAtIndex:i] valueForKey:@"cat_id"]];
                 [entries addObject:[[[responseObject valueForKey:@"category"] objectAtIndex:i] valueForKey:@"cat_name"]];
                 
                 
             }
             
             
             selectionStates = [[NSMutableDictionary alloc] init];
             for (NSString *key in entries)
             {
                 [selectionStates setObject:[NSNumber numberWithBool:YES] forKey:key];//NO
             }
             
             //            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             //            [defaults setObject:[responseObject valueForKey:@"profile"] forKey:@"userdetails"];
             //            [defaults synchronize];
             //
             //            [[NSNotificationCenter defaultCenter] postNotificationName:@"profilepicchange" object:nil userInfo:nil];
             //
             //            [appDel.alertview showInfo:self title:@"Profile Update" subTitle:@"Profile updated successfully" closeButtonTitle:@"OK" duration:0.0f];
         }
         else
         {
             //            [appDel.alertview showInfo:self title:@"Profile Update" subTitle:[responseObject valueForKey:@"message"] closeButtonTitle:@"OK" duration:0.0f];
         }
         
         [self setPickerView];
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         NSLog(@"Error: %@", error);
         
         [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
         
         
     }];
    
}

-(void)setPickerView
{
    
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        pickerView = [[ALPickerView alloc] initWithFrame:CGRectMake(0, 68, 375.0, 0)];
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        pickerView = [[ALPickerView alloc] initWithFrame:CGRectMake(0, 68, 414, 0)];
    }
    else
    {
        pickerView = [[ALPickerView alloc] initWithFrame:CGRectMake(0, 68, 320, 0)];
    }
    //    pickerView = [[ALPickerView alloc] initWithFrame:CGRectMake(0, 68, 0, 0)];
    pickerView.delegate = self;
    _categoryTextField.inputView = pickerView;
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleBlackOpaque;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTouched1:)];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTouched:)];
    [toolBar setItems:[NSArray arrayWithObjects:cancelButton, [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneButton, nil]];
    _categoryTextField.inputAccessoryView = toolBar;
    
    
    
    
    [pickerView reloadAllComponents];
    
    
}


#pragma mark pickerDelegates

- (NSInteger)numberOfRowsForPickerView:(ALPickerView *)pickerView {
    return [entries count];
}

- (NSString *)pickerView:(ALPickerView *)pickerView textForRow:(NSInteger)row {
    return [entries objectAtIndex:row] ;
}

- (BOOL)pickerView:(ALPickerView *)pickerView selectionStateForRow:(NSInteger)row {
    return [[selectionStates objectForKey:[entries objectAtIndex:row]] boolValue];
}

- (void)pickerView:(ALPickerView *)pickerView didCheckRow:(NSInteger)row {
    // Check whether all rows are checked or only one
    
    NSLog(@"row: %d",row);
    
    if (row == -1)
        for (id key in [selectionStates allKeys])
            [selectionStates setObject:[NSNumber numberWithBool:YES] forKey:key];
    else
        [selectionStates setObject:[NSNumber numberWithBool:YES] forKey:[entries objectAtIndex:row]];
    
    
//    for(int i=0;i<[entries count];i++)
//    {
//        if(i!=row)
//        {
//            [selectionStates setObject:[NSNumber numberWithBool:NO] forKey:[entries objectAtIndex:i]];
//        }
//        
//        
//    }
    
    
    NSLog(@"Done....%@",[selectionStates valueForKey:[entries objectAtIndex:0]]);
    
    
}

- (void)pickerView:(ALPickerView *)pickerView didUncheckRow:(NSInteger)row {
//     Check whether all rows are unchecked or only one
    if (row == -1)
        for (id key in [selectionStates allKeys])
            [selectionStates setObject:[NSNumber numberWithBool:NO] forKey:key];
    else
        [selectionStates setObject:[NSNumber numberWithBool:NO] forKey:[entries objectAtIndex:row]];
}



- (void)doneTouched1:(UIBarButtonItem *)sender
{
    
    finalList=[[NSMutableArray alloc]init];
    finalIdList=[[NSMutableArray alloc]init];
    for(int i=0;i<[entries count];i++)
    {
        NSLog(@"Done....%@",[selectionStates valueForKey:[entries objectAtIndex:i]]);
        if([[NSString stringWithFormat:@"%@",[selectionStates valueForKey:[entries objectAtIndex:i]]] isEqualToString:@"1"])
        {
            [finalList addObject:[entries objectAtIndex:i]];
            [finalIdList addObject:[catIds objectAtIndex:i]];
        }
    }
    
    
    
    NSString *joinedString=@"";
    if([finalList count]>0)
    {
        
        joinedString = [finalList componentsJoinedByString:@", "];
        catIdString=[finalIdList componentsJoinedByString:@","];
        
        
        
    }
    NSLog(@"......%@....%@",joinedString,catIdString);
    
    
    
    NSString *completeString=@"";
    for(int i=0;i<[entries count];i++)
    {
        NSLog(@"PRODUCT: %@",[entries objectAtIndex:i]);
        completeString = [entries componentsJoinedByString:@", "];
        NSLog(@"PRODUCT FINAL: %@",completeString);
    }
    
    
    
    _categoryTextField.text=joinedString;
    if([completeString isEqualToString:joinedString])
    {
        _categoryTextField.text=@"All";
    }
    
    
    
    
    
    [_categoryTextField resignFirstResponder];
    
    
    
    
}




- (void)cancelTouched:(UIBarButtonItem *)sender
{
    [_categoryTextField resignFirstResponder];
}




- (void)doneTouched:(UIBarButtonItem *)sender
{
    // hide the picker view
    [_mapSearchText resignFirstResponder];
    // perform some action
}











-(void)mapShow
{
    
    [mapView_ clear];
    
    self.locationManager = [[CLLocationManager alloc] init];
    
    self.locationManager.delegate = self;
    if(IS_OS_8_OR_LATER){
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                [self.locationManager requestAlwaysAuthorization];
            } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                [self.locationManager  requestWhenInUseAuthorization];
            } else {
                NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
            }
        }
    }
    [self.locationManager startUpdatingLocation];
}


-(void)productLocationAnnotation
{
    
//    [mapView_ clear];
    
    for(int i=0;i<[productList count];i++)
    {
        MapSearchClass *search=[[MapSearchClass alloc]init];
        search=[productList objectAtIndex:i];
        
        float latitude = [search.lat floatValue];
        float longitude = [search.lon floatValue];
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                                longitude:longitude
                                                                     zoom:15];
//        mapView_.camera=camera;
        //plotting the google map
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.userData=search;
        marker.position=CLLocationCoordinate2DMake(latitude, longitude);
        marker.title = search.product_name;;
        marker.snippet = [NSString stringWithFormat:@"%@\n%@",search.address,@""];
        marker.icon=[UIImage imageNamed:@"point-12.png"];
        marker.map = mapView_;
        
//        search=nil;
        
        
        
        
    }
    
    
    
}


-(IBAction)choiceBtnAction1:(id)sender
{
    
    
    if(_choiceBtn1.selected)
    {
        _choiceBtn1.selected=NO;
        _choiceImg1.image=[UIImage imageNamed:@"redcheck1.png"];
    }
    else
    {
        _choiceBtn1.selected=YES;
        _choiceImg1.image=[UIImage imageNamed:@"redcheck2.png"];
    }
    
    
}

-(IBAction)choiceBtnAction2:(id)sender
{
    if(_choiceBtn2.selected)
    {
        _choiceBtn2.selected=NO;
        _choiceImg2.image=[UIImage imageNamed:@"redcheck1.png"];
    }
    else
    {
        _choiceBtn2.selected=YES;
        _choiceImg2.image=[UIImage imageNamed:@"redcheck2.png"];
    }
}

-(IBAction)choiceBtnAction3:(id)sender
{
    if(_choiceBtn3.selected)
    {
        _choiceBtn3.selected=NO;
        _choiceImg3.image=[UIImage imageNamed:@"redcheck1.png"];
    }
    else
    {
        _choiceBtn3.selected=YES;
        _choiceImg3.image=[UIImage imageNamed:@"redcheck2.png"];
    }
}




-(IBAction)searchBtnAction:(id)sender
{
    _searchView.hidden=NO;
}


-(IBAction)searchCancelBtnAction:(id)sender
{
    _searchView.hidden=YES;
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    
    
    
    
    
//    lon = [@"88.125642" floatValue];
//    lat = [@"22.365487" floatValue];
    
    
    lon = [@"88.214563" floatValue];
    lat = [@"22.365486" floatValue];
    
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    MKMapPoint point1 = MKMapPointForCoordinate(CLLocationCoordinate2DMake(lat, lon));
    MKMapPoint point2 = MKMapPointForCoordinate(CLLocationCoordinate2DMake(lat, lon));
    
    CLLocationCoordinate2D centreLocation = MKCoordinateForMapPoint(point1);
    
    double mapScaleWidth = 1000 / fabs(point2.x - point1.x);
    double mapScaleHeight = 1000 / fabs(point2.y - point1.y);
    double mapScale = MIN(mapScaleWidth, mapScaleHeight);
    
    float rad=[[[defaults valueForKey:@"usersettings"] valueForKey:@"radius"] floatValue];
    
    
    double zoomLevel = rad * 10;
    
    
//    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
//                                                           longitude:lon
//                                                                zoom:60
//                                                             bearing:30
//                                                        viewingAngle:45];
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                            longitude:lon
                                                                 zoom:zoomLevel];//15
    
    
    mapView_.camera=camera;
    //plotting the google map
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position=CLLocationCoordinate2DMake(lat, lon);
    marker.title = @"Current Location";
    marker.snippet = @"";
    marker.icon=[UIImage imageNamed:@"point-13.png"];
    marker.map = mapView_;
    marker.tappable=NO;
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    if([defaults valueForKey:@"status"]!=nil && [[defaults valueForKey:@"status"] isEqualToString:@"1"])
    {
        
        CLLocationCoordinate2D circleCenter = CLLocationCoordinate2DMake(lat, lon);
        GMSCircle *circ = [GMSCircle circleWithPosition:circleCenter
                                                 radius:rad];
        circ.fillColor = [UIColor colorWithRed:96.0/255 green:172.0/255 blue:251.0/255 alpha:0.5];
        circ.strokeColor = [UIColor blueColor];
        circ.strokeWidth = 3;
//        circ.map = mapView_;
    }
    
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self productSearchWebService];
        
    });
    
    
    
    
//    [self productLocationAnnotation];
    
    
    
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        
        lat = [[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] floatValue];
        lon = [[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] floatValue];
        
        
        [locationManager stopUpdatingLocation];
        locationManager.delegate=nil;
        
        
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                                longitude:lon
                                                                     zoom:15];
        mapView_.camera=camera;
        //plotting the google map
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position=CLLocationCoordinate2DMake(lat, lon);
        marker.title = @"Current Location";
        marker.snippet = @"";
        marker.icon=[UIImage imageNamed:@"point-13.png"];
        marker.map = mapView_;
        marker.tappable=NO;
        
        
        
        
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        if([defaults valueForKey:@"status"]!=nil && [[defaults valueForKey:@"status"] isEqualToString:@"1"])
        {
            
            CLLocationCoordinate2D circleCenter = CLLocationCoordinate2DMake(lat, lon);
            GMSCircle *circ = [GMSCircle circleWithPosition:circleCenter
                                                     radius:[[[defaults valueForKey:@"usersettings"] valueForKey:@"radius"] floatValue]];
            circ.fillColor = [UIColor colorWithRed:96.0/255 green:172.0/255 blue:251.0/255 alpha:0.5];
            circ.strokeColor = [UIColor blueColor];
            circ.strokeWidth = 3;
            circ.map = mapView_;
        }
        
        
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self productSearchWebService];
            
        });
        
        
        
//        [self productLocationAnnotation];
        
        
    }
}








-(IBAction)productSearch:(id)sender
{
    [_mapSearchText resignFirstResponder];
    
    _searchView.hidden=YES;
    
    
    if(!_choiceBtn1.selected && !_choiceBtn2.selected && !_choiceBtn3.selected)
    {
        [appDel.alertview showInfo:self title:@"Product" subTitle:@"Please select at least one availble as" closeButtonTitle:@"OK" duration:0.0f];
    }
    else
    {
//        [self productSearchWebService];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self mapShow];
        });

        
        
    }
    
    
    
    
    
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    return [string isEqualToString:filtered];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
//    [textField resignFirstResponder];
    
    
    
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
        
        
        
        
        
        [_detailsView setFrame:CGRectMake(_detailsView.frame.origin.x, self.view.frame.size.height, _detailsView.frame.size.width, _detailsView.frame.size.height)];
        
        
        
        
    } completion:^(BOOL finished) {
    }];
    
    
    
    
    
    [_mapSearchText resignFirstResponder];
    
    _searchView.hidden=YES;
    
    
    if(!_choiceBtn1.selected && !_choiceBtn2.selected && !_choiceBtn3.selected)
    {
        [appDel.alertview showInfo:self title:@"Product" subTitle:@"Please select at least one availble as" closeButtonTitle:@"OK" duration:0.0f];
    }
    else
    {
        //        [self productSearchWebService];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self mapShow];
        });
        
        
        
    }
    
    
    return YES;
}


#pragma mark productSearchWebService

-(void)productSearchWebService
{
    NSString *type1,*type2,*type3;
    NSString *choiceString=@"";
    
    if(_choiceBtn1.selected)
    {
        if(![choiceString isEqualToString:@""])
        {
            choiceString=[choiceString stringByAppendingString:@",'0'"];
        }
        else
        {
            choiceString=[choiceString stringByAppendingString:@"'0'"];
        }
    }
    
    
    if(_choiceBtn2.selected)
    {
        if(![choiceString isEqualToString:@""])
        {
            choiceString=[choiceString stringByAppendingString:@",'1'"];
        }
        else
        {
            choiceString=[choiceString stringByAppendingString:@"'1'"];
        }
    }
    
    
    if(_choiceBtn3.selected)
    {
        if(![choiceString isEqualToString:@""])
        {
            choiceString=[choiceString stringByAppendingString:@",'2'"];
        }
        else
        {
            choiceString=[choiceString stringByAppendingString:@"'2'"];
        }
    }
    
    
    
    
    
    
    NSLog(@"choiceString %@",choiceString);
    
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *UserId;
    
    if([[defaults valueForKey:@"status"] isEqualToString:@"1"])
    {
        UserId=[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"];
    }
    else
    {
        UserId=@"";
    }
    //    UserId=@"";
    
    NSString *url = [NSString stringWithFormat:@"%@?r=api/Search",appDel.hostUrl];
    
    NSLog(@"ID %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"]);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
//    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:_mapSearchText.text,@"searchText",catIdString,@"cat_id",choiceString,@"type",UserId,@"user_id",
//                           nil];
    
    
    float miles;
    
    if([[defaults valueForKey:@"status"] isEqualToString:@"1"])
    {
        float dist=[[[defaults valueForKey:@"usersettings"] valueForKey:@"radius"] floatValue];
        miles=dist/1609.34;
    }
    else
    {
        miles=10000;
    }
    
    miles=10000.0;
    
    
    
    
    
    
    
    
    
    
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",_mapSearchText.text],@"searchText",catIdString,@"cat_id",choiceString,@"type",UserId,@"user_id",[NSString stringWithFormat:@"%f",lat],@"latitude",[NSString stringWithFormat:@"%f",lon],@"longitude",[NSString stringWithFormat:@"%f",miles],@"distance",
                           nil];
    
    
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         
         NSLog(@"JSON: %@", responseObject);
         
         
         productList=[[NSMutableArray alloc]init];
         
         if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
         {
             
             
             
             //             appDel.alertview showInfo:self title:@"Product" subTitle:@"No product found" closeButtonTitle:@"OK" duration:0.0f];
             
             
             
             for(int i=0;i<[[responseObject valueForKey:@"searchList"] count];i++)
             {
                 
                 
                 
                 
                 
                 MapSearchClass *mapSearch=[[MapSearchClass alloc]init];
                 mapSearch.product_id=[self checkNull:[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"product_id"]];
                 mapSearch.user_id=[self checkNull:[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"user_id"]];
                 mapSearch.product_name=[self checkNull:[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"product_name"]];
                 mapSearch.product_type=[self checkNull:[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"product_type"]];
                 mapSearch.product_desc=[self checkNull:[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"product_desc"]];
                 mapSearch.productQuantity=[self checkNull:[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"quantity"]];
                 mapSearch.product_category=[self checkNull:[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"category"]];
                 
                 
                 mapSearch.name=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"name"]];
                 mapSearch.phone_no=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"phone_no"]];
                 mapSearch.email=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"email"]];
                 mapSearch.address=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"address"]];
                 mapSearch.lat=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"lat"]];
                 mapSearch.lon=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"long"]];
                 mapSearch.profileimage=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"profile_image"]];
                 mapSearch.productOwnerId=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"user_id"]];
                 
                 mapSearch.productImageList=[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"images"];
                 mapSearch.backyardImageList=[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"backyard"];
                 
                 
                 
                 
                 [productList addObject:mapSearch];
                 
                 mapSearch=nil;
                 
                 
                 
             }
             
             
             
             [self productLocationAnnotation];
             
             
         }
         else
         {
             [appDel.alertview showInfo:self title:@"Product" subTitle:@"No product found" closeButtonTitle:@"OK" duration:0.0f];
         }
         
         [_table reloadData];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         NSLog(@"Error: %@", error);
         
         [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
         
     }];
    
}





- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
    
    UIView *newview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 150, 80)];
    newview.backgroundColor=[UIColor clearColor];
    
    UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 150, 80)];
    imgView.image=[UIImage imageNamed:@"MarkerView.png"];
    [newview addSubview:imgView];
    
    
    
    UILabel *usernameLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, 150, 20)];
    UILabel *productLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 130, 80)];//[[UILabel alloc]initWithFrame:CGRectMake(0, 30, 150, 30)]
    [productLabel setTextAlignment:NSTextAlignmentCenter];
    productLabel.numberOfLines=5;
    
    [self customizeLabel:usernameLabel:1];
    [self customizeLabel:productLabel:2];
    
    usernameLabel.text=@"";//marker.title
    productLabel.text=marker.snippet;
    
    [newview addSubview:usernameLabel];
    [newview addSubview:productLabel];
    
    
    //    [ma addSubview:newview];
    
    return newview;
}



- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    
    
    
    
    
    if(directionBtnTag==0)
    {
    
        MapSearchClass *search=[MapSearchClass alloc];
        search=(MapSearchClass*)marker.userData;
        productArray=[[NSMutableArray alloc]init];
        [productArray addObject:search];
        [self allProductService:search];
    
    
    
    
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
    
        
        
        
        
        [_detailsView setFrame:CGRectMake(_detailsView.frame.origin.x, self.view.frame.size.height-_detailsView.frame.size.height, _detailsView.frame.size.width, _detailsView.frame.size.height)];
    
        
        
        
    } completion:^(BOOL finished) {
        
        
        MapSearchClass *search=[MapSearchClass alloc];
        search=(MapSearchClass*)marker.userData;
        NSLog(@"DATA: %@",search.product_name);
        
        NSString *productType=@"";
        if([search.product_type isEqualToString:@"0"])
        {
            productType=@"FREE";
        }
        else if([search.product_type isEqualToString:@"1"])
        {
            productType=@"SWAP";
        }
        else if([search.product_type isEqualToString:@"2"])
        {
            productType=@"SALE";
        }
        
        _label1.text=search.product_name;
        _label2.text=search.product_category;
        _label3.text=productType;
        _Label4.text=search.name;
        _label5.text=search.productQuantity;
        _label6.text=search.product_desc;
        
        productLat=[search.lat floatValue];
        productLon=[search.lon floatValue];
        phoneNumber=search.phone_no;
        emailAddress=search.email;
        
//        if([search.phone_no isEqualToString:@""])
//        {
//            _callBtn.hidden=YES;
//        }
//        else
//        {
//            _callBtn.hidden=NO;
//        }
//        
//        if([search.email isEqualToString:@""])
//        {
//            _emailBtn.hidden=YES;
//        }
//        else
//        {
//            _emailBtn.hidden=NO;
//        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        locationTitle = search.product_name;;
        subtitle = [NSString stringWithFormat:@"%@\n%@",search.address,@""];
        
        
        
        
        NSString *productImage=@"";
        for(int i=0;i<[search.productImageList count];i++)
        {
            if([[[search.productImageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
            {
                productImage=[self checkNull:[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"]];
                break;
            }
        }
        
        
        
        [_profileImageView sd_setImageWithURL:[NSURL URLWithString:productImage]
                           placeholderImage:[UIImage imageNamed:@"noimages.png"]];
        
        
        
        
        _profileImageView.image =    [UIImage getRoundedRectImageFromImage:_profileImageView.image onReferenceView:_profileImageView withCornerRadius:_profileImageView.frame.size.width/2];
        _profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
        _profileImageView.layer.cornerRadius = _profileImageView.frame.size.width/2;
        _profileImageView.layer.borderWidth = 2;
        _profileImageView.clipsToBounds = YES;

        
        
    }];

    
    
    }
    
    
    
    
    
    return NO;
}


-(NSString *)checkNull :(NSString *)str
{
    NSString *InputString=str;
    if( (InputString == nil) ||(InputString ==(NSString *)[NSNull null])||([InputString isEqual:nil])||([InputString length] == 0)||[allTrim( InputString ) length] == 0||([InputString isEqualToString:@""])||([InputString isEqualToString:@"(NULL)"])||([InputString isEqualToString:@"<NULL>"])||([InputString isEqualToString:@"<null>"]||([InputString isEqualToString:@"(null)"])||([InputString isEqualToString:@""]))
       
       )
        return @"";
    else
        return str ;
    
}


-(void)allProductService:(MapSearchClass*)search
{
    
    NSLog(@"PRODUCT USER ID %@   %@",search.user_id,search.product_id);
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *UserId;
    
    if([[defaults valueForKey:@"status"] isEqualToString:@"1"])
    {
        UserId=[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"];
    }
    else
    {
        UserId=@"";
    }
    //    UserId=@"";
    
    
    
    
    
    
    
    
    
    
    
    
    NSString *url = [NSString stringWithFormat:@"%@?r=api/ProductOfUser",appDel.hostUrl];
    
    NSLog(@"ID %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"]);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:search.user_id,@"user_id",search.product_id,@"product_id",
                           nil];
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject)
     
     {
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         
         NSLog(@"JSON: %@", responseObject);
         
         if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
         {
             
             
             
             //             appDel.alertview showInfo:self title:@"Product" subTitle:@"No product found" closeButtonTitle:@"OK" duration:0.0f];
             
             
             
             for(int i=0;i<[[responseObject valueForKey:@"productList"] count];i++)
             {
                 
                 
                 MapSearchClass *mapSearch=[[MapSearchClass alloc]init];
                 mapSearch.product_id=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"product_id"];
                 mapSearch.user_id=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"user_id"];
                 mapSearch.product_name=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"product_name"];
                 mapSearch.product_type=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"product_type"];
                 mapSearch.product_desc=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"product_desc"];
                 mapSearch.productQuantity=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"quantity"];
                 mapSearch.product_category=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"category"];
                 
                 
                 mapSearch.name=[[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"name"];
                 mapSearch.phone_no=[[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"phone_no"];
                 mapSearch.email=[[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"email"];
                 mapSearch.address=[[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"address"];
                 mapSearch.lat=[[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"lat"];
                 mapSearch.lon=[[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"long"];
                 mapSearch.profileimage=[[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"profile_image"];
                 mapSearch.productOwnerId=[[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"user_id"];
                 mapSearch.productImageList=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"images"];
                 mapSearch.backyardImageList=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"backyard"];
                 
                 
                 
                 
                 [productArray addObject:mapSearch];
                 
                 mapSearch=nil;
                 
                 
                 
             }
             
//             [self performSegueWithIdentifier:@"productsearchpush" sender:nil];
         }
         else
         {
//             [appDel.alertview showInfo:self title:@"Product" subTitle:@"No product found" closeButtonTitle:@"OK" duration:0.0f];
         }
         
         
         [_collection reloadData];
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         NSLog(@"Error: %@", error);
         
         [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
         
         
     }];

}





#pragma mark collectionview delegates



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
//    NSLog(@"COUNT: %d",[[userdefaults valueForKey:@"backyard"] count]);
    
    return [productArray count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    ProductCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    
//    NSString *imageString=[[[userdefaults valueForKey:@"backyard"] objectAtIndex:indexPath.row] valueForKey:@"backyard_image"];//@"http://www.newyorker.com/wp-content/uploads/2012/07/chris-nolan.jpg";
//    
//    [cell.backYardImageView sd_setImageWithURL:[NSURL URLWithString:imageString]
//                              placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    
    
    
    
    
    
    
    
    
    
    
    if(indexPath.row % 2 == 0)
    {
        cell.backgroundImageView.image=[UIImage imageNamed:@"colorStrip-2.png"];
    }
    else
    {
        cell.backgroundImageView.image=[UIImage imageNamed:@"colorStrip-1select.png"];
    }
    
    
    
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        
        cell.backgroundImageView.frame=CGRectMake(0, 0, 375, 223);
        //        cell.backgroundImageView.highlightedImage=[UIImage imageNamed:@"colorStrip-1select.png"];
        
        cell.profileImageView.frame=CGRectMake(9, 57, 100, 100);
        cell.profileImageBtn.frame=CGRectMake(9, 57, 100, 100);
        
        cell.titlelabel1.frame=CGRectMake(120, 21, 99, 21);
        cell.titlelabel2.frame=CGRectMake(120, 53, 99, 21);
        cell.titlelabel3.frame=CGRectMake(120, 83, 99, 21);
        cell.titleLabel4.frame=CGRectMake(120, 115, 99, 21);
        cell.titlelabel5.frame=CGRectMake(120, 147, 99, 21);
        cell.titlelabel6.frame=CGRectMake(120, 179, 99, 21);
        
        
        cell.label1.frame=CGRectMake(226, 21, 124, 21);
        cell.label2.frame=CGRectMake(226, 53, 124, 21);
        cell.label3.frame=CGRectMake(226, 83, 124, 21);
        cell.Label4.frame=CGRectMake(226, 115, 124, 21);
        cell.label5.frame=CGRectMake(226, 147, 92, 21);
        cell.label6.frame=CGRectMake(226, 179, 92, 21);
        
        cell.separatorlabel1.frame=CGRectMake(120, 44, 229, 1);
        cell.separatorlabel2.frame=CGRectMake(120, 76, 229, 1);
        cell.separatorlabel3.frame=CGRectMake(120, 106, 229, 1);
        cell.separatorLabel4.frame=CGRectMake(120, 138, 229, 1);
        cell.separatorlabel5.frame=CGRectMake(120, 170, 229, 1);
        cell.separatorlabel6.frame=CGRectMake(120, 202, 229, 1);
        
        cell.cellSeparator.frame=CGRectMake(0, 222, 375, 1);
        
        cell.callBtn.frame=CGRectMake(318, 137, 32, 32);
        cell.emailBtn.frame=CGRectMake(318, 169, 32, 32);
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        
        cell.backgroundImageView.frame=CGRectMake(0, 0, 414, 223);
        //        cell.backgroundImageView.highlightedImage=[UIImage imageNamed:@"colorStrip-1select.png"];
        
        cell.profileImageView.frame=CGRectMake(10, 55, 110, 110);
        cell.profileImageBtn.frame=CGRectMake(10, 55, 110, 110);
        
        cell.titlelabel1.frame=CGRectMake(133, 21, 109, 21);
        cell.titlelabel2.frame=CGRectMake(133, 53, 109, 21);
        cell.titlelabel3.frame=CGRectMake(133, 83, 109, 21);
        cell.titleLabel4.frame=CGRectMake(133, 115, 109, 21);
        cell.titlelabel5.frame=CGRectMake(133, 147, 109, 21);
        cell.titlelabel6.frame=CGRectMake(133, 179, 109, 21);
        
        
        cell.label1.frame=CGRectMake(249, 21, 137, 21);
        cell.label2.frame=CGRectMake(249, 53, 137, 21);
        cell.label3.frame=CGRectMake(249, 83, 137, 21);
        cell.Label4.frame=CGRectMake(249, 115, 137, 21);
        cell.label5.frame=CGRectMake(249, 147, 102, 21);
        cell.label6.frame=CGRectMake(249, 179, 102, 21);
        
        cell.separatorlabel1.frame=CGRectMake(133, 44, 253, 1);
        cell.separatorlabel2.frame=CGRectMake(133, 76, 253, 1);
        cell.separatorlabel3.frame=CGRectMake(133, 106, 253, 1);
        cell.separatorLabel4.frame=CGRectMake(133, 138, 253, 1);
        cell.separatorlabel5.frame=CGRectMake(133, 170, 253, 1);
        cell.separatorlabel6.frame=CGRectMake(133, 202, 253, 1);
        
        cell.cellSeparator.frame=CGRectMake(0, 222, 414, 1);
        
        
        cell.callBtn.frame=CGRectMake(351, 137, 35, 35);
        cell.emailBtn.frame=CGRectMake(351, 169, 35, 35);
        
    }
    else
    {
        cell.backgroundImageView.frame=CGRectMake(0, 0, 320, 223);
        //        cell.backgroundImageView.highlightedImage=[UIImage imageNamed:@"colorStrip-1select.png"];
        
        cell.profileImageView.frame=CGRectMake(8, 64, 85, 85);
        cell.profileImageBtn.frame=CGRectMake(8, 64, 85, 85);
        
        cell.titlelabel1.frame=CGRectMake(103, 31, 84, 21);
        cell.titlelabel2.frame=CGRectMake(103, 57, 84, 21);
        cell.titlelabel3.frame=CGRectMake(103, 83, 84, 21);
        cell.titleLabel4.frame=CGRectMake(103, 109, 84, 21);
        cell.titlelabel5.frame=CGRectMake(103, 135, 84, 21);
        cell.titlelabel6.frame=CGRectMake(103, 160, 84, 21);
        
        
        cell.label1.frame=CGRectMake(192, 31, 106, 21);
        cell.label2.frame=CGRectMake(192, 57, 106, 21);
        cell.label3.frame=CGRectMake(192, 83, 106, 21);
        cell.Label4.frame=CGRectMake(192, 109, 106, 21);
        cell.label5.frame=CGRectMake(192, 135, 83, 21);
        cell.label6.frame=CGRectMake(192, 160, 83, 21);
        
        cell.separatorlabel1.frame=CGRectMake(103, 54, 196, 1);
        cell.separatorlabel2.frame=CGRectMake(103, 80, 196, 1);
        cell.separatorlabel3.frame=CGRectMake(103, 106, 196, 1);
        cell.separatorLabel4.frame=CGRectMake(103, 132, 196, 1);
        cell.separatorlabel5.frame=CGRectMake(103, 158, 196, 1);
        cell.separatorlabel6.frame=CGRectMake(103, 185, 196, 1);
        
        cell.cellSeparator.frame=CGRectMake(0, 222, 320, 1);
        
        cell.callBtn.frame=CGRectMake(275, 132, 27, 27);
        cell.emailBtn.frame=CGRectMake(275, 158, 27, 27);
    }
    
    
    
    
    
    
    
    MapSearchClass *search=[MapSearchClass alloc];
    search=[productArray objectAtIndex:indexPath.row];
    
    NSString *productType=@"";
    if([search.product_type isEqualToString:@"0"])
    {
        productType=@"FREE";
    }
    else if([search.product_type isEqualToString:@"1"])
    {
        productType=@"SWAP";
    }
    else if([search.product_type isEqualToString:@"2"])
    {
        productType=@"SALE";
    }
    
    cell.label1.text=search.product_name;
    cell.label2.text=search.product_category;
    cell.label3.text=productType;
    cell.Label4.text=search.name;
    cell.label5.text=search.phone_no;
    cell.label6.text=search.email;
    
    NSString *productImage=@"";
    for(int i=0;i<[search.productImageList count];i++)
    {
        if([[[search.productImageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            productImage=[self checkNull:[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"]];
            break;
        }
    }
    
    
    
    [cell.profileImageView sd_setImageWithURL:[NSURL URLWithString:productImage]
                             placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    cell.profileImageView.image =    [UIImage getRoundedRectImageFromImage:cell.profileImageView.image onReferenceView:cell.profileImageView withCornerRadius:cell.profileImageView.frame.size.width/2];
    cell.profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.profileImageView.layer.cornerRadius = cell.profileImageView.frame.size.width/2;
    cell.profileImageView.layer.borderWidth = 2;
    cell.profileImageView.clipsToBounds = YES;
    
    
    
    
    
    [cell.callBtn addTarget:self action:@selector(listCallBtnAction1:) forControlEvents:UIControlEventTouchUpInside];
    cell.callBtn.tag=indexPath.row;
    
    [cell.emailBtn addTarget:self action:@selector(listEmailBtnAction1:) forControlEvents:UIControlEventTouchUpInside];
    cell.emailBtn.tag=indexPath.row;
    
    [cell.profileImageBtn addTarget:self action:@selector(profileImageBtnAction1:) forControlEvents:UIControlEventTouchUpInside];
    cell.profileImageBtn.tag=indexPath.row;
    
    
    
    
    
    
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.backgroundColor=[UIColor clearColor];
    
    
    
    
    
    
    
    
    
    
    return cell;
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    UIImage *image = [imageArray objectAtIndex:indexPath.row];
    //You may want to create a divider to scale the size by the way..
    
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        return CGSizeMake(375, 223);
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        return CGSizeMake(414, 223);
    }
    else
    {
        return CGSizeMake(320, 223);
    }
    
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MapSearchClass *search=[MapSearchClass alloc];
    search=[productArray objectAtIndex:indexPath.row];
    
    ProductInfo *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"productinfopush"];
    controller.search=search;
    
    [self.navigationController pushViewController:controller animated:YES];
}




- (void) mapView:(GMSMapView *) mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    NSLog(@"coordinate %f",coordinate.latitude);
    
    
    
    
    
    
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
        
        
        
        
        
        [_detailsView setFrame:CGRectMake(_detailsView.frame.origin.x, self.view.frame.size.height, _detailsView.frame.size.width, _detailsView.frame.size.height)];
        
        
        
        
    } completion:^(BOOL finished) {
    }];
    
    
}





-(void)customizeLabel:(UILabel *)customLabel :(int)type
{
    if(type==1)
    {
        customLabel.font=[UIFont systemFontOfSize:13.0];
        [customLabel setTextColor:[UIColor blackColor]];
    }
    else
    {
        customLabel.font=[UIFont systemFontOfSize:10.0];
        [customLabel setTextColor:[UIColor darkGrayColor]];
    }
    
    
    [customLabel setTextAlignment:NSTextAlignmentCenter];
    customLabel.backgroundColor=[UIColor clearColor];
    
}
















- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"mapsearchnav"]){
        
        
        TopNavigation *vc = (TopNavigation*)[segue destinationViewController];
        vc.titleString=@"";//SEARCH MAP VIEW
        vc.fromWhere=3;//4
        vc.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"maproutemodal"]){
        
        MapRoute *vc = (MapRoute*)[segue destinationViewController];
        vc.productLat=productLat;
        vc.productLon=productLon;
        vc.locationTitle=locationTitle;
        vc.subtitle=subtitle;
        
    }
    

    
}
- (IBAction)topMenu1BtnAction:(id)sender {
    
    
    
    
    
    
    
    
//    _topBtn1.selected=NO;
//    _topBtn2.selected=YES;
//    _topBtn1.userInteractionEnabled=NO;
//    _topBtn2.userInteractionEnabled=YES;
//    
//    _listView.hidden=YES;
//    
//  
//    
//    for(UIViewController *vc in [self childViewControllers])
//    {
//        if([vc isKindOfClass:[TopNavigation class]])
//        {
//            NSLog(@"");
//            
//            TopNavigation *top=(TopNavigation *)vc;
//            top.titleLabel.text=@"SEARCH MAP VIEW";
//            
//            
//        }
//    }

    
    
    
}


- (IBAction)topMenu2BtnAction:(id)sender {
   
    
    
    if(_topBtn2.selected==YES)
    {
        _topBtn2.selected=NO;
        _listView.hidden=NO;
//        _mapView.hidden=NO;
    }
    else
    {
        _topBtn2.selected=YES;
        _listView.hidden=YES;
//        _mapView.hidden=NO;
    }

    
    
//    _topBtn1.selected=YES;
//    _topBtn2.selected=NO;
//    _topBtn1.userInteractionEnabled=YES;
//    _topBtn2.userInteractionEnabled=NO;
//    
//    _listView.hidden=NO;
//    
//    
//    
//    for(UIViewController *vc in [self childViewControllers])
//    {
//        if([vc isKindOfClass:[TopNavigation class]])
//        {
//            NSLog(@"");
//            
//            TopNavigation *top=(TopNavigation *)vc;
//            top.titleLabel.text=@"LIST VIEW";
//            
//            
//        }
//    }
    
    
}



- (IBAction)profileImageBtnAction:(id)sender {
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    
    MapSearchClass *search=[MapSearchClass alloc];
    search=[productList objectAtIndex:[sender tag]];
    NSString *productImage=@"";
    for(int i=0;i<[search.productImageList count];i++)
    {
        if([[[search.productImageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            productImage=[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"];
            break;
        }
    }
    
    controller.imageUrl=productImage;
    [self.navigationController pushViewController:controller animated:NO];
    
    
}



- (IBAction)profileImageBtnAction1:(id)sender {
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    
    MapSearchClass *search=[MapSearchClass alloc];
    search=[productArray objectAtIndex:[sender tag]];
    NSString *productImage=@"";
    for(int i=0;i<[search.productImageList count];i++)
    {
        if([[[search.productImageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            productImage=[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"];
            break;
        }
    }
    
    controller.imageUrl=productImage;
    [self.navigationController pushViewController:controller animated:NO];
    
    
}



- (IBAction)callBtnAction:(id)sender {
    
    [self callMethod:phoneNumber];
    
    
}
- (IBAction)emailBtnAction:(id)sender {
    
    [self emailMethod:emailAddress];
}
- (IBAction)directionBtnAction:(id)sender {
    
    
    
//    https://maps.google.com?saddr=Current+Location&daddr=43.12345,-76.12345
    
//    [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://saddr=Current+Location&daddr=43.12345,-76.12345"]];
    
    
//    https://maps.google.com?saddr=22.3265485,88.364789&daddr=22.687452,88.698452
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://maps.google.com?saddr=Current+Location&daddr=%f,%f",productLat,productLon]]];
    
    
    
//    MapRoute *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"maproutepush"];
//    controller.productLat=productLat;
//    controller.productLon=productLon;
//    controller.locationTitle=locationTitle;
//    controller.subtitle=subtitle;
//    [self.navigationController pushViewController:controller animated:NO];
    
    
    
//    directionBtnTag=1;
//    
//    [mapView_ clear];
//    
//
//    
//    
//    
//    
//    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
//        
//        
//        
//        
//        
//        [_detailsView setFrame:CGRectMake(_detailsView.frame.origin.x, self.view.frame.size.height, _detailsView.frame.size.width, _detailsView.frame.size.height)];
//        
//        
//        
//        
//    } completion:^(BOOL finished) {
//        
//        
//        
//        
//        
//        
//        
//        
//        [self routeDraw];
//        
//        
//        
//        closeBtn=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-25, mapView_.frame.size.height-50-30, 50, 50)];
//        closeBtn.backgroundColor=[UIColor redColor];
//        [mapView_ addSubview:closeBtn];
//        [closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:UIControlEventAllTouchEvents];
//
//    }];

    
}












#pragma mark route draw

-(void)routeDraw
{
    
    
    
    
    NSLog(@"%f, %f, %f, %f",lat,lon,productLat,productLon);
    
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                            longitude:lon
                                                                 zoom:15];
    mapView_.camera=camera;
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position=CLLocationCoordinate2DMake(lat, lon);
    marker.title = @"Current Location";
    marker.snippet = @"";
    marker.icon=[UIImage imageNamed:@"point-12.png"];
    marker.map = mapView_;
    marker.tappable=NO;
    
    
    
    
//    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
//                                                            longitude:lon
//                                                                 zoom:15];
//    mapView_.camera=camera;
    
    GMSMarker *marker1 = [[GMSMarker alloc] init];
    marker1.position=CLLocationCoordinate2DMake(productLat, productLon);
    marker1.title = locationTitle;
    marker1.snippet = subtitle;
    marker1.icon=[UIImage imageNamed:@"point-12.png"];
    marker1.map = mapView_;
    
    
    
    
    
    
    
    
    
    
    CLLocation *from = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
    CLLocation *to = [[CLLocation alloc] initWithLatitude:productLat longitude:productLon];
    
    
//    CLLocation *from = [[CLLocation alloc] initWithLatitude:40.713214 longitude:-74.013873];
//    CLLocation *to = [[CLLocation alloc] initWithLatitude:40.704935 longitude:-74.011492];
    
    
    
    
    
    
    
    
//    _coordinates = [[NSMutableArray alloc]init];
//    _routeController = [[LRouteController alloc]init];

    
    [_coordinates removeAllObjects];
    
    
    [_coordinates addObject:[[CLLocation alloc] initWithLatitude:lat longitude:lon]];
    [_coordinates addObject:[[CLLocation alloc] initWithLatitude:productLat longitude:productLon]];
    
    NSLog(@"_coordinates %@",_coordinates);
    
    if ([_coordinates count] > 1)
    {
    
    [_routeController getPolylineWithLocations:_coordinates travelMode:TravelModeDriving andCompletitionBlock:^(GMSPolyline *polyline, NSError *error) {
        if (error)
        {
            NSLog(@"%@", error);
        }
        else if (!polyline)
        {
            NSLog(@"No route");
            [_coordinates removeAllObjects];
        }
        else
        {
            _markerStart.position = [[_coordinates objectAtIndex:0] coordinate];
            _markerStart.map = mapView_;
           
            _markerFinish.position = [[_coordinates lastObject] coordinate];
            _markerFinish.map = mapView_;
            
            _polyline = polyline;
            _polyline.strokeWidth = 5;
            _polyline.strokeColor = [UIColor blueColor];
            _polyline.map = mapView_;
        }
    }];
    
    }
    
    
//    GooglePolylineRequest *gpl = [[GooglePolylineRequest alloc] init];
//    gpl.delegate = self;
//    [gpl requestPolylineFromPoint:from toPoint:to];
}


- (void)viewDidUnload
{
//    mapView_=nil;
}





















-(IBAction)closeBtnAction:(id)sender
{
    
    
    
    _polyline.map = nil;
    _markerStart.map = nil;
    _markerFinish.map = nil;
    
    
    [mapView_ clear];
    
    directionBtnTag=0;
    
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
        
        
        
        
        
        [_detailsView setFrame:CGRectMake(_detailsView.frame.origin.x, self.view.frame.size.height-_detailsView.frame.size.height, _detailsView.frame.size.width, _detailsView.frame.size.height)];
        
        
        
        
    } completion:^(BOOL finished) {
        
        
        
        [self productLocationAnnotation];
        
        [closeBtn removeFromSuperview];
        closeBtn=nil;
        
    }];
}






#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return productList.count;
}


-(UITableViewCell *)tableView: (UITableView *)tableView cellForRowAtIndexPath: (NSIndexPath *)indexPath{
    
    
    
    static  NSString *CellIdentifier = @"ProductCell";
    MapSearchListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[MapSearchListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
//    cell.backgroundImageView.image=[UIImage imageNamed:@"colorStrip-2.png"];
    
    
    if(indexPath.row % 2 == 0)
    {
        cell.backgroundImageView.image=[UIImage imageNamed:@"colorStrip-2.png"];
    }
    else
    {
        cell.backgroundImageView.image=[UIImage imageNamed:@"colorStrip-1select.png"];
    }
    
    

    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        
        cell.backgroundImageView.frame=CGRectMake(0, 0, 375, 223);
//        cell.backgroundImageView.highlightedImage=[UIImage imageNamed:@"colorStrip-1select.png"];
        
        cell.profileImageView.frame=CGRectMake(9, 57, 100, 100);
        cell.profileImageBtn.frame=CGRectMake(9, 57, 100, 100);
        
        cell.titlelabel1.frame=CGRectMake(120, 21, 99, 21);
        cell.titlelabel2.frame=CGRectMake(120, 53, 99, 21);
        cell.titlelabel3.frame=CGRectMake(120, 83, 99, 21);
        cell.titleLabel4.frame=CGRectMake(120, 115, 99, 21);
        cell.titlelabel5.frame=CGRectMake(120, 147, 99, 21);
        cell.titlelabel6.frame=CGRectMake(120, 179, 99, 21);
        
        
        cell.label1.frame=CGRectMake(226, 21, 124, 21);
        cell.label2.frame=CGRectMake(226, 53, 124, 21);
        cell.label3.frame=CGRectMake(226, 83, 124, 21);
        cell.Label4.frame=CGRectMake(226, 115, 124, 21);
        cell.label5.frame=CGRectMake(226, 147, 92, 21);
        cell.label6.frame=CGRectMake(226, 179, 92, 21);
        
        cell.separatorlabel1.frame=CGRectMake(120, 44, 229, 1);
        cell.separatorlabel2.frame=CGRectMake(120, 76, 229, 1);
        cell.separatorlabel3.frame=CGRectMake(120, 106, 229, 1);
        cell.separatorLabel4.frame=CGRectMake(120, 138, 229, 1);
        cell.separatorlabel5.frame=CGRectMake(120, 170, 229, 1);
        cell.separatorlabel6.frame=CGRectMake(120, 202, 229, 1);
        
        cell.cellSeparator.frame=CGRectMake(0, 222, 375, 1);
        
        cell.callBtn.frame=CGRectMake(318, 137, 32, 32);
        cell.emailBtn.frame=CGRectMake(318, 169, 32, 32);
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        
        cell.backgroundImageView.frame=CGRectMake(0, 0, 414, 223);
//        cell.backgroundImageView.highlightedImage=[UIImage imageNamed:@"colorStrip-1select.png"];
        
        cell.profileImageView.frame=CGRectMake(10, 55, 110, 110);
        cell.profileImageBtn.frame=CGRectMake(10, 55, 110, 110);
        
        cell.titlelabel1.frame=CGRectMake(133, 21, 109, 21);
        cell.titlelabel2.frame=CGRectMake(133, 53, 109, 21);
        cell.titlelabel3.frame=CGRectMake(133, 83, 109, 21);
        cell.titleLabel4.frame=CGRectMake(133, 115, 109, 21);
        cell.titlelabel5.frame=CGRectMake(133, 147, 109, 21);
        cell.titlelabel6.frame=CGRectMake(133, 179, 109, 21);
        
        
        cell.label1.frame=CGRectMake(249, 21, 137, 21);
        cell.label2.frame=CGRectMake(249, 53, 137, 21);
        cell.label3.frame=CGRectMake(249, 83, 137, 21);
        cell.Label4.frame=CGRectMake(249, 115, 137, 21);
        cell.label5.frame=CGRectMake(249, 147, 102, 21);
        cell.label6.frame=CGRectMake(249, 179, 102, 21);
        
        cell.separatorlabel1.frame=CGRectMake(133, 44, 253, 1);
        cell.separatorlabel2.frame=CGRectMake(133, 76, 253, 1);
        cell.separatorlabel3.frame=CGRectMake(133, 106, 253, 1);
        cell.separatorLabel4.frame=CGRectMake(133, 138, 253, 1);
        cell.separatorlabel5.frame=CGRectMake(133, 170, 253, 1);
        cell.separatorlabel6.frame=CGRectMake(133, 202, 253, 1);
        
        cell.cellSeparator.frame=CGRectMake(0, 222, 414, 1);
        
        
        cell.callBtn.frame=CGRectMake(351, 137, 35, 35);
        cell.emailBtn.frame=CGRectMake(351, 169, 35, 35);
        
    }
    else
    {
        cell.backgroundImageView.frame=CGRectMake(0, 0, 320, 223);
//        cell.backgroundImageView.highlightedImage=[UIImage imageNamed:@"colorStrip-1select.png"];
        
        cell.profileImageView.frame=CGRectMake(8, 64, 85, 85);
        cell.profileImageBtn.frame=CGRectMake(8, 64, 85, 85);
        
        cell.titlelabel1.frame=CGRectMake(103, 21, 84, 21);
        cell.titlelabel2.frame=CGRectMake(103, 53, 84, 21);
        cell.titlelabel3.frame=CGRectMake(103, 83, 84, 21);
        cell.titleLabel4.frame=CGRectMake(103, 115, 84, 21);
        cell.titlelabel5.frame=CGRectMake(103, 147, 84, 21);
        cell.titlelabel6.frame=CGRectMake(103, 179, 84, 21);
        
        
        cell.label1.frame=CGRectMake(192, 21, 106, 21);
        cell.label2.frame=CGRectMake(192, 53, 106, 21);
        cell.label3.frame=CGRectMake(192, 83, 106, 21);
        cell.Label4.frame=CGRectMake(192, 115, 106, 21);
        cell.label5.frame=CGRectMake(192, 147, 83, 21);
        cell.label6.frame=CGRectMake(192, 179, 83, 21);
        
        cell.separatorlabel1.frame=CGRectMake(103, 44, 196, 1);
        cell.separatorlabel2.frame=CGRectMake(103, 76, 196, 1);
        cell.separatorlabel3.frame=CGRectMake(103, 106, 196, 1);
        cell.separatorLabel4.frame=CGRectMake(103, 138, 196, 1);
        cell.separatorlabel5.frame=CGRectMake(103, 170, 196, 1);
        cell.separatorlabel6.frame=CGRectMake(103, 202, 196, 1);
        
        cell.cellSeparator.frame=CGRectMake(0, 222, 320, 1);
        
        cell.callBtn.frame=CGRectMake(275, 142, 27, 27);
        cell.emailBtn.frame=CGRectMake(275, 174, 27, 27);
    }
    
    
    
    
    
    
    
    MapSearchClass *search=[MapSearchClass alloc];
    search=[productList objectAtIndex:indexPath.row];
    
    NSString *productType=@"";
    if([search.product_type isEqualToString:@"0"])
    {
        productType=@"FREE";
    }
    else if([search.product_type isEqualToString:@"1"])
    {
        productType=@"SWAP";
    }
    else if([search.product_type isEqualToString:@"2"])
    {
        productType=@"SALE";
    }

    cell.label1.text=search.product_name;
    cell.label2.text=search.product_category;
    cell.label3.text=productType;
    cell.Label4.text=search.name;
    cell.label5.text=search.phone_no;
    cell.label6.text=search.email;
    
    NSString *productImage=@"";
    for(int i=0;i<[search.productImageList count];i++)
    {
        if([[[search.productImageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            productImage=[self checkNull:[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"]];
            break;
        }
    }
    
    
    
    [cell.profileImageView sd_setImageWithURL:[NSURL URLWithString:productImage]
                         placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    cell.profileImageView.image =    [UIImage getRoundedRectImageFromImage:cell.profileImageView.image onReferenceView:cell.profileImageView withCornerRadius:cell.profileImageView.frame.size.width/2];
    cell.profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.profileImageView.layer.cornerRadius = cell.profileImageView.frame.size.width/2;
    cell.profileImageView.layer.borderWidth = 2;
    cell.profileImageView.clipsToBounds = YES;
    
    
    
    
    
    [cell.callBtn addTarget:self action:@selector(listCallBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.callBtn.tag=indexPath.row;
    
    [cell.emailBtn addTarget:self action:@selector(listEmailBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.emailBtn.tag=indexPath.row;

    
    [cell.profileImageBtn addTarget:self action:@selector(profileImageBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.profileImageBtn.tag=indexPath.row;
    
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        return 223.0;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        return 223.0;
    }
    else
    {
        return 223.0;
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MapSearchClass *search=[MapSearchClass alloc];
    search=[productList objectAtIndex:indexPath.row];
    
    ProductInfo *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"productinfopush"];
    controller.search=search;
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
//    [self performSegueWithIdentifier:@"profilereveal" sender:self];
}



-(IBAction)listCallBtnAction:(id)sender
{
    MapSearchClass *search=[MapSearchClass alloc];
    search=[productList objectAtIndex:[sender tag]];
    
    [self callMethod:search.phone_no];
}

-(IBAction)listEmailBtnAction:(id)sender
{
    MapSearchClass *search=[MapSearchClass alloc];
    search=[productList objectAtIndex:[sender tag]];
    
    [self emailMethod:search.email];
}


-(IBAction)listCallBtnAction1:(id)sender
{
    MapSearchClass *search=[MapSearchClass alloc];
    search=[productArray objectAtIndex:[sender tag]];
    
    [self callMethod:search.phone_no];
}

-(IBAction)listEmailBtnAction1:(id)sender
{
    MapSearchClass *search=[MapSearchClass alloc];
    search=[productArray objectAtIndex:[sender tag]];
    
    [self emailMethod:search.email];
}


-(void)callMethod:(NSString *)str
{
    NSString *phNo = str;
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        alert.shouldDismissOnTapOutside = YES;
        
        [alert alertIsDismissed:^{
            NSLog(@"SCLAlertView dismissed!");
        }];
        
        [alert showInfo:self title:@"INFO" subTitle:@"Call facility is not available!!!" closeButtonTitle:@"OK" duration:0.0f];
    }
}

-(void)emailMethod:(NSString *)str
{
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
        {
            [self displayComposerSheet:str];
            //            [self launchMailAppOnDevice];
        }
        else
        {
            [self launchMailAppOnDevice];
        }
    }
    else
    {
        [self launchMailAppOnDevice];
    }
}

-(void)displayComposerSheet:(NSString *)str
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    [picker setSubject:@"Backyard"];
    NSArray *toRecipients = [NSArray arrayWithObject:str];
    [picker setToRecipients:toRecipients];
    NSString *emailBody =@"";
    [picker setMessageBody:emailBody isHTML:NO];
    [picker.navigationBar setTintColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0]];
    //	[self presentModalViewController:picker animated:YES];
    [self.navigationController presentViewController:picker animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //	message.text = @"Result: canceled";
            break;
        case MFMailComposeResultSaved:
            //message.text = @"Result: saved";
            break;
        case MFMailComposeResultSent:
            //message.text = @"Result: sent";
            break;
        case MFMailComposeResultFailed:
            //message.text = @"Result: failed";
            break;
        default:
            //message.text = @"Result: not sent";
            break;
    }
    //	[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)launchMailAppOnDevice
{
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    alert.shouldDismissOnTapOutside = YES;
    
    [alert alertIsDismissed:^{
        NSLog(@"SCLAlertView dismissed!");
    }];
    
    [alert showInfo:self title:@"INFO" subTitle:@"Please configure your mail" closeButtonTitle:@"OK" duration:0.0f];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
