//
//  MapClass.h
//  Exchange
//
//  Created by Debayan Ghosh on 03/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MapClass : NSObject
@property (nonatomic,retain) NSString *lat, *lon, *locationName;
@end
