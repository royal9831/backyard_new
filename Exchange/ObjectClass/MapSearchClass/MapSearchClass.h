//
//  MapSearchClass.h
//  Exchange
//
//  Created by Debayan Ghosh on 19/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MapSearchClass : NSObject
@property (nonatomic,retain) NSString *product_id,*user_id,*product_name,*product_type,*product_category,*product_desc,*name,*phone_no,*address,*email,*lat,*lon,*profileimage,*productQuantity,*productOwnerId;
@property (nonatomic,retain) NSMutableArray *productImageList, *backyardImageList;
@end



