//
//  ProductClass.h
//  Exchange
//
//  Created by Debayan Ghosh on 14/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductClass : NSObject
@property(nonatomic,retain) NSString *productId,*productName, *productLocation,*productDesc,*productTime, *productQuantity,*productType,*categoriesName, *categoriesIds,*productAmount;
@property (nonatomic,retain) NSString *username,*useraddress,*email;
@property (nonatomic,retain) NSMutableArray *imageList;
@end
