//
//  CategoryObject.h
//  Exchange
//
//  Created by Debayan Ghosh on 11/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryObject : NSObject
@property (nonatomic,retain)NSString *CatId, *CatName;
@end
