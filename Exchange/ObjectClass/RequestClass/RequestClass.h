//
//  RequestClass.h
//  Exchange
//
//  Created by Debayan Ghosh on 03/09/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestClass : NSObject
@property (nonatomic,retain) NSString *product_id,*product_name,*req_id,*request_date,*status,*swap_name,*swap_quantity,*user_id;
@property (nonatomic,retain) NSDictionary *demandUserDict;
@end
