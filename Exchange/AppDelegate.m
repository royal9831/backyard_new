//
//  AppDelegate.m
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//




#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>


//AIzaSyAkaA3NrSz8PU36iAWDYx_eXAiudeLWiBs

NSString * const NSURLIsExcludedFromBackupKey = @"NSURLIsExcludedFromBackupKey";

//#import <GoogleMapsM4B/GoogleMaps.h>
#define kMapApiKey                      @"AIzaSyDanxIZKbyipXbkiDCB6mrErEF1eJD5zjA"  //             AIzaSyBL5wLVeEmnveYO2EhDZcwiBU6Nk_eP3vA
@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize sidebarStatus,lastValue,AccessTocken,token,pushNotificationVal,googleApiKey,allPickerTag;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    self.allPickerTag=0;
    
    // Override point for customization after application launch.
    [GMSServices provideAPIKey:kMapApiKey];
    
//    self.googleApiKey=@"AIzaSyC4Pr5tW_zI5gNosbmjIxKNLmcwCyeNJmU";
    
    self.googleApiKey=@"AIzaSyAbGHvHFND3MkeHBmSgSgU22Ge6-HmM4eY";
    
     _hostUrl = @"http://dev1.businessprodemo.com/Backyard/php/index.php";
    
//     _hostUrl = @"http://test2.businessprodemo.com/Backyard/v1/index.php";
    
    
    NSString *str=[NSString stringWithFormat:@"EXE: %@",self.hostUrl];
    NSLog(@"URL: %@",str);
    
    
    
    
    self.token=@"";
    
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    }
    else
    {
        // Register for Push Notifications, if running iOS version < 8
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
    
    
    return YES;
}







#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:   (UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString   *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //    //handle the actions
    //    if ([identifier isEqualToString:@"declineAction"]){
    //    }
    //    else if ([identifier isEqualToString:@"answerAction"]){
    //    }
    
    completionHandler();
}
#endif

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    
    
    
    
    self.pushNotificationVal = [[NSData alloc] initWithData:deviceToken];
    NSLog(@"device token %@",self.pushNotificationVal);
    
    AccessTocken=[[NSString alloc]initWithFormat:@"%@", pushNotificationVal];
    
    
    
    self.token = [[[[AccessTocken description]
                    stringByReplacingOccurrencesOfString:@"<"withString:@""]
                   stringByReplacingOccurrencesOfString:@">" withString:@""]
                  stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    
    
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"ddd %@",str);
    
}




- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
   
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushReceived" object:nil userInfo:nil];
    
    
    for (id key in userInfo) {
        NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
    }
}




-(SCLAlertView *)alertview
{
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    //    [alert addButton:@"First Button" target:self selector:@selector(firstButton)];
    alert.shouldDismissOnTapOutside = NO;
    alert.showAnimationType=SlideInToCenter;
    alert.hideAnimationType=SlideOutToCenter;
    [alert alertIsDismissed:^{
        
        NSLog(@"SCLAlertView dismissed!");
        
    }];
    
    return alert;
   
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
