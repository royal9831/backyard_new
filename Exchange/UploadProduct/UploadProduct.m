//
//  UploadProduct.m
//  Exchange
//
//  Created by Debayan Ghosh on 04/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "UploadProduct.h"
#import "TopNavigation.h"
#import "SCLAlertView.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "UIImage+RoundedImage.h"
#import "CategoryObject.h"
#import "ProductListing.h"
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
NSString *catIdString;

@interface UploadProduct ()<UIViewControllerTransitioningDelegate,ProfileViewPageDetegale>

@end

@implementation UploadProduct
@synthesize fromWhaereTag,product,delegate,uploadTag;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
//    CGRect viewFrame = self.view.frame;
//    viewFrame.origin.y = -80;
//    [self.view setFrame:viewFrame];
    
    
    _nameTextField.returnKeyType = UIReturnKeyDone;
    _quantityTextField.returnKeyType = UIReturnKeyDone;
    _amountTextField.returnKeyType = UIReturnKeyDone;
    
    
//    _imageView1.image=nil;
    
    NSLog(@"From Where: %d",fromWhaereTag);
    
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    [_categoryTextField setValue:[UIColor blackColor]
                   forKeyPath:@"_placeholderLabel.textColor"];
    catIdString=@"";
    
    entries=[[NSMutableArray alloc]init];
    catIds=[[NSMutableArray alloc]init];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    SWRevealViewController *revealController = [self revealViewController];
    self.revealViewController.delegate = self;
    tap = [revealController tapGestureRecognizer];
    tap.delegate = self;
    tap.enabled=NO;
    [self.view addGestureRecognizer:tap];
    
    
    
    
    
    
    
    
    [self makeRoundImage:_imageView1];
    [self makeRoundImage:_imageView2];
    [self makeRoundImage:_imageView3];
    [self makeRoundImage:_imageView4];
    [self makeRoundImage:_imageView5];
    
    
    if(fromWhaereTag==2)
    {
        _barBtn1.selected=NO;
    }
    else
    {
        _barBtn1.selected=YES;
        
        
        
        _imageView1.tag=0;
        _imageView2.tag=0;
        _imageView3.tag=0;
        _imageView4.tag=0;
        _imageView5.tag=0;
        
        
        
    }
    
    
    _barBtn2.selected=NO;
    _barBtn3.selected=NO;
    _barBtn4.selected=NO;
    _barBtn5.selected=NO;
    
    
    
    _choiceBtn1.selected=NO;
    _choiceBtn2.selected=NO;
    _choiceBtn3.selected=NO;
    
    _choiceImg1.image=[UIImage imageNamed:@"radio-button-off-1.png"];
    _choiceImg2.image=[UIImage imageNamed:@"radio-button-off-1.png"];
    _choiceImg3.image=[UIImage imageNamed:@"radio-button-off-1.png"];
    
    
    
    _amountLabel.hidden=YES;
    _amountTextImageView.hidden=YES;
    _amountTextField.hidden=YES;
    
    
    
    
    
    _scroll.frame=CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height+64);
    if([ [ UIScreen mainScreen ] bounds ].size.height==480)
    {
        _scroll.frame=CGRectMake(0, 64, self.view.frame.size.width, 568+64);
        
    }
    
    
    
    
    
    _scroll.contentSize=CGSizeMake(self.view.frame.size.width, _choiceBtn1.frame.origin.y+_choiceBtn1.frame.size.height+64+300);
    
        
    if([ [ UIScreen mainScreen ] bounds ].size.height==480)
    {
        _scroll.contentSize=CGSizeMake(self.view.frame.size.width, _choiceBtn1.frame.origin.y+_choiceBtn1.frame.size.height+100+64+64+80);
        
    }
    
    
    
    // add a toolbar with Cancel & Done button
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleBlackOpaque;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTouched:)];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTouched:)];
    
    // the middle button is to make the Done button align to right
    [toolBar setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneButton, nil]];
    _productDescriptionText.inputAccessoryView = toolBar;
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
    [self categoryWebservice];
    
    });
    
    
    
    
    _productDescriptionText.text=@"";
    
    
    if(fromWhaereTag==2)
    {
        
        catIdString=product.categoriesIds;
        
        
        
        _nameTextField.text=product.productName;
        _quantityTextField.text=product.productQuantity;
        _productDescriptionText.text=product.productDesc;
        
        
        
        
        
        
//        NSString *completeString=@"";
//        for(int i=0;i<[entries count];i++)
//        {
//            NSLog(@"PRODUCT: %@",[entries objectAtIndex:i]);
//            completeString = [entries componentsJoinedByString:@","];
//            NSLog(@"PRODUCT FINAL: %@",completeString);
//        }
//        
//        
//        
//        _categoryTextField.text=product.categoriesName;
//        if([completeString isEqualToString:product.categoriesName])
//        {
//            _categoryTextField.text=@"All";
//        }
        
        
        
        
        
        
        
        
        _amountTextField.text=product.productAmount;
        
        int selectedIndex=0;
        NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
        for(int i=0;i<[product.imageList count];i++)
        {
            if([[[product.imageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
            {
                selectedIndex=i+1;
                [noncoverimages addObject:[[product.imageList objectAtIndex:i] valueForKey:@"img_name"]];
            }
            else
            {
                [noncoverimages addObject:[[product.imageList objectAtIndex:i] valueForKey:@"img_name"]];
            }
        }
        
        switch (selectedIndex) {
            case 1:
                _barBtn1.selected=YES;
                break;
            case 2:
                _barBtn2.selected=YES;
                break;
            case 3:
                _barBtn3.selected=YES;
                break;
            case 4:
                _barBtn4.selected=YES;
                break;
            case 5:
                _barBtn5.selected=YES;
                break;
                
            default:
                break;
        }
        
        
        if([product.productType isEqualToString:@"0"])
        {
            [self choiceBtnAction1:nil];
        }
        else if([product.productType isEqualToString:@"1"])
        {
            [self choiceBtnAction2:nil];
        }
        else if([product.productType isEqualToString:@"2"])
        {
            [self choiceBtnAction3:nil];
        }
       
        NSString *url1=[noncoverimages objectAtIndex:0];
        NSString *url2=[noncoverimages objectAtIndex:1];
        NSString *url3=[noncoverimages objectAtIndex:2];
        NSString *url4=[noncoverimages objectAtIndex:3];
        NSString *url5=[noncoverimages objectAtIndex:4];
        
        
        
        
        
        [_imageView1 sd_setImageWithURL:[NSURL URLWithString:url1]
                           placeholderImage:[UIImage imageNamed:@"noimages.png"]];
        [_imageView2 sd_setImageWithURL:[NSURL URLWithString:url2]
                           placeholderImage:[UIImage imageNamed:@"noimages.png"]];
        [_imageView3 sd_setImageWithURL:[NSURL URLWithString:url3]
                           placeholderImage:[UIImage imageNamed:@"noimages.png"]];
        [_imageView4 sd_setImageWithURL:[NSURL URLWithString:url4]
                           placeholderImage:[UIImage imageNamed:@"noimages.png"]];
        [_imageView5 sd_setImageWithURL:[NSURL URLWithString:url5]
                           placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    }
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard:)];
    [_scroll addGestureRecognizer:gestureRecognizer];
    
    
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_nameTextField resignFirstResponder];
    [_quantityTextField resignFirstResponder];
    [_amountTextField resignFirstResponder];
    [_productDescriptionText resignFirstResponder];
    
    
}


-(void) hideKeyBoard:(id) sender
{
    [_nameTextField resignFirstResponder];
    [_quantityTextField resignFirstResponder];
    [_amountTextField resignFirstResponder];
    [_productDescriptionText resignFirstResponder];

}



-(IBAction)choiceBtnAction1:(id)sender
{
    _choiceImg1.image=[UIImage imageNamed:@"radio-button-on-1.png"];
    _choiceImg2.image=[UIImage imageNamed:@"radio-button-off-1.png"];
    _choiceImg3.image=[UIImage imageNamed:@"radio-button-off-1.png"];
    
    _choiceBtn1.selected=YES;
    _choiceBtn2.selected=NO;
    _choiceBtn3.selected=NO;
    
    _choiceBtn1.userInteractionEnabled=NO;
    _choiceBtn2.userInteractionEnabled=YES;
    _choiceBtn3.userInteractionEnabled=YES;
    
    
    _amountLabel.hidden=YES;
    _amountTextImageView.hidden=YES;
    _amountTextField.hidden=YES;
    
}

-(IBAction)choiceBtnAction2:(id)sender
{
    _choiceImg1.image=[UIImage imageNamed:@"radio-button-off-1.png"];
    _choiceImg2.image=[UIImage imageNamed:@"radio-button-on-1.png"];
    _choiceImg3.image=[UIImage imageNamed:@"radio-button-off-1.png"];
    
    _choiceBtn1.selected=NO;
    _choiceBtn2.selected=YES;
    _choiceBtn3.selected=NO;
    
    _choiceBtn1.userInteractionEnabled=YES;
    _choiceBtn2.userInteractionEnabled=NO;
    _choiceBtn3.userInteractionEnabled=YES;
    
    _amountLabel.hidden=YES;
    _amountTextImageView.hidden=YES;
    _amountTextField.hidden=YES;
}

-(IBAction)choiceBtnAction3:(id)sender
{
    _choiceImg1.image=[UIImage imageNamed:@"radio-button-off-1.png"];
    _choiceImg2.image=[UIImage imageNamed:@"radio-button-off-1.png"];
    _choiceImg3.image=[UIImage imageNamed:@"radio-button-on-1.png"];
    
    _choiceBtn1.selected=NO;
    _choiceBtn2.selected=NO;
    _choiceBtn3.selected=YES;
    
    _choiceBtn1.userInteractionEnabled=YES;
    _choiceBtn2.userInteractionEnabled=YES;
    _choiceBtn3.userInteractionEnabled=NO;
    
    _amountLabel.hidden=NO;
    _amountTextImageView.hidden=NO;
    _amountTextField.hidden=NO;
}

#pragma  mark categoryWebservice

-(void)categoryWebservice
{
    
    
    
//    ProductDetails[product_name], ProductDetails[user_id], ProductDetails[product_type], ProductDetails[product_desc], cat_id,ProductDetails[quantity]
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/Category",appDel.hostUrl];
    
    NSLog(@"ID %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"]);
    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],@"ProductDetails[user_id]",_nameTextField.text, @"ProductDetails[product_name]",_productDescriptionText.text,@"ProductDetails[product_desc]",@"1",@"ProductDetails[product_type]",catIdString,@"cat_id" ,nil];
//    
//    [manager POST:url parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:                           _emailTextField.text,@"UserLogin[username]",_passwordTextField.text,@"UserLogin[password]",
//                           nil];
    
    [manager POST:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    
    {
        
        
//        NSData * imgData;
//        
//        imgData = UIImagePNGRepresentation(_circleImageView.image);
//        
//        if(imgData!=NULL){
//            [formData appendPartWithFileData:imgData name:@"profileImage" fileName:@"avatar.png" mimeType:@"image/png"];
//        }
//        
//        
//    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        
        NSLog(@"JSON: %@", responseObject);
        
        if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
        {
            
            
            entries=[[NSMutableArray alloc]init];
            catIds=[[NSMutableArray alloc]init];
            
            
            
            
            for(int i=0;i<[[responseObject valueForKey:@"category"] count];i++)
            {
                
                
                [catIds addObject:[[[responseObject valueForKey:@"category"] objectAtIndex:i] valueForKey:@"cat_id"]];
                [entries addObject:[[[responseObject valueForKey:@"category"] objectAtIndex:i] valueForKey:@"cat_name"]];
                
                
            }
            
            
            selectionStates = [[NSMutableDictionary alloc] init];
            for (NSString *key in entries)
            {
                [selectionStates setObject:[NSNumber numberWithBool:NO] forKey:key];
            }
            
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            [defaults setObject:[responseObject valueForKey:@"profile"] forKey:@"userdetails"];
//            [defaults synchronize];
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"profilepicchange" object:nil userInfo:nil];
//            
//            [appDel.alertview showInfo:self title:@"Profile Update" subTitle:@"Profile updated successfully" closeButtonTitle:@"OK" duration:0.0f];
        }
        else
        {
//            [appDel.alertview showInfo:self title:@"Profile Update" subTitle:[responseObject valueForKey:@"message"] closeButtonTitle:@"OK" duration:0.0f];
        }
        
        
        
        
        
        
        NSString *completeString=@"";
        for(int i=0;i<[entries count];i++)
        {
            NSLog(@"PRODUCT: %@",[entries objectAtIndex:i]);
            completeString = [entries componentsJoinedByString:@","];
            NSLog(@"PRODUCT FINAL: %@",completeString);
        }
        
        
        
        _categoryTextField.text=product.categoriesName;
        if([completeString isEqualToString:product.categoriesName])
        {
            _categoryTextField.text=@"All";
        }

        
        
        
        
        
        [self setPickerView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        NSLog(@"Error: %@", error);
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
        
        
    }];

}

-(void)setPickerView
{
    appDel.allPickerTag=0;
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        pickerView = [[ALPickerView alloc] initWithFrame:CGRectMake(0, 68, 375.0, 0)];
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        pickerView = [[ALPickerView alloc] initWithFrame:CGRectMake(0, 68, 414, 0)];
    }
    else
    {
        pickerView = [[ALPickerView alloc] initWithFrame:CGRectMake(0, 68, 320, 0)];
    }
    
//    pickerView = [[ALPickerView alloc] initWithFrame:CGRectMake(0, 68, 0, 0)];
    pickerView.delegate = self;
    _categoryTextField.inputView = pickerView;
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleBlackOpaque;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTouched1:)];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTouched:)];
    [toolBar setItems:[NSArray arrayWithObjects:cancelButton, [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneButton, nil]];
    _categoryTextField.inputAccessoryView = toolBar;
    
    
    
//    entries=[[NSMutableArray alloc]init];
//    [entries addObject:@"ABC1"];
//    [entries addObject:@"ABC2"];
//    [entries addObject:@"ABC3"];
//    [entries addObject:@"ABC4"];
//    [entries addObject:@"ABC5"];
//    [entries addObject:@"ABC6"];
    
    
    
    
//    selectionStates = [[NSMutableDictionary alloc] init];
//    for (NSString *key in entries)
//    {
//        [selectionStates setObject:[NSNumber numberWithBool:NO] forKey:key];
//    }
    
    
    [pickerView reloadAllComponents];
    
    
}


#pragma mark pickerDelegates

- (NSInteger)numberOfRowsForPickerView:(ALPickerView *)pickerView {
    return [entries count];
}

- (NSString *)pickerView:(ALPickerView *)pickerView textForRow:(NSInteger)row {
    return [entries objectAtIndex:row] ;
}

- (BOOL)pickerView:(ALPickerView *)pickerView selectionStateForRow:(NSInteger)row {
    return [[selectionStates objectForKey:[entries objectAtIndex:row]] boolValue];
}

- (void)pickerView:(ALPickerView *)pickerView didCheckRow:(NSInteger)row {
    // Check whether all rows are checked or only one
    
    NSLog(@"row: %d",row);
    
//    if (row == -1)
//        for (id key in [selectionStates allKeys])
//            [selectionStates setObject:[NSNumber numberWithBool:YES] forKey:key];
//    else
        [selectionStates setObject:[NSNumber numberWithBool:YES] forKey:[entries objectAtIndex:row]];
    
    
    for(int i=0;i<[entries count];i++)
    {
        if(i!=row)
        {
            [selectionStates setObject:[NSNumber numberWithBool:NO] forKey:[entries objectAtIndex:i]];
        }
        
        
    }

    
    
    NSLog(@"Done....%@",[selectionStates valueForKey:[entries objectAtIndex:0]]);
    
    
}

- (void)pickerView:(ALPickerView *)pickerView didUncheckRow:(NSInteger)row {
    // Check whether all rows are unchecked or only one
    if (row == -1)
        for (id key in [selectionStates allKeys])
            [selectionStates setObject:[NSNumber numberWithBool:NO] forKey:key];
    else
        [selectionStates setObject:[NSNumber numberWithBool:NO] forKey:[entries objectAtIndex:row]];
}




- (void)doneTouched1:(UIBarButtonItem *)sender
{
    
    finalList=[[NSMutableArray alloc]init];
    finalIdList=[[NSMutableArray alloc]init];
    for(int i=0;i<[entries count];i++)
    {
        NSLog(@"Done....%@",[selectionStates valueForKey:[entries objectAtIndex:i]]);
        if([[NSString stringWithFormat:@"%@",[selectionStates valueForKey:[entries objectAtIndex:i]]] isEqualToString:@"1"])
        {
            [finalList addObject:[entries objectAtIndex:i]];
            [finalIdList addObject:[catIds objectAtIndex:i]];
        }
    }
    
    
    
    NSString *joinedString=@"";
    if([finalList count]>0)
    {
        
        joinedString = [finalList componentsJoinedByString:@", "];
        catIdString=[finalIdList componentsJoinedByString:@","];
        
        
               
    }
    NSLog(@"......%@....%@",joinedString,catIdString);
    
    
    
    NSString *completeString=@"";
    for(int i=0;i<[entries count];i++)
    {
        NSLog(@"PRODUCT: %@",[entries objectAtIndex:i]);
        completeString = [entries componentsJoinedByString:@", "];
        NSLog(@"PRODUCT FINAL: %@",completeString);
    }
    
    
    
    _categoryTextField.text=joinedString;
    if([completeString isEqualToString:joinedString])
    {
        _categoryTextField.text=@"All";
    }
    
    
    
    
    
    
   [_categoryTextField resignFirstResponder];
    
    
    
    
}





- (void)cancelTouched:(UIBarButtonItem *)sender
{
    [_categoryTextField resignFirstResponder];
}

- (void)doneTouched:(UIBarButtonItem *)sender
{
    // hide the picker view
    [_productDescriptionText resignFirstResponder];
    
    // perform some action
}

-(void)makeRoundImage:(UIImageView *)imageView
{
    imageView.image =    [UIImage getRoundedRectImageFromImage:imageView.image onReferenceView:imageView withCornerRadius:imageView.frame.size.width/2];
    imageView.layer.borderColor = [UIColor whiteColor].CGColor;
    imageView.layer.cornerRadius = imageView.frame.size.width/2;
    imageView.layer.borderWidth = 2;
    imageView.clipsToBounds = YES;
}

-(IBAction)selectImageBtnAction:(id)sender
{
    btnIndex=[sender tag];
    
    
    
    NSString *other1 = @"Choose from Gallery";
    NSString *other2 = @"Take Picture";
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,nil];
    
    [actionSheet showInView:self.view];
    
    
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (buttonIndex) {
        case 0:
            [self chooseFromGallery];
            break;
        case 1:
            [self takePhoto];
            break;
            
        default:
            break;
    }
}

#pragma mark- Take photo
-(void)takePhoto{
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        //  picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"device not support." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
        
    }
    
    
    
}




#pragma mark- choose from gallery
-(void)chooseFromGallery{
    
    
    if ([UIImagePickerController  isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        // picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"device not support." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
        
    }
    
    
    
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    UIImage *chosenImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    float ratio = chosenImage.size.width/chosenImage.size.height;
    
    if(btnIndex==1)
    {
        chosenImage = [self imageWithImage:chosenImage scaledToSize:CGSizeMake(177, 174/ratio)];
        image1 = nil;
        image1 = chosenImage;
        _imageView1.image=chosenImage;
        _imageView1.tag=1;
    }
    else if (btnIndex==2)
    {
        chosenImage = [self imageWithImage:chosenImage scaledToSize:CGSizeMake(177, 174/ratio)];
        image2 = nil;
        image2 = chosenImage;
        _imageView2.image=chosenImage;
        _imageView2.tag=1;
    }
    else if (btnIndex==3)
    {
        chosenImage = [self imageWithImage:chosenImage scaledToSize:CGSizeMake(177, 174/ratio)];
        image3 = nil;
        image3 = chosenImage;
        _imageView3.image=chosenImage;
        _imageView3.tag=1;
    }
    else if (btnIndex==4)
    {
        chosenImage = [self imageWithImage:chosenImage scaledToSize:CGSizeMake(177, 174/ratio)];
        image4 = nil;
        image4 = chosenImage;
        _imageView4.image=chosenImage;
        _imageView4.tag=1;
    }
    else if(btnIndex==5)
    {
        chosenImage = [self imageWithImage:chosenImage scaledToSize:CGSizeMake(177, 174/ratio)];
        image5 = nil;
        image5 = chosenImage;
        _imageView5.image=chosenImage;
        _imageView5.tag=1;
    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


#pragma mark - Image Scaling
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


-(IBAction)barBtnAction:(id)sender
{
    
    if([sender tag]==1)
    {
        
        if(!_barBtn1.selected)
        {
            _barBtn1.selected=YES;
            _barBtn1.userInteractionEnabled=FALSE;
            
            _barBtn2.selected=NO;
            _barBtn3.selected=NO;
            _barBtn4.selected=NO;
            _barBtn5.selected=NO;
            _barBtn2.userInteractionEnabled=TRUE;
            _barBtn3.userInteractionEnabled=TRUE;
            _barBtn4.userInteractionEnabled=TRUE;
            _barBtn5.userInteractionEnabled=TRUE;
            
        }
        
        
//        [self barBtnSelectDeselect:_barBtn1];
    }
    else if([sender tag]==2)
    {
        
        if(!_barBtn2.selected)
        {
            _barBtn2.selected=YES;
            _barBtn2.userInteractionEnabled=FALSE;
            
            _barBtn1.selected=NO;
            _barBtn3.selected=NO;
            _barBtn4.selected=NO;
            _barBtn5.selected=NO;
            _barBtn1.userInteractionEnabled=TRUE;
            _barBtn3.userInteractionEnabled=TRUE;
            _barBtn4.userInteractionEnabled=TRUE;
            _barBtn5.userInteractionEnabled=TRUE;
            
        }
        
        
//        [self barBtnSelectDeselect:_barBtn2];
    }
    else if([sender tag]==3)
    {
        
        if(!_barBtn3.selected)
        {
            _barBtn3.selected=YES;
            _barBtn3.userInteractionEnabled=FALSE;
            
            _barBtn2.selected=NO;
            _barBtn1.selected=NO;
            _barBtn4.selected=NO;
            _barBtn5.selected=NO;
            _barBtn2.userInteractionEnabled=TRUE;
            _barBtn1.userInteractionEnabled=TRUE;
            _barBtn4.userInteractionEnabled=TRUE;
            _barBtn5.userInteractionEnabled=TRUE;
            
        }
        
        
//        [self barBtnSelectDeselect:_barBtn3];
    }
    else if([sender tag]==4)
    {
        
        if(!_barBtn4.selected)
        {
            _barBtn4.selected=YES;
            _barBtn4.userInteractionEnabled=FALSE;
            
            _barBtn2.selected=NO;
            _barBtn3.selected=NO;
            _barBtn1.selected=NO;
            _barBtn5.selected=NO;
            _barBtn2.userInteractionEnabled=TRUE;
            _barBtn3.userInteractionEnabled=TRUE;
            _barBtn1.userInteractionEnabled=TRUE;
            _barBtn5.userInteractionEnabled=TRUE;
            
        }
        
        
//        [self barBtnSelectDeselect:_barBtn4];
    }
    else if([sender tag]==5)
    {
        
        if(!_barBtn5.selected)
        {
            _barBtn5.selected=YES;
            _barBtn5.userInteractionEnabled=FALSE;
            
            _barBtn2.selected=NO;
            _barBtn3.selected=NO;
            _barBtn4.selected=NO;
            _barBtn1.selected=NO;
            _barBtn2.userInteractionEnabled=TRUE;
            _barBtn3.userInteractionEnabled=TRUE;
            _barBtn4.userInteractionEnabled=TRUE;
            _barBtn1.userInteractionEnabled=TRUE;
            
        }
        
        
//        [self barBtnSelectDeselect:_barBtn5];
    }
    
    
    
    
}

-(void)barBtnSelectDeselect:(UIButton *)barBtn
{
    if(barBtn.selected)
    {
        barBtn.selected=NO;
    }
    else
    {
        barBtn.selected=YES;
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"uploadproductnav"]){
        
        TopNavigation *vc = (TopNavigation*)[segue destinationViewController];
        if(fromWhaereTag==2)
        {
            
            
            vc.titleString=@"PRODUCT EDIT";
            vc.fromWhere=4;
        }
        else
        {
            vc.titleString=@"PRODUCT UPLOAD";
            vc.fromWhere=3;
        }
        
        vc.delegate = self;
    }
    
}


-(IBAction)uploadProductBtnAction:(id)sender
{
    if([_nameTextField.text isEqualToString:@""])
    {
        [appDel.alertview showInfo:self title:@"Product Upload" subTitle:@"Please provide product name before saving" closeButtonTitle:@"OK" duration:0.0f];
    }
    else if([_categoryTextField.text isEqualToString:@""] || [_categoryTextField.text isEqualToString:@"PRODUCT CATEGORY"])
    {
        [appDel.alertview showInfo:self title:@"Product Upload" subTitle:@"Please provide category before saving" closeButtonTitle:@"OK" duration:0.0f];
    }
    else if([_quantityTextField.text isEqualToString:@""])
    {
        [appDel.alertview showInfo:self title:@"Product Upload" subTitle:@"Please provide product quantity before saving" closeButtonTitle:@"OK" duration:0.0f];
    }
//    else if([_productDescriptionText.text isEqualToString:@""])
//    {
//        [appDel.alertview showInfo:self title:@"Product Upload" subTitle:@"Please provide product description before saving" closeButtonTitle:@"OK" duration:0.0f];
//    }
    else if(!_choiceBtn1.selected && !_choiceBtn2.selected && !_choiceBtn3.selected)
    {
        [appDel.alertview showInfo:self title:@"Product Upload" subTitle:@"Please provide available as before saving" closeButtonTitle:@"OK" duration:0.0f];
    }
    else if(!_barBtn1.selected && !_barBtn2.selected && !_barBtn3.selected && !_barBtn4.selected && !_barBtn5.selected)
    {
        [appDel.alertview showInfo:self title:@"Product Upload" subTitle:@"Please provide base image before saving" closeButtonTitle:@"OK" duration:0.0f];
    }
    
    else
    {
        
        
        
        [_nameTextField resignFirstResponder];
        [_quantityTextField resignFirstResponder];
        [_amountTextField resignFirstResponder];
        [_productDescriptionText resignFirstResponder];
        
        imageArray=[[NSMutableArray alloc]init];
//
//        UIImageView *imgview=[[UIImageView alloc]init];
//        imgview.frame=_imageView1.bounds;
//        imgview.image=[UIImage imageNamed:@"noimages.png"];
//        
//        NSLog(@"COMPARE: %d",[self image:_imageView1.image isEqualTo:_imageView2.image]);
        
        
       
        
//        if(_imageView1.tag==1)
//        {
//            [imageArray addObject:_imageView1.image];
//        }
//        if(_imageView2.tag==1)
//        {
//            [imageArray addObject:_imageView2.image];
//        }
//        if(_imageView3.tag==1)
//        {
//            [imageArray addObject:_imageView3.image];
//        }
//        if(_imageView4.tag==1)
//        {
//            [imageArray addObject:_imageView4.image];
//        }
//        if(_imageView5.tag==1)
//        {
//            [imageArray addObject:_imageView5.image];
//        }
        
        
        
        [imageArray addObject:_imageView1.image];
        [imageArray addObject:_imageView2.image];
        [imageArray addObject:_imageView3.image];
        [imageArray addObject:_imageView4.image];
        [imageArray addObject:_imageView5.image];
        
        
        
        [self uploadProductWebService];
    }
    
        
    
    
    
}


- (BOOL)image:(UIImage *)img1 isEqualTo:(UIImage *)img2
{
    NSData *data1 = UIImagePNGRepresentation(img1);
    NSData *data2 = UIImagePNGRepresentation(img2);
    
    return [data1 isEqual:data2];
}




#pragma mark uploadProductWebService

-(void)uploadProductWebService
{
    
    NSString *choice;
    if(_choiceBtn1.selected)
    {
        choice=@"0";
    }
    else if(_choiceBtn2.selected)
    {
        choice=@"1";
    }
    else
    {
        choice=@"2";
    }
    
    
    
    NSString *coverImage;
    
    if(_barBtn1.selected)
    {
        coverImage=@"1";
    }
    else if(_barBtn2.selected)
    {
        coverImage=@"2";
    }
    else if(_barBtn3.selected)
    {
        coverImage=@"3";
    }
    else if(_barBtn4.selected)
    {
        coverImage=@"4";
    }
    else if(_barBtn5.selected)
    {
        coverImage=@"5";
    }
        
    int temp=[[NSDate date] timeIntervalSince1970];
    NSString *timestamp=[NSString stringWithFormat:@"%d",temp];
    NSLog(@"timestamp %@",timestamp);
    
    
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url;
    NSDictionary *param;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if(fromWhaereTag==2)
    {
        url = [NSString stringWithFormat:@"%@?r=api/UpdateProduct",appDel.hostUrl];
        
                
        param = [NSDictionary dictionaryWithObjectsAndKeys:product.productId,@"ProductDetails[product_id]",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],@"ProductDetails[user_id]",_nameTextField.text, @"ProductDetails[product_name]",_productDescriptionText.text,@"ProductDetails[product_desc]",choice,@"ProductDetails[product_type]",catIdString,@"cat_id",_quantityTextField.text,@"ProductDetails[quantity]",coverImage,@"cover",_amountTextField.text,@"ProductDetails[amount]",timestamp,@"ProductDetails[date_modified]" ,nil];
        
        
    }
    else
    {
        url = [NSString stringWithFormat:@"%@?r=api/UploadProduct",appDel.hostUrl];
        
        param = [NSDictionary dictionaryWithObjectsAndKeys:[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],@"ProductDetails[user_id]",_nameTextField.text, @"ProductDetails[product_name]",_productDescriptionText.text,@"ProductDetails[product_desc]",choice,@"ProductDetails[product_type]",catIdString,@"cat_id",_quantityTextField.text,@"ProductDetails[quantity]",coverImage,@"cover",_amountTextField.text,@"ProductDetails[amount]",timestamp,@"ProductDetails[date_added]" ,nil];
    }
    
    
    
    
    
//    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
    
    
    
    NSLog(@"URL: %@",url);
    
    [manager POST:url parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        
//        NSData * imgData;
//        
//        imgData = UIImagePNGRepresentation(_image2);
//        
//        if(imgData!=NULL){
//            [formData appendPartWithFileData:imgData name:@"image" fileName:@"avatar.png" mimeType:@"image/png"];
//        }
        
        
        
        
        
        
        int i = 0;
        for(UIImage *eachImage in imageArray)
        {
//            NSData *imageData = UIImageJPEGRepresentation(eachImage,0.5);
            NSData *imageData = UIImagePNGRepresentation(eachImage);
            
            NSLog(@"imageData: %@",imageData);
            
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"images[%d]",i] fileName:@"images" mimeType:@"image/jpeg"];//[NSString stringWithFormat:@"%@file%d.jpg",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],i ]
            i++;
        }
//        
        
        
//        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:imageArray];
//        [formData appendPartWithFileData:data name:@"images" fileName: @"image"mimeType:@"image/jpeg"];
        
        
//        NSMutableArray *imageDataArray=[[NSMutableArray alloc]init];
//        for(UIImage *eachImage in imageArray)
//        {
//            NSData *imageData = UIImageJPEGRepresentation(eachImage,0.5);
//            
//            [imageDataArray addObject:imageData];
//            
//        }
        
//        [formData appendPartWithFileData:imageDataArray name:@"image" fileName:@"images" mimeType:@"image/jpeg"];
        
//        [formData appendPartWithFormData:data name:@"image"];
        
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
    
    
    
    
    
    
    
    
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        
        NSLog(@"JSON: %@", responseObject);
        
        if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
        {
            
            
            
            
            if(fromWhaereTag==2)
            {
                NSLog(@"INFO:%@",[responseObject valueForKey:@"product"] );
                
                
                if(uploadTag==1)
                {
                    [delegate objectWhenPop:[responseObject valueForKey:@"product"]];
                }
                else
                {
                    
                    [delegate objectWhenPop1:[responseObject valueForKey:@"product"]];
                }
                
                
                
                
                
                
                SCLAlertView *alert = [[SCLAlertView alloc] init];
                alert.shouldDismissOnTapOutside = NO;
                alert.showAnimationType=SlideInToCenter;
                alert.hideAnimationType=SlideOutToCenter;
                [alert alertIsDismissed:^{
                    NSLog(@"SCLAlertView dismissed!");
                    
                    
//                        [self.navigationController popViewControllerAnimated:YES];
                    
                }];
                
                [alert showInfo:self title:@"Product Update" subTitle:@"Product updated successfully" closeButtonTitle:@"OK" duration:0.0f];
            }
            else
            {
//                [appDel.alertview showInfo:self title:@"Product Upload" subTitle:@"Product uploaded successfully" closeButtonTitle:@"OK" duration:0.0f];
                
                
                UIAlertView *alv=[[UIAlertView alloc]initWithTitle:@"Product Uploaded" message:@"Do you want to upload more products?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No",nil];
                [alv show];
                
                
//                _productDescriptionText.text=@"";
//                _nameTextField.text=@"";
//                _quantityTextField.text=@"";
            }
            
        }
        else
        {
            [appDel.alertview showInfo:self title:@"Product Upload" subTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]] closeButtonTitle:@"OK" duration:0.0f];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        NSLog(@"Error: %@", error);
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
        
        
    }];
    
    
    
    
    
    
    
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex != 0)
    {
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"productlist"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else
    {
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"uploadproductpush"];
        [self.navigationController pushViewController:controller animated:YES];
    }
}




#pragma mark textviewDelegates

- (void)textViewDidBeginEditing:(UITextView *)textField{
    
    
    CGRect viewFrame1 = _container.frame;
    
    

    
    
    
    
    
    
    
    CGRect viewFrame = self.view.frame;
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        viewFrame.origin.y = -80;
        viewFrame1.origin.y = 80;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        viewFrame.origin.y = -80;
        viewFrame1.origin.y = 80;
    }
    else
    {
        viewFrame.origin.y = -160;
        viewFrame1.origin.y = 160;
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.5];
    [_container setFrame:viewFrame1];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)textViewDidEndEditing:(UITextView *)textField
{
    
    
//    if(![_destText hasText]) {
//        _destText.text=@"\n\n\n\n\nDriver Notes";
//    } else if ([_destText.text length]==0) {
//        _destText.text=@"\n\n\n\n\nDriver Notes";
//    }
//    else if ([[self spaceCheck:_destText.text] length]==0)
//    {
//        _destText.text=@"\n\n\n\n\nDriver Notes";
//    }
    
    CGRect viewFrame1 = _container.frame;
    
    CGRect viewFrame = self.view.frame;
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        viewFrame.origin.y = +0;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        viewFrame.origin.y = +0;
    }
    else
    {
        viewFrame.origin.y = +0;
    }
    
    viewFrame1.origin.y = 0;
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.5];
    [_container setFrame:viewFrame1];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}


#pragma mark Textfield delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    CGRect viewFrame = self.view.frame;
    CGRect viewFrame1 = _container.frame;
    CGRect viewFrame2 = _saveBtn.frame;
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        viewFrame.origin.y = -140;
        viewFrame1.origin.y = 140;
        viewFrame2.origin.y= 140+_saveBtn.frame.origin.y;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        viewFrame.origin.y = -140;
        viewFrame1.origin.y = 140;
        viewFrame2.origin.y= 140+_saveBtn.frame.origin.y;
    }
    else
    {
        viewFrame.origin.y = -180;
        viewFrame1.origin.y = 180;
        viewFrame2.origin.y= 180+_saveBtn.frame.origin.y;
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.5];
    [_container setFrame:viewFrame1];
    [_saveBtn setFrame:viewFrame2];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGRect viewFrame = self.view.frame;
    CGRect viewFrame1 = _container.frame;
    CGRect viewFrame2 = _saveBtn.frame;
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        viewFrame.origin.y = +0;
        viewFrame2.origin.y= _saveBtn.frame.origin.y-140;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        viewFrame.origin.y = +0;
        viewFrame2.origin.y= _saveBtn.frame.origin.y-140;
    }
    else
    {
        viewFrame.origin.y = +0;
        viewFrame2.origin.y= _saveBtn.frame.origin.y-180;
    }
    
    
    viewFrame1.origin.y = 0;
//    viewFrame2.origin.y = 0-_saveBtn.frame.origin.y;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.5];
    [_container setFrame:viewFrame1];
    [_saveBtn setFrame:viewFrame2];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
