//
//  UploadProduct.h
//  Exchange
//
//  Created by Debayan Ghosh on 04/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "ALPickerView.h"
#import "ProductClass.h"

@protocol UploadProductDetegale <NSObject>

-(void)objectWhenPop:(id)productObject;
-(void)objectWhenPop1:(id)productObject;

@end

@interface UploadProduct : UIViewController<UIGestureRecognizerDelegate,SWRevealViewControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,ALPickerViewDelegate,UIAlertViewDelegate>
{
    AppDelegate *appDel;
    UITapGestureRecognizer *tap;
    int btnIndex;
    UIImage *image1,*image2,*image3,*image4,*image5;
    
    
    
    ALPickerView *pickerView;
    NSMutableArray *entries,*catIds;
    NSMutableDictionary *selectionStates;
    NSMutableArray *finalList,*finalIdList;
    NSMutableArray *imageArray;
    
    
}
@property (nonatomic,retain) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIView *container;
@property (nonatomic, weak) id<UploadProductDetegale> delegate;
@property (nonatomic,retain) ProductClass *product;
@property int fromWhaereTag, uploadTag;
@property (nonatomic,retain)IBOutlet UIImageView *imageView1, *imageView2, *imageView3, *imageView4, *imageView5;
@property (nonatomic,retain)IBOutlet UIButton *barBtn1, *barBtn2, *barBtn3, *barBtn4, *barBtn5;
@property (nonatomic,retain)IBOutlet UITextView *productDescriptionText;
@property (nonatomic,retain) IBOutlet UITextField *categoryTextField, *nameTextField, *quantityTextField, *amountTextField;
@property (nonatomic,retain) IBOutlet UIButton *choiceBtn1,*choiceBtn2,*choiceBtn3;
@property (nonatomic,retain) IBOutlet UIImageView *choiceImg1,*choiceImg2,*choiceImg3;
@property (nonatomic,retain) IBOutlet UIScrollView *scroll;

@property (nonatomic,retain) IBOutlet UILabel *amountLabel;
@property (nonatomic,retain) IBOutlet UIImageView *amountTextImageView;

@end
