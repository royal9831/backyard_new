//
//  ViewController.m
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "SCLAlertView.h"
#import "Map.h"
#import "SWRevealViewController.h"
#import "MapSearch.h"
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 200;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 100;
@interface ViewController ()

@end

@implementation ViewController
@synthesize locationManager;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _nameText.returnKeyType = UIReturnKeyDone;
    _emailText.returnKeyType = UIReturnKeyDone;
    _passwordText.returnKeyType = UIReturnKeyDone;
    _phoneText.returnKeyType = UIReturnKeyDone;
    
    
  
    
    
    if([ [ UIScreen mainScreen ] bounds ].size.height==480)
    {
        _backgroundView.frame=CGRectMake(0, 0, 320, 568);
        
    }
    
    
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    _demandBtn.selected=NO;
    _supplierBtn.selected=NO;
    _demandBtn.userInteractionEnabled=TRUE;
    _supplierBtn.userInteractionEnabled=TRUE;
    _radioBtn1.image=[UIImage imageNamed:@"radioBtn2.png"];
    _radioBtn2.image=[UIImage imageNamed:@"radioBtn2.png"];
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    
    self.locationManager.delegate = self;
    if(IS_OS_8_OR_LATER){
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                [self.locationManager requestAlwaysAuthorization];
            } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                [self.locationManager  requestWhenInUseAuthorization];
            } else {
                NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
            }
        }
    }
    [self.locationManager startUpdatingLocation];
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self.locationManager stopUpdatingLocation];
}
- (IBAction)backBtnAction:(id)sender {
    
    
    
    MapSearch *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"mapsearchviewpush"];
//    controller.fromTag=1;
    
//    [self.navigationController pushViewController:controller animated:YES];
    
    
    revealTag=2;
    [self performSegueWithIdentifier:@"reveal" sender:self];
    
    
    
//    [self performSegueWithIdentifier:@"mapviewpush" sender:self];
    
//    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)supplierBtnAction:(id)sender {
    
    _demandBtn.selected=NO;
    _supplierBtn.selected=YES;
    _demandBtn.userInteractionEnabled=TRUE;
    _supplierBtn.userInteractionEnabled=FALSE;
    _radioBtn1.image=[UIImage imageNamed:@"radioBtn1.png"];
    _radioBtn2.image=[UIImage imageNamed:@"radioBtn2.png"];

    
}
- (IBAction)demandBtnAction:(id)sender {
    
    _demandBtn.selected=YES;
    _supplierBtn.selected=NO;
    _demandBtn.userInteractionEnabled=FALSE;
    _supplierBtn.userInteractionEnabled=TRUE;
    _radioBtn1.image=[UIImage imageNamed:@"radioBtn2.png"];
    _radioBtn2.image=[UIImage imageNamed:@"radioBtn1.png"];

}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_nameText resignFirstResponder];
    [_emailText resignFirstResponder];
    [_passwordText resignFirstResponder];
    [_phoneText resignFirstResponder];
    
    
    
}

-(IBAction)registerBtnAction:(id)sender
{
    [_nameText resignFirstResponder];
    [_emailText resignFirstResponder];
    [_passwordText resignFirstResponder];
    [_phoneText resignFirstResponder];
//    [self performSegueWithIdentifier:@"loginpush" sender:self];
//    [self.navigationController popViewControllerAnimated:YES];
    
    
    if([[self checkNull:_nameText.text] isEqualToString:@""] || [[self checkNull:_passwordText.text] isEqualToString:@""] || [[self checkNull:_emailText.text] isEqualToString:@""])
    {
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Please provide name, email id and password" closeButtonTitle:@"OK" duration:0.0f];
        
    }
    else if([self validateEmail:[_emailText text]] !=1)
    {
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Invalid email id" closeButtonTitle:@"OK" duration:0.0f];
    }
//    else if(!_supplierBtn.selected && !_demandBtn.selected)
//    {
//        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Please select the user type" closeButtonTitle:@"OK" duration:0.0f];
//    }
    else
    {
        [self registrationWebService];
    }
    
    
    
    
}

#pragma mark registrationWebService

-(void)registrationWebService
{
    
    
    NSString *userType;
//    if(_supplierBtn.selected)
//    {
//        userType=@"supplier";
//    }
//    else
//    {
        userType=@"user";
//    }
    
    
    
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/Registration",appDel.hostUrl];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:                           _emailText.text,@"User[email]",_passwordText.text,@"User[password]",_nameText.text,@"Profile[name]",lat,@"Profile[lat]",lon,@"Profile[long]",appDel.token,@"Profile[device_token]",@"IOS",@"Profile[token_type]",userType,@"User[role]",_phoneText.text,@"Profile[phone_no]",locationAddress,@"Profile[address]",zip,@"Profile[zip]",
                           nil];
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        
        NSLog(@"JSON: %@", responseObject);
        
        if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"response"]] isEqualToString:@"1"])
        {
            
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            alert.shouldDismissOnTapOutside = NO;
            alert.showAnimationType=SlideInToCenter;
            alert.hideAnimationType=SlideOutToCenter;
            [alert alertIsDismissed:^{
                NSLog(@"SCLAlertView dismissed!");
                
                
//                [self.navigationController popViewControllerAnimated:YES];
                
                [self loginWebservice];
                
//                [self performSegueWithIdentifier:@"reveal" sender:self];
                
            }];
            
            [alert showInfo:self title:@"Registration" subTitle:@"Registration successful" closeButtonTitle:@"OK" duration:0.0f];
        }
        else
        {
            [appDel.alertview showInfo:self title:@"Registration" subTitle:[responseObject valueForKey:@"message"] closeButtonTitle:@"OK" duration:0.0f];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        NSLog(@"Error: %@", error);
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
        
        
        
    }];

    
    
    
    
    
    
}



#pragma mark Loginservice

-(void)loginWebservice
{
    
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/Login",appDel.hostUrl];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:                           _emailText.text,@"UserLogin[username]",_passwordText.text,@"UserLogin[password]",appDel.token,@"Profile[device_token]",@"IOS",@"Profile[token_type]",
                           nil];
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        
        NSLog(@"JSON: %@", responseObject);
        
        if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"response"]] isEqualToString:@"1"])
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[responseObject valueForKey:@"details"] forKey:@"userdetails"];
            [defaults setObject:[responseObject valueForKey:@"settings"] forKey:@"usersettings"];
            [defaults setObject:[responseObject valueForKey:@"backyard"] forKey:@"backyard"];
            [defaults setObject:@"1" forKey:@"status"];
            [defaults synchronize];
            revealTag=1;
            [self performSegueWithIdentifier:@"reveal" sender:self];
        }
        else
        {
//            [appDel.alertview showInfo:self title:@"Login" subTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]] closeButtonTitle:@"OK" duration:0.0f];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        NSLog(@"Error: %@", error);
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
        
        
    }];
    
    
    
    
    
    
    
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"reveal"])
    {
        
        SWRevealViewController *vc = (SWRevealViewController*)[segue destinationViewController];
        vc.revealTag=revealTag;
        
    }
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    
    
    zip=@"741235";
    locationAddress=@"Kalyani";
    lat=@"22.365486";
    lon=@"88.214563";
    
    
//    UIAlertView *errorAlert = [[UIAlertView alloc]
//                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        lon = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        lat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        
        [locationManager stopUpdatingLocation];
        
//        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
//        [geocoder reverseGeocodeLocation:locationManager.location
//                       completionHandler:^(NSArray *placemarks, NSError *error) {
//                           NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
//                           
//                           if (error){
//                               NSLog(@"Geocode failed with error: %@", error);
//                               return;
//                               
//                           }
//                           
//                           NSLog(@"placemarks=%@",[placemarks objectAtIndex:0]);
//                           CLPlacemark *placemark = [placemarks objectAtIndex:0];
//                           
//                           NSLog(@"placemark.ISOcountryCode =%@",placemark.ISOcountryCode);
//                           NSLog(@"placemark.country =%@",placemark.country);
//                           NSLog(@"placemark.postalCode =%@",placemark.postalCode);
//                           NSLog(@"placemark.administrativeArea =%@",placemark.administrativeArea);
//                           NSLog(@"placemark.locality =%@",placemark.locality);
//                           NSLog(@"placemark.subLocality =%@",placemark.subLocality);
//                           NSLog(@"placemark.subThoroughfare =%@",placemark.subThoroughfare);
//                           
//                           locationAddress=[NSString stringWithFormat:@"%@ %@ %@ %@",placemark.administrativeArea,placemark.locality,placemark.subLocality,placemark.country];
//                           
//                           zip=placemark.postalCode;
//                           
//                           NSLog(@"locationAddress %@",locationAddress);
//                           
//                       }];
//        
//
        
        
        
        
        ;
        
//        NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps/geo?q=%f,%f&output=csv",currentLocation.coordinate.latitude, currentLocation.coordinate.longitude];//AIzaSyBTMbVNi8fLA-dzUQ61L0QbeociKndR8aQ
        
        
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=false&key=%@",currentLocation.coordinate.latitude, currentLocation.coordinate.longitude,appDel.googleApiKey];//AIzaSyBTMbVNi8fLA-dzUQ61L0QbeociKndR8aQ
        
        
        NSError* error;
        NSString *locationString = [NSString stringWithContentsOfURL:[NSURL URLWithString:urlString] encoding:NSASCIIStringEncoding error:&error];
        
        NSData *data = [locationString dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        
        NSLog(@"LOCA: %@",[json objectForKey:@"results"]);
        
        
        NSDictionary *dic = [[json objectForKey:@"results"] objectAtIndex:0];
        //    NSString *cityName = [[[dic objectForKey:@"address_components"] objectAtIndex:1] objectForKey:@"long_name"];
        NSLog(@"DICT: %@",[dic valueForKey:@"formatted_address"]);
        
        locationAddress=[dic valueForKey:@"formatted_address"];
        
        
        
        
        
        
        
        
    }
}




#pragma mark Textfield delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    CGRect viewFrame = self.view.frame;
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        viewFrame.origin.y = -80;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        viewFrame.origin.y = -90;
    }
    else
    {
        viewFrame.origin.y = -150;
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.5];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGRect viewFrame = self.view.frame;
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        viewFrame.origin.y = +0;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        viewFrame.origin.y = +0;
    }
    else
    {
        viewFrame.origin.y = +0;
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.5];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


-(NSString *)checkNull :(NSString *)str
{
    NSString *InputString=str;
    if( (InputString == nil) ||(InputString ==(NSString *)[NSNull null])||([InputString isEqual:nil])||([InputString length] == 0)||[allTrim( InputString ) length] == 0||([InputString isEqualToString:@""])||([InputString isEqualToString:@"(NULL)"])||([InputString isEqualToString:@"<NULL>"])||([InputString isEqualToString:@"<null>"]||([InputString isEqualToString:@"(null)"])||([InputString isEqualToString:@""]))
       
       )
        return @"";
    else
        return str ;
    
}

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    //	return 0;
    return [emailTest evaluateWithObject:candidate];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
