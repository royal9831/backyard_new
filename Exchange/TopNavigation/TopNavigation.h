//
//  TopNavigation.h
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SWRevealViewController.h"
@protocol ProfileViewPageDetegale <NSObject>

-(void)didTapMenuButton;

@end
@interface TopNavigation : UIViewController<SWRevealViewControllerDelegate,UIGestureRecognizerDelegate>
{
    AppDelegate *appDel;
    UITapGestureRecognizer *tap;
}
@property (nonatomic, weak) id<ProfileViewPageDetegale> delegate;
@property (nonatomic,retain) NSString *titleString;
@property int fromWhere,backgroundImageTag;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn,*backBtn;
@property (weak, nonatomic) IBOutlet UIImageView *rightBtnImage, *leftBtnImage, *backgroundImageView, *backBtnImage;
@end
