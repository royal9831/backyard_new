//
//  TopNavigation.m
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "TopNavigation.h"

@interface TopNavigation ()<UIViewControllerTransitioningDelegate>

@end

@implementation TopNavigation
@synthesize titleString,fromWhere,backgroundImageTag;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    _titleLabel.text=titleString;
    
    NSLog(@"fromWhere %d",fromWhere);
    
    if(fromWhere == 0)
    {
        _leftButton.hidden=YES;
        _leftBtnImage.hidden=YES;
        _backBtn.hidden=YES;
    }
    
    if(fromWhere == 1)
    {
        _leftButton.hidden=YES;
        _leftBtnImage.hidden=YES;
        _rightBtn.hidden=YES;
        _rightBtnImage.hidden=YES;
        _backBtn.hidden=YES;
        _backBtnImage.hidden=YES;
        
    }
    
    if(fromWhere == 2)
    {
        _leftButton.hidden=NO;
        _leftBtnImage.hidden=NO;
        _rightBtn.hidden=NO;
        _rightBtnImage.hidden=NO;
        _backBtn.hidden=YES;
        _backBtnImage.hidden=YES;
    }
    
    if(fromWhere == 3)
    {
        _leftButton.hidden=NO;
        _leftBtnImage.hidden=NO;
        _rightBtn.hidden=YES;
        _rightBtnImage.hidden=YES;
        _backBtn.hidden=YES;
        _backBtnImage.hidden=YES;
    }
    if(fromWhere == 4)
    {
        _leftButton.hidden=YES;
        _leftBtnImage.hidden=YES;
        _rightBtn.hidden=YES;
        _rightBtnImage.hidden=YES;
        _backBtn.hidden=NO;
        _backBtnImage.hidden=NO;
    }
    
    
//    if(backgroundImageTag==1)
//    {
//        _backgroundImageView.alpha=0.8;
//    }
//    else
//    {
//        _backgroundImageView.alpha=1.0;
//    }
    
    
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    [_leftButton addTarget:self.revealViewController action:@selector(revealToggle:)forControlEvents:UIControlEventTouchUpInside];
    
//    SWRevealViewController *revealController = [self revealViewController];
//    self.revealViewController.delegate = self;
//    tap = [revealViewController tapGestureRecognizer];
//    tap.delegate = self;
//    tap.enabled=NO;
//    [self.view addGestureRecognizer:tap];


}





- (IBAction)didClickMenuButton:(id)sender {
    
  
}

-(IBAction)backBtnAction:(id)sender
{
//    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
