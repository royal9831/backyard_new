//
//  AppDelegate.h
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//RobotoCondensed-Regular 15.0      DIN Condensed Bold 15.0
// Debayan Ghosh



/*
 
 {
 backyard =     //backyard
 (
         {
         "backyard_image" = "0.08172100 1438845811-Koala.jpg";
         "date_added" = "2015-08-06 12:53:31";
         "image_id" = 4;
         "user_id" = 18;
         },
         {
         "backyard_image" = "fa6d5859781ade06fdfb77158127bf65-Koala.jpg";
         "date_added" = "2015-08-06 12:54:39";
         "image_id" = 5;
         "user_id" = 18;
         }
);
 
 details =     //userdetails
     {
     email = "santu.rana@businessprodesigns.com";
     lat = "40.701229";
     long = "-111.949226";
     name = santu;
     "phone_no" = 123487934654;
     "profile_image" = "http://dev1.businessprodemo.com/Backyard/php/images/profile/4640_nilssonpolias.jpg";
     role = user;
     status = 1;
     "user_id" = 18;
     zip = "";
     };
 
 
 message = "";
 response = 1;
 
 
 settings =     //usersettings
     {
     "category_id" = 1;
     "preference_id" = 12;
     quantity = 1;
     radius = 88;
     status = 1;
     "user_id" = 18;
     };
 }

 
 
 
 */


#import <UIKit/UIKit.h>
#import "SCLAlertView.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) NSString *hostUrl;
@property (strong, nonatomic) UIWindow *window;
@property int sidebarStatus,lastValue;
-(SCLAlertView *)alertview;
@property (strong,nonatomic) NSString *AccessTocken, *token;
@property(nonatomic,retain) NSData *pushNotificationVal;
@property (nonatomic,retain) NSString *googleApiKey;
@property int allPickerTag;
@end

