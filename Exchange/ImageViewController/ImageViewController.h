//
//  ImageViewController.h
//  Exchange
//
//  Created by Debayan Ghosh on 09/11/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController<UIScrollViewDelegate>
@property (nonatomic,retain) NSString *imageUrl;
@end
