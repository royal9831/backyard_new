//
//  RequestList.h
//  Exchange
//
//  Created by Debayan Ghosh on 01/09/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "AppDelegate.h"
@interface RequestList : UIViewController<UIGestureRecognizerDelegate,SWRevealViewControllerDelegate>
{
    AppDelegate *appDel;
    UITapGestureRecognizer *tap;
     NSMutableArray *requestArray;
}
@property (nonatomic,retain) IBOutlet UITableView *table;
@property (nonatomic,retain) IBOutlet UIView *backgroundView;
@end
