//
//  RequestListCell.h
//  Exchange
//
//  Created by Debayan Ghosh on 01/09/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestListCell : UITableViewCell
@property (nonatomic,retain) IBOutlet UIView *cellView;
@property (nonatomic,retain) IBOutlet UILabel *demand_Name,*demand_Email,*demand_Address;
@property (nonatomic,retain) IBOutlet UILabel *product_Name, *product_Quantity,*product_Date;
@property (nonatomic,retain) IBOutlet UIButton *acceptBtn, *declineBtn;
@end
