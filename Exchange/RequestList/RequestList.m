//
//  RequestList.m
//  Exchange
//
//  Created by Debayan Ghosh on 01/09/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "RequestList.h"
#import "UploadProduct.h"
#import "TopNavigation.h"
#import "SCLAlertView.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "UIImage+RoundedImage.h"
#import "RequestListCell.h"
#import "RequestClass.h"
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
@interface RequestList ()<UIViewControllerTransitioningDelegate,ProfileViewPageDetegale>

@end

@implementation RequestList

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    requestArray=[[NSMutableArray alloc]init];
    
    if([ [ UIScreen mainScreen ] bounds ].size.height==480)
    {
        _backgroundView.frame=CGRectMake(0, 0, 320, 568);
        _table.frame=CGRectMake(0, 64, 320, 480-64);
        
    }
    
    
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    SWRevealViewController *revealController = [self revealViewController];
    self.revealViewController.delegate = self;
    tap = [revealController tapGestureRecognizer];
    tap.delegate = self;
    tap.enabled=NO;
    [self.view addGestureRecognizer:tap];
    
    [self requestListWebservice];
    
}





-(NSString *)dateConversion:(NSString *)dateString
{
    ///////////////////////////////////////////
    
//    NSDateFormatter *utcDateFormatter = [[NSDateFormatter alloc] init] ;
//    [utcDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//New modified
//    [utcDateFormatter setTimeZone :[NSTimeZone timeZoneForSecondsFromGMT: 0]];
//    
//    // utc format
//    NSDate *dateInUTC = [utcDateFormatter dateFromString: dateString];
//    
//    // offset second
//    NSInteger seconds = [[NSTimeZone systemTimeZone] secondsFromGMT];
//    
//    // format it and send
//    NSDateFormatter *localDateFormatter = [[NSDateFormatter alloc] init] ;
//    [localDateFormatter setDateFormat:@"HH mm a 'I' dd MM yyyy"];//New modified
//    
//    [localDateFormatter setTimeZone :[NSTimeZone timeZoneForSecondsFromGMT: seconds]];
//    
//    // formatted string
//    NSString *localDate = [localDateFormatter stringFromDate: dateInUTC];
//    
//    
//    
//    return localDate;
    
    
    
    
    double unixTimeStamp =[dateString doubleValue];
    NSTimeInterval _interval=unixTimeStamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"HH mm a 'I' dd MM yyyy"];
    NSString *convertedDateString = [formatter stringFromDate:date];
    
    
    
    return convertedDateString;
    
    
    
    
    //////////////////////////////////////////
}








-(void)requestListWebservice
{
    
    
    
    
    
    
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/MyRequests",appDel.hostUrl];
    
    NSLog(@"ID %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"]);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],@"user_id",
                           nil];
    
    //    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    //    [manager.requestSerializer setValue:@"calvinAndHobbessRock" forHTTPHeaderField:@"X-I do what I want"];
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         
         NSLog(@"JSON: %@", responseObject);
         
         if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
         {
             
             
             requestArray=[[NSMutableArray alloc]init];
             for(int i=0;i<[[responseObject valueForKey:@"requestList"] count];i++)
             {
                 
                 RequestClass *request=[[RequestClass alloc]init];
                 
                 request.product_id=[[[responseObject valueForKey:@"requestList"] objectAtIndex:i] valueForKey:@"product_id"];
                 request.product_name=[[[responseObject valueForKey:@"requestList"] objectAtIndex:i] valueForKey:@"product_name"];
                 request.req_id=[[[responseObject valueForKey:@"requestList"] objectAtIndex:i] valueForKey:@"req_id"];
                 request.request_date=[[[responseObject valueForKey:@"requestList"] objectAtIndex:i] valueForKey:@"request_date"];
                 request.status=[[[responseObject valueForKey:@"requestList"] objectAtIndex:i] valueForKey:@"status"];
                 request.swap_name=[[[responseObject valueForKey:@"requestList"] objectAtIndex:i] valueForKey:@"swap_name"];
                 request.swap_quantity=[[[responseObject valueForKey:@"requestList"] objectAtIndex:i] valueForKey:@"swap_quantity"];
                 request.user_id=[[[responseObject valueForKey:@"requestList"] objectAtIndex:i] valueForKey:@"user_id"];
                 request.demandUserDict=[[[responseObject valueForKey:@"requestList"] objectAtIndex:i] valueForKey:@"demand_user"];
                 
//
                 
                 [requestArray addObject:request];
                 
                 
                 request=nil;
                 
                 
                 
             }
             
             
             if([requestArray count]<1)
             {
                 [appDel.alertview showInfo:self title:@"Request" subTitle:@"No request available" closeButtonTitle:@"OK" duration:0.0f];
             }
             
             
             [_table reloadData];
         }
         else
         {
             [appDel.alertview showInfo:self title:@"Request" subTitle:@"No request available" closeButtonTitle:@"OK" duration:0.0f];
         }
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         NSLog(@"Error: %@", error);
         
         [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
         
         
     }];
    
    
    
    
    
    
    
    
    
}





#pragma mark tableview delegates


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [requestArray count];
}
-(UITableViewCell *)tableView: (UITableView *)tableView cellForRowAtIndexPath: (NSIndexPath *)indexPath{
    
    
    
    static  NSString *CellIdentifier = @"requestcell";
    RequestListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[RequestListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    
    
    if(indexPath.row%2==0)
    {
        cell.cellView.backgroundColor=[UIColor colorWithRed:206.0/255 green:206.0/255 blue:206.0/255 alpha:0.8];
    }
    else
    {
        cell.cellView.backgroundColor=[UIColor colorWithRed:223.0/255 green:223.0/255 blue:223.0/255 alpha:0.8];
    }
    
    
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        
        
        cell.cellView.frame=CGRectMake(0, 0, 375, 205);
        
        
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        
        cell.cellView.frame=CGRectMake(0, 0, 414, 205);
        
       
        
    }
    else
    {
        cell.cellView.frame=CGRectMake(0, 0, 320, 205);
        
    }
    
    RequestClass *request=[[RequestClass alloc]init];
    request=[requestArray objectAtIndex:indexPath.row];
    cell.demand_Name.text=[request.demandUserDict valueForKey:@"name"];
    cell.demand_Email.text=[request.demandUserDict valueForKey:@"email"];
    cell.demand_Address.text=[request.demandUserDict valueForKey:@"address"];

    cell.product_Quantity.text=request.swap_quantity;
    cell.product_Name.text=request.swap_name;
    cell.product_Date.text=request.request_date;//[self dateConversion:request.request_date];
    
    
    
    
    [cell.acceptBtn addTarget:self action:@selector(acceptBtnAction:)forControlEvents:UIControlEventTouchUpInside];
    cell.acceptBtn.tag=indexPath.row;
    [cell.declineBtn addTarget:self action:@selector(declineBtnAction:)forControlEvents:UIControlEventTouchUpInside];
    cell.declineBtn.tag=indexPath.row;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}



-(IBAction)acceptBtnAction:(id)sender
{
    RequestClass *request=[[RequestClass alloc]init];
    request=[requestArray objectAtIndex:[sender tag]];
    
    [self acceptDeclineWebservice:@"1":request.req_id:[sender tag]];
}


-(IBAction)declineBtnAction:(id)sender
{
    RequestClass *request=[[RequestClass alloc]init];
    request=[requestArray objectAtIndex:[sender tag]];
    
    [self acceptDeclineWebservice:@"2":request.req_id:[sender tag]];
}


-(void)acceptDeclineWebservice:(NSString *)type :(NSString *)reqId :(int)requestTag
{
    
    int temp=[[NSDate date] timeIntervalSince1970];
    NSString *timestamp=[NSString stringWithFormat:@"%d",temp];
    NSLog(@"timestamp %@",timestamp);
    
    
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/ChangeRequestsStatus",appDel.hostUrl];
    
    NSLog(@"ID %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"]);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:reqId,@"req_id",type,@"status",timestamp,@"accept_date",
                           nil];
    
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         
         NSLog(@"JSON: %@", responseObject);
         
         if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
         {
             if([type isEqualToString:@"1"])
             {
                 [appDel.alertview showInfo:self title:@"Request" subTitle:@"Request accepted" closeButtonTitle:@"OK" duration:0.0f];
             }
             else
             {
                 [appDel.alertview showInfo:self title:@"Request" subTitle:@"Request declined" closeButtonTitle:@"OK" duration:0.0f];
             }
             
             
             [requestArray removeObjectAtIndex:requestTag];
             
             [_table reloadData];
             
             
             
            
         }
         else
         {
             if([type isEqualToString:@"1"])
             {
                 [appDel.alertview showInfo:self title:@"Request" subTitle:@"Request accepted failed" closeButtonTitle:@"OK" duration:0.0f];
             }
             else
             {
                 [appDel.alertview showInfo:self title:@"Request" subTitle:@"Request declined failed" closeButtonTitle:@"OK" duration:0.0f];
             }
             
             
         }
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         NSLog(@"Error: %@", error);
         
         [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
         
         
     }];
    
    
    

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
//    {
//        return 223.0;
//    }
//    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
//    {
//        return 223.0;
//    }
//    else
//    {
//        return 190.0;
//    }
    
    return 206.0;
}





- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"requestlistnav"]){
        
        TopNavigation *vc = (TopNavigation*)[segue destinationViewController];
        vc.titleString=@"REQUEST LISTING";
        vc.fromWhere=3;
        vc.delegate = self;
        
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
