//
//  ProductDetails.m
//  Exchange
//
//  Created by Debayan Ghosh on 17/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "ProductDetails.h"
#import "TopNavigation.h"
#import "UIImageView+WebCache.h"
#import "UIImage+RoundedImage.h"
#import "UIImageEffects.h"
#import "UploadProduct.h"
#import "ImageViewController.h"
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

@interface ProductDetails ()<UIViewControllerTransitioningDelegate,ProfileViewPageDetegale>

@end

@implementation ProductDetails
@synthesize product;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"product:%@",product.productName);
    
    if([ [ UIScreen mainScreen ] bounds ].size.height==480)
    {
        _backgroundView.frame=CGRectMake(0, 0, 320, 568);
        
    }
    
    
    [self productDetails];
    
}

-(void)productDetails
{
    
//    NSDateFormatter *df=[[NSDateFormatter alloc]init];
//    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSDate *convertedDate=[df dateFromString:product.productTime];
//    NSLog(@"convertedDate %@",convertedDate);
//    [df setDateFormat:@"HH mm a | dd MM yyyy"];
//    
//    NSString *dateString=[df stringFromDate:convertedDate];
    
    
    
    
    NSString *name=product.username;
    NSString *address=product.useraddress;
    
    _nameLabel.text=name;
    _addressLabel.text=address;
    _descriptionLabel.text=product.productDesc;
    _productQuantityLabel.text=product.productQuantity;
    _productTimeLabel.text=[self dateConversion:product.productTime];
    _productNameLabel.text=product.productName;
    
    
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    for(int i=0;i<[product.imageList count];i++)
    {
        if([[[product.imageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[product.imageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[product.imageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    [self makeRoundImage:_imageview1];
    [self makeRoundImage:_imageview2];
    [self makeRoundImage:_imageview3];
    [self makeRoundImage:_imageview4];
    [self makeRoundImage:_imageview5];
    
    NSString *url2=[noncoverimages objectAtIndex:0];
    NSString *url3=[noncoverimages objectAtIndex:1];
    NSString *url4=[noncoverimages objectAtIndex:2];
    NSString *url5=[noncoverimages objectAtIndex:3];
    
    
    
    
    [_imageview1 sd_setImageWithURL:[NSURL URLWithString:url1]
                       placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    [_imageview2 sd_setImageWithURL:[NSURL URLWithString:url2]
                       placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    [_imageview3 sd_setImageWithURL:[NSURL URLWithString:url3]
                       placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    [_imageview4 sd_setImageWithURL:[NSURL URLWithString:url4]
                       placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    [_imageview5 sd_setImageWithURL:[NSURL URLWithString:url5]
                       placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    
    
    
    
    
}



- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    NSArray *viewControllers = self.navigationController.viewControllers;
    if (viewControllers.count > 1 && [viewControllers objectAtIndex:viewControllers.count-2] == self) {
        // View is disappearing because a new view controller was pushed onto the stack
        NSLog(@"New view controller was pushed");
    } else if ([viewControllers indexOfObject:self] == NSNotFound) {
        // View is disappearing because it was popped from the stack
        NSLog(@"View controller was popped");
        
        
        [_delegate objectWhenPop2:product];
        
        
    }
}


-(IBAction)button1Action:(id)sender
{
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    for(int i=0;i<[product.imageList count];i++)
    {
        if([[[product.imageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[product.imageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[product.imageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    controller.imageUrl=url1;
    [self.navigationController pushViewController:controller animated:NO];
    
    
}

-(IBAction)button2Action:(id)sender
{
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    for(int i=0;i<[product.imageList count];i++)
    {
        if([[[product.imageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[product.imageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[product.imageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    
//    NSURL *imageURL = [NSURL URLWithString:[noncoverimages objectAtIndex:0]];
//    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
//    UIImage *image = [UIImage imageWithData:imageData];
//    
//    
//    NSLog(@"IMAGE MATCH: %d",[self image:[noncoverimages objectAtIndex:0] isEqualTo:[UIImage imageNamed:@"noimages.png"]]);
    
    
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    controller.imageUrl=[noncoverimages objectAtIndex:0];
    [self.navigationController pushViewController:controller animated:NO];
}

-(IBAction)button3Action:(id)sender
{
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    for(int i=0;i<[product.imageList count];i++)
    {
        if([[[product.imageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[product.imageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[product.imageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    
    
//    NSURL *imageURL = [NSURL URLWithString:[noncoverimages objectAtIndex:1]];
//    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
//    UIImage *image = [UIImage imageWithData:imageData];
//    
//    
//    NSLog(@"IMAGE MATCH: %d",[self image:[noncoverimages objectAtIndex:1] isEqualTo:[UIImage imageNamed:@"noimages.png"]]);
    
    
    
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    controller.imageUrl=[noncoverimages objectAtIndex:1];
    [self.navigationController pushViewController:controller animated:NO];
}

-(IBAction)button4Action:(id)sender
{
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    for(int i=0;i<[product.imageList count];i++)
    {
        if([[[product.imageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[product.imageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[product.imageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    controller.imageUrl=[noncoverimages objectAtIndex:2];
    [self.navigationController pushViewController:controller animated:NO];
}

-(IBAction)button5Action:(id)sender
{
    
    
    
    
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    for(int i=0;i<[product.imageList count];i++)
    {
        if([[[product.imageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[product.imageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[product.imageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    
//    NSURL *imageURL = [NSURL URLWithString:[noncoverimages objectAtIndex:3]];
//    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
//    UIImage *image = [UIImage imageWithData:imageData];
//    
//    
//    NSLog(@"IMAGE MATCH: %d",[self image:[UIImage imageNamed:@"noimages.png"] isEqualTo:image]);
    
    
    
    
    
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    controller.imageUrl=[noncoverimages objectAtIndex:3];
    [self.navigationController pushViewController:controller animated:NO];
}




- (BOOL)image:(NSString *)image1 isEqualTo:(UIImage *)image2
{
    
//    NSData *data1 = UIImageJPEGRepresentation(image1,0.5);
//    NSData *data2 = UIImageJPEGRepresentation(image2,1.0);
    
    NSURL *imageURL = [NSURL URLWithString:image1];
    NSData *data1 = [NSData dataWithContentsOfURL:imageURL];
    NSData *data2 = UIImageJPEGRepresentation(image2,0.5);
    
    
    return [data1 isEqual:data2];
}





-(NSString *)dateConversion:(NSString *)dateString
{
//    ///////////////////////////////////////////
//    
//    NSDateFormatter *utcDateFormatter = [[NSDateFormatter alloc] init] ;
//    [utcDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//New modified
//    [utcDateFormatter setTimeZone :[NSTimeZone timeZoneForSecondsFromGMT: 0]];
//    
//    // utc format
//    NSDate *dateInUTC = [utcDateFormatter dateFromString: dateString];
//    
//    // offset second
//    NSInteger seconds = [[NSTimeZone systemTimeZone] secondsFromGMT];
//    
//    // format it and send
//    NSDateFormatter *localDateFormatter = [[NSDateFormatter alloc] init] ;
//    [localDateFormatter setDateFormat:@"HH mm a 'I' dd MM yyyy"];//New modified
//    
//    [localDateFormatter setTimeZone :[NSTimeZone timeZoneForSecondsFromGMT: seconds]];
//    
//    // formatted string
//    NSString *localDate = [localDateFormatter stringFromDate: dateInUTC];
    
   
    double unixTimeStamp =[dateString doubleValue];
    NSTimeInterval _interval=unixTimeStamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"hh:mma 'I' MMM dd, yyyy"];
    NSString *convertedDateString = [formatter stringFromDate:date];
    
//    a. “11:53PM | Oct 24, 2015”
    
    return convertedDateString;
    
    
    //////////////////////////////////////////
}


-(IBAction)editBtnAction:(id)sender
{
    
    
    
    UploadProduct *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"uploadproductpush"];
    controller.fromWhaereTag=2;
    controller.product=product;
    controller.delegate=(id)self;
//    controller.delegate=(id)_delegate;
    controller.uploadTag=2;
    
    [self.navigationController pushViewController:controller animated:YES];
}


-(void)objectWhenPop1:(id)productObject
{
    
    
    
    product=[[ProductClass alloc]init];
    
    product.productId=[productObject valueForKey:@"product_id"];
    product.productName=[productObject valueForKey:@"product_name"];
    product.productQuantity=[productObject valueForKey:@"quantity"];;
    product.productDesc=[productObject valueForKey:@"product_desc"];
    product.imageList=[productObject valueForKey:@"images"];
    product.productTime=[productObject valueForKey:@"date_added"];
    product.productType=[productObject valueForKey:@"product_type"];
    product.productAmount=[productObject valueForKey:@"amount"];
    product.categoriesName=[productObject valueForKey:@"category"];
    product.categoriesIds=[productObject valueForKey:@"catid"];
    
    
    product.username=[[productObject valueForKey:@"owner"] valueForKey:@"name"];
    product.useraddress=[[productObject valueForKey:@"owner"] valueForKey:@"address"];
    product.email=[[productObject valueForKey:@"owner"] valueForKey:@"email"];
    
    
    
    
    
    
    
    
//    product=productObject;
    [self productDetails];
    
    
    
    
    
//    NSLog(@"productObject: %@",productObject);
//    
//    for(int i=0;i<[productArray count];i++)
//    {
//        
//        ProductClass *product1=[[ProductClass alloc]init];
//        product1=[productArray objectAtIndex:i];
//        
//        if([product1.productId isEqualToString:[productObject valueForKey:@"product_id"]])
//        {
//            ProductClass *product=[[ProductClass alloc]init];
//            
//            product.productId=[productObject valueForKey:@"product_id"];
//            product.productName=[productObject valueForKey:@"product_name"];
//            product.productQuantity=[productObject valueForKey:@"quantity"];;
//            product.productDesc=[productObject valueForKey:@"product_desc"];
//            product.imageList=[productObject valueForKey:@"images"];
//            product.productTime=[productObject valueForKey:@"date_added"];
//            product.productType=[productObject valueForKey:@"product_type"];
//            product.productAmount=[productObject valueForKey:@"amount"];
//            product.categoriesName=[productObject valueForKey:@"category"];
//            product.categoriesIds=[productObject valueForKey:@"catid"];
//            
//            
//            product.username=[[productObject valueForKey:@"owner"] valueForKey:@"name"];
//            product.useraddress=[[productObject valueForKey:@"owner"] valueForKey:@"address"];
//            product.email=[[productObject valueForKey:@"owner"] valueForKey:@"email"];
//            
//            
//            [productArray replaceObjectAtIndex:i withObject:product];
//            
//            product=nil;
//            
//            
//        }
//        
//        
//        product1=nil;
//        
//        
//    }
//    
//    [_table reloadData];
}


-(void)makeRoundImage:(UIImageView *)imageView
{
    imageView.image =    [UIImage getRoundedRectImageFromImage:imageView.image onReferenceView:imageView withCornerRadius:imageView.frame.size.width/2];
    imageView.layer.borderColor = [UIColor whiteColor].CGColor;
    imageView.layer.cornerRadius = imageView.frame.size.width/2;
    imageView.layer.borderWidth = 0;//2
    imageView.clipsToBounds = YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"productdetailsnav"]){
        
        TopNavigation *vc = (TopNavigation*)[segue destinationViewController];
        vc.titleString=@"PRODUCT VIEW";
        vc.fromWhere=4;
        vc.delegate = self;
        
        
        
        
        
        
        
        
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
