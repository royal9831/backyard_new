//
//  ProductDetails.h
//  Exchange
//
//  Created by Debayan Ghosh on 17/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductClass.h"
@protocol ProductDetailsDetegale <NSObject>

-(void)objectWhenPop2:(ProductClass*)productObject;

@end
@interface ProductDetails : UIViewController
@property (nonatomic,retain)ProductClass *product;
@property (nonatomic,retain) IBOutlet UIView *backgroundView;
@property (nonatomic,retain)IBOutlet UIImageView *imageview1, *imageview2, *imageview3, *imageview4, *imageview5;
@property (nonatomic,retain) IBOutlet UILabel *nameLabel, *addressLabel, *descriptionLabel,*productNameLabel,*productQuantityLabel,*productTimeLabel;
@property (nonatomic, weak) id<ProductDetailsDetegale> delegate;
@end
