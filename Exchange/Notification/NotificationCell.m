//
//  NotificationCell.m
//  Exchange
//
//  Created by Debayan Ghosh on 11/09/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "NotificationCell.h"

@implementation NotificationCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
