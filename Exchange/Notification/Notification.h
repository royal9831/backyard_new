//
//  Notification.h
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "AppDelegate.h"
@interface Notification : UIViewController<UIGestureRecognizerDelegate,SWRevealViewControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{

    AppDelegate *appDel;
    UITapGestureRecognizer *tap;
    NSMutableArray *notificationArray;
}
@property (nonatomic,retain) IBOutlet UITableView *table;
@property (nonatomic,retain) IBOutlet UIView *backgroundView;
@end
