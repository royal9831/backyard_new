//
//  NotificationCell.h
//  Exchange
//
//  Created by Debayan Ghosh on 11/09/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell
@property (nonatomic,retain) IBOutlet UIView *cellView;
@property (nonatomic,retain) IBOutlet UILabel *nameLabel, *descriptionLabel, *dateLabel,*productName;
@property (nonatomic,retain) IBOutlet UIButton *closeBtn;
@end
