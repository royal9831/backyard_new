//
//  Notification.m
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "Notification.h"
#import "TopNavigation.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "SCLAlertView.h"
#import "NotificationCell.h"
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEI
@interface Notification ()<UIViewControllerTransitioningDelegate,ProfileViewPageDetegale>

@end

@implementation Notification

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if([ [ UIScreen mainScreen ] bounds ].size.height==480)
    {
        _backgroundView.frame=CGRectMake(0, 0, 320, 568);
        _table.frame=CGRectMake(0, 64, 320, 480-64);
        
    }

    int temp=[[NSDate date] timeIntervalSince1970];
    NSString *timestamp=[NSString stringWithFormat:@"%d",temp];
    NSLog(@"timestamp %@",timestamp);
    
    
    
    ////////////////////////// Try this /////////////////////////////
    
//    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
//    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    
//    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:162/255.0 green:38/255.0 blue:32/255.0 alpha:1.0]];
//    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
//    
//    [SVProgressHUD setRingThickness:6.0];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//    [SVProgressHUD showWithStatus:@"Loading.."];
//    self.view.userInteractionEnabled=NO;
    
    
//    [SVProgressHUD dismiss];
//    self.view.userInteractionEnabled=YES;
//    
//    
//    SCLAlertView *alert = [[SCLAlertView alloc] init];
////    [alert addButton:@"First Button" target:self selector:@selector(firstButton)];
//    alert.shouldDismissOnTapOutside = NO;
//    alert.showAnimationType=SlideInToCenter;
//    alert.hideAnimationType=SlideOutToCenter;
//    [alert alertIsDismissed:^{
//        NSLog(@"SCLAlertView dismissed!");
//        
//    }];
//    
//    [alert showInfo:self title:@"INFO" subTitle:@"No jobs available" closeButtonTitle:@"OK" duration:0.0f];
    
    
    
    
    
    ////////////////////////// Try this /////////////////////////////
    
    
    
    
    
    
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    SWRevealViewController *revealController = [self revealViewController];
    self.revealViewController.delegate = self;
    tap = [revealController tapGestureRecognizer];
    tap.delegate = self;
    tap.enabled=NO;
    [self.view addGestureRecognizer:tap];
    
    
    
    [self notificationListWebservice];
    
    
}



-(NSString *)dateConversion:(NSString *)dateString
{
    double unixTimeStamp =[dateString doubleValue];
    NSTimeInterval _interval=unixTimeStamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"EEE, MMM dd, hh:mm a"];
    NSString *convertedDateString = [formatter stringFromDate:date];
    
    
    
    return convertedDateString ;
    
    
//    HH mm a 'I' dd MM yyyy
    
    //////////////////////////////////////////
}


-(void)firstButton
{
    
}

#pragma mark - SWRevealViewController Delegate Methods



- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        tap.enabled=NO;
        //        table.userInteractionEnabled=TRUE;
    } else {
        tap.enabled=YES;
        //        table.userInteractionEnabled=FALSE;
        
    }
    
    
    
}





-(void)notificationListWebservice
{
    
    
    
    notificationArray=[[NSMutableArray alloc]init];
    
    
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/NotificationHistory",appDel.hostUrl];
    
    NSLog(@"ID %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"]);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],@"user_id",
                           nil];
    
    //    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    //    [manager.requestSerializer setValue:@"calvinAndHobbessRock" forHTTPHeaderField:@"X-I do what I want"];
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         
         NSLog(@"JSON: %@", responseObject);
         
         if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
         {
             
             for(int i=0;i<[[responseObject valueForKey:@"notification"] count];i++)
             {
                 [notificationArray addObject:[[responseObject valueForKey:@"notification"] objectAtIndex:i]];
             }
             
             [_table reloadData];
         }
         else
         {
             [appDel.alertview showInfo:self title:@"Notification" subTitle:@"No notification available" closeButtonTitle:@"OK" duration:0.0f];
         }
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         NSLog(@"Error: %@", error);
         
         [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
         
         
     }];
    
    
    
    
    
    
    
    
    
}







#pragma mark tableview delegates


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [notificationArray count];
}
-(UITableViewCell *)tableView: (UITableView *)tableView cellForRowAtIndexPath: (NSIndexPath *)indexPath{
    
    
    
    static  NSString *CellIdentifier = @"notificationcell";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[NotificationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    if([[NSString stringWithFormat:@"%@",[[notificationArray objectAtIndex:indexPath.row] valueForKey:@"owner_tag"]] isEqualToString:@"0"])
    {
        cell.nameLabel.text=[[[notificationArray objectAtIndex:indexPath.row] valueForKey:@"demand_user"] valueForKey:@"name"];
        cell.productName.text=[NSString stringWithFormat:@"Product Name: %@",[[notificationArray objectAtIndex:indexPath.row] valueForKey:@"product_name"]];
        cell.descriptionLabel.text=@"You have accepted the request";
        cell.dateLabel.text=[[self dateConversion:[[notificationArray objectAtIndex:indexPath.row] valueForKey:@"request_date"]]uppercaseString];
    }
    else
    {
        NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
        cell.nameLabel.text=[[userdefaults valueForKey:@"userdetails"] valueForKey:@"name"];
        cell.productName.text=[NSString stringWithFormat:@"Product Name: %@",[[notificationArray objectAtIndex:indexPath.row] valueForKey:@"product_name"]];
        cell.descriptionLabel.text=@"Your product request has been accepted";
        cell.dateLabel.text=[[self dateConversion:[[notificationArray objectAtIndex:indexPath.row] valueForKey:@"request_date"]]uppercaseString];
    }
    
    
    if(indexPath.row%2==0)
    {
        cell.cellView.backgroundColor=[UIColor colorWithRed:206.0/255 green:206.0/255 blue:206.0/255 alpha:1.0];
    }
    else
    {
        cell.cellView.backgroundColor=[UIColor colorWithRed:223.0/255 green:223.0/255 blue:223.0/255 alpha:1.0];
    }
    
    
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        cell.cellView.frame=CGRectMake(0, 0, 375, 95);
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        cell.cellView.frame=CGRectMake(0, 0, 414, 95);
    }
    else
    {
        cell.cellView.frame=CGRectMake(0, 0, 320, 95);
    }
    
        
    
    
    
    [cell.closeBtn addTarget:self action:@selector(closeBtnAction:)forControlEvents:UIControlEventTouchUpInside];
    cell.closeBtn.tag=indexPath.row;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}





-(IBAction)closeBtnAction:(id)sender
{
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/NotificationDelete",appDel.hostUrl];
    
    NSLog(@"ID %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"]);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:[[notificationArray objectAtIndex:[sender tag]] valueForKey:@"req_id"],@"req_id",
                           nil];
    
    
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         
         NSLog(@"JSON: %@", responseObject);
         
         if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
         {
             
             [appDel.alertview showInfo:self title:@"Notification" subTitle:@"Notification deleted successfully" closeButtonTitle:@"OK" duration:0.0f];
             
             [notificationArray removeObjectAtIndex:[sender tag]];
             
             [_table reloadData];
             
             
         }
         else
         {
             [appDel.alertview showInfo:self title:@"Notification" subTitle:@"Notification deleted failed" closeButtonTitle:@"OK" duration:0.0f];
         }
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         NSLog(@"Error: %@", error);
         
         [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
         
         
     }];
    

}







- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"notificationnav"]){
        
        TopNavigation *vc = (TopNavigation*)[segue destinationViewController];
        vc.titleString=@"NOTIFICATION";
        vc.fromWhere=3;
        vc.delegate = self;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
