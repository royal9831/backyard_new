//
//  Map.m
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//<GMSMapViewDelegate>

#import "Map.h"
#import "TopNavigation.h"
#import<GoogleMaps/GoogleMaps.h>
//#import <GoogleMapsM4B/GoogleMaps.h>
#import "MapClass.h"
#import "SCLAlertView.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "UIImage+RoundedImage.h"
#import "CategoryObject.h"
#import "MapSearchClass.h"
#import "MapSearch.h"

#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))


#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
NSString *catIdString;
UIPickerView *pickerView;
@interface Map ()<UIViewControllerTransitioningDelegate,ProfileViewPageDetegale,GMSMapViewDelegate>
{
    GMSMapView *mapView_;
    UIView *viewForSlider;
    UILabel *labelToShowCurrentRadiusValue;
}

@end

@implementation Map
@synthesize locationManager,fromTag;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    
    
    
    
//    GMSMutablePath *poly = [GMSMutablePath path];
//    [poly addCoordinate:CLLocationCoordinate2DMake(52.506191, 1.83197)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(52.05249, 1.650696)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(51.92225, 1.321106)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(51.996719, 1.219482)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(52.049112, 1.244202)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(52.197507, 1.334839)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(52.519564, 1.801758)];
//    
//    GMSPolygon *polygon = [GMSPolygon polygonWithPath:poly];
//    polygon.fillColor = [UIColor blackColor];
//    polygon.strokeColor = [UIColor greenColor];
//    polygon.strokeWidth = 5;
//    polygon.map = mapView_;
    
    
    
    
    
    
    
    
    _mapSearchText.returnKeyType = UIReturnKeyDone;
    
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    if(fromTag!=1)
    {
    
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    SWRevealViewController *revealController = [self revealViewController];
    self.revealViewController.delegate = self;
    tap = [revealController tapGestureRecognizer];
    tap.delegate = self;
    tap.enabled=NO;
    [self.view addGestureRecognizer:tap];
    }
    
    
    
    
    
    
    
    if([ [ UIScreen mainScreen ] bounds ].size.height==480)
    {
        _backgroundView.frame=CGRectMake(0, 0, 320, 568);
        _scroll.frame=CGRectMake(0, 64, 320, 568-64);
        _scroll.contentSize=CGSizeMake(320, 590);
        
    }
    else
    {
        _scroll.contentSize=CGSizeMake(self.view.frame.size.width, 900);
    }
    
    
    
    
    
//    [_categoryTextField setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    catIdString=@"";
    entries=[[NSMutableArray alloc]init];
    catIds=[[NSMutableArray alloc]init];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self categoryWebservice];
        
    });
    
    
    
    
    
//    mapView_ = [[GMSMapView alloc]initWithFrame:CGRectMake(0.0, 64.0, self.view.frame.size.width, _mapSearchText.frame.origin.y-100)];
    
    
    mapView_ = [[GMSMapView alloc]initWithFrame:CGRectMake(0.0, 0, self.view.frame.size.width, _searchView.frame.origin.y)];
    
    mapView_.delegate=self;
    mapView_.settings.myLocationButton = NO;
    mapView_.myLocationEnabled = YES;
    // new code
//    CALayer *theLayer = [CALayer layer];
//    theLayer.contentsRect = mapView_.bounds;
//    theLayer.frame = mapView_.bounds;
//    
//    theLayer.backgroundColor=[UIColor colorWithRed:20/255.0f green:20/255.0f blue:20/255.0f alpha:0.2f].CGColor;
//    [[mapView_ layer] addSublayer:theLayer];
    
    
    
    
//    CALayer *theLayer1 = [CALayer layer];
//    theLayer1.contentsRect = mapView_.bounds;
//    theLayer1.frame = CGRectMake(0.0, 0, 50, 50);
//    
//    theLayer1.backgroundColor=[UIColor colorWithRed:200/255.0f green:20/255.0f blue:20/255.0f alpha:1.0].CGColor;
//    [theLayer addSublayer:theLayer1];


    
//    CALayer *layer = [CALayer layer];
//    CAShapeLayer *mask = [CAShapeLayer layer];
//    layer.frame = mapView_.bounds;
//    UIImage *image = [UIImage imageNamed:@"editBtn"];
//    layer.contents = (id)image.CGImage;
//    CGPathRef path = CGPathCreateWithEllipseInRect(self.view.frame, NULL);
//    mask.path = path;
//    CGPathRelease(path);
//    layer.mask = mask;
//    [self.layer addSublayer:layer];
    
    [_scroll addSubview:mapView_];
    
    
    
    
    
    
    
    
    mapArray=[[NSMutableArray alloc]init];
    
    MapClass *map=[[MapClass alloc]init];
    map.lat=@"42.930674";
    map.lon=@"-75.380330";
    map.locationName=@"Place1";
    [mapArray addObject:map];
    
    MapClass *map1=[[MapClass alloc]init];
    map1.lat=@"40.701228";
    map1.lon=@"-111.949224";
    map1.locationName=@"Place2";
    [mapArray addObject:map1];
    
    
    MapClass *map2=[[MapClass alloc]init];
    map2.lat=@"22.588555";
    map2.lon=@"88.402004";
    map2.locationName=@"Place3";
    [mapArray addObject:map2];
    
    MapClass *map3=[[MapClass alloc]init];
    map3.lat=@"-33.867487";
    map3.lon=@"151.206990";
    map3.locationName=@"Place4";
    [mapArray addObject:map3];

    
//    pickerView= [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
//    pickerView.showsSelectionIndicator = YES;
//    pickerView.dataSource = self;
//    pickerView.delegate = self;
//    
//    _mapSearchText.inputView = pickerView;
//    
//    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
//    toolBar.barStyle = UIBarStyleBlackOpaque;
//    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTouched:)];
//    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTouched:)];
//    
//    [toolBar setItems:[NSArray arrayWithObjects:cancelButton, [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneButton, nil]];
//    _mapSearchText.inputAccessoryView = toolBar;
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    lat=[[[defaults valueForKey:@"userdetails"] valueForKey:@"lat"] floatValue];
    lon=[[[defaults valueForKey:@"userdetails"] valueForKey:@"long"] floatValue];
    location=@"Current Location";
    
    
    
    
    
    
    
    _choiceBtn1.selected=YES;
    _choiceBtn2.selected=YES;
    _choiceBtn3.selected=YES;
    
    _choiceImg1.image=[UIImage imageNamed:@"check-button-2.png"];
    _choiceImg2.image=[UIImage imageNamed:@"check-button-2.png"];
    _choiceImg3.image=[UIImage imageNamed:@"check-button-2.png"];
    
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard:)];
    [_scroll addGestureRecognizer:gestureRecognizer];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self mapShow];
    });
}



-(void) hideKeyBoard:(id) sender
{
    // Do whatever such as hiding the keyboard
    [_mapSearchText resignFirstResponder];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    

    
    
    [_mapSearchText resignFirstResponder];
    
    
}



-(IBAction)choiceBtnAction1:(id)sender
{
    
    
    if(_choiceBtn1.selected)
    {
        _choiceBtn1.selected=NO;
        _choiceImg1.image=[UIImage imageNamed:@"check-button-1.png"];
    }
    else
    {
        _choiceBtn1.selected=YES;
        _choiceImg1.image=[UIImage imageNamed:@"check-button-2.png"];
    }
    
    
}

-(IBAction)choiceBtnAction2:(id)sender
{
    if(_choiceBtn2.selected)
    {
        _choiceBtn2.selected=NO;
        _choiceImg2.image=[UIImage imageNamed:@"check-button-1.png"];
    }
    else
    {
        _choiceBtn2.selected=YES;
        _choiceImg2.image=[UIImage imageNamed:@"check-button-2.png"];
    }
}

-(IBAction)choiceBtnAction3:(id)sender
{
    if(_choiceBtn3.selected)
    {
        _choiceBtn3.selected=NO;
        _choiceImg3.image=[UIImage imageNamed:@"check-button-1.png"];
    }
    else
    {
        _choiceBtn3.selected=YES;
        _choiceImg3.image=[UIImage imageNamed:@"check-button-2.png"];
    }
}






-(void)categoryWebservice
{
    
    
    
    //    ProductDetails[product_name], ProductDetails[user_id], ProductDetails[product_type], ProductDetails[product_desc], cat_id,ProductDetails[quantity]
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/Category",appDel.hostUrl];
    
//    NSLog(@"ID %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"]);
    
    //    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],@"ProductDetails[user_id]",_nameTextField.text, @"ProductDetails[product_name]",_productDescriptionText.text,@"ProductDetails[product_desc]",@"1",@"ProductDetails[product_type]",catIdString,@"cat_id" ,nil];
    //
    //    [manager POST:url parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:                           _emailTextField.text,@"UserLogin[username]",_passwordTextField.text,@"UserLogin[password]",
    //                           nil];
    
    [manager POST:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     
     {
         
         
         //        NSData * imgData;
         //
         //        imgData = UIImagePNGRepresentation(_circleImageView.image);
         //
         //        if(imgData!=NULL){
         //            [formData appendPartWithFileData:imgData name:@"profileImage" fileName:@"avatar.png" mimeType:@"image/png"];
         //        }
         //
         //
         //    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
         
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         
         NSLog(@"JSON: %@", responseObject);
         
         if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
         {
             
             
             entries=[[NSMutableArray alloc]init];
             catIds=[[NSMutableArray alloc]init];
             
             
             
             
             for(int i=0;i<[[responseObject valueForKey:@"category"] count];i++)
             {
                 
                 
                 [catIds addObject:[[[responseObject valueForKey:@"category"] objectAtIndex:i] valueForKey:@"cat_id"]];
                 [entries addObject:[[[responseObject valueForKey:@"category"] objectAtIndex:i] valueForKey:@"cat_name"]];
                 
                 
             }
             
             
             selectionStates = [[NSMutableDictionary alloc] init];
             for (NSString *key in entries)
             {
                 [selectionStates setObject:[NSNumber numberWithBool:NO] forKey:key];
             }
             
             //            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             //            [defaults setObject:[responseObject valueForKey:@"profile"] forKey:@"userdetails"];
             //            [defaults synchronize];
             //
             //            [[NSNotificationCenter defaultCenter] postNotificationName:@"profilepicchange" object:nil userInfo:nil];
             //
             //            [appDel.alertview showInfo:self title:@"Profile Update" subTitle:@"Profile updated successfully" closeButtonTitle:@"OK" duration:0.0f];
         }
         else
         {
             //            [appDel.alertview showInfo:self title:@"Profile Update" subTitle:[responseObject valueForKey:@"message"] closeButtonTitle:@"OK" duration:0.0f];
         }
         
         [self setPickerView];
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         NSLog(@"Error: %@", error);
         
         [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
         
         
     }];
    
}

-(void)setPickerView
{
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        pickerView = [[ALPickerView alloc] initWithFrame:CGRectMake(0, 68, 375.0, 0)];
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        pickerView = [[ALPickerView alloc] initWithFrame:CGRectMake(0, 68, 414, 0)];
    }
    else
    {
        pickerView = [[ALPickerView alloc] initWithFrame:CGRectMake(0, 68, 320, 0)];
    }
//    pickerView = [[ALPickerView alloc] initWithFrame:CGRectMake(0, 68, 0, 0)];
    pickerView.delegate = self;
    _categoryTextField.inputView = pickerView;
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleBlackOpaque;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTouched1:)];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTouched:)];
    [toolBar setItems:[NSArray arrayWithObjects:cancelButton, [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneButton, nil]];
    _categoryTextField.inputAccessoryView = toolBar;
    
    
    
    
    [pickerView reloadAllComponents];
    
    
}


#pragma mark pickerDelegates

- (NSInteger)numberOfRowsForPickerView:(ALPickerView *)pickerView {
    return [entries count];
}

- (NSString *)pickerView:(ALPickerView *)pickerView textForRow:(NSInteger)row {
    return [entries objectAtIndex:row] ;
}

- (BOOL)pickerView:(ALPickerView *)pickerView selectionStateForRow:(NSInteger)row {
    return [[selectionStates objectForKey:[entries objectAtIndex:row]] boolValue];
}

- (void)pickerView:(ALPickerView *)pickerView didCheckRow:(NSInteger)row {
    // Check whether all rows are checked or only one
    
    NSLog(@"row: %d",row);
    
    if (row == -1)
        for (id key in [selectionStates allKeys])
            [selectionStates setObject:[NSNumber numberWithBool:YES] forKey:key];
    else
        [selectionStates setObject:[NSNumber numberWithBool:YES] forKey:[entries objectAtIndex:row]];
    
    
    NSLog(@"Done....%@",[selectionStates valueForKey:[entries objectAtIndex:0]]);
    
    
}

- (void)pickerView:(ALPickerView *)pickerView didUncheckRow:(NSInteger)row {
    // Check whether all rows are unchecked or only one
    if (row == -1)
        for (id key in [selectionStates allKeys])
            [selectionStates setObject:[NSNumber numberWithBool:NO] forKey:key];
    else
        [selectionStates setObject:[NSNumber numberWithBool:NO] forKey:[entries objectAtIndex:row]];
}



- (void)doneTouched1:(UIBarButtonItem *)sender
{
    
    finalList=[[NSMutableArray alloc]init];
    finalIdList=[[NSMutableArray alloc]init];
    for(int i=0;i<[entries count];i++)
    {
        NSLog(@"Done....%@",[selectionStates valueForKey:[entries objectAtIndex:i]]);
        if([[NSString stringWithFormat:@"%@",[selectionStates valueForKey:[entries objectAtIndex:i]]] isEqualToString:@"1"])
        {
            [finalList addObject:[entries objectAtIndex:i]];
            [finalIdList addObject:[catIds objectAtIndex:i]];
        }
    }
    
    
    
    NSString *joinedString=@"";
    if([finalList count]>0)
    {
        
        joinedString = [finalList componentsJoinedByString:@", "];
        catIdString=[finalIdList componentsJoinedByString:@","];
        
        
        
    }
    NSLog(@"......%@....%@",joinedString,catIdString);
    
    
    
    NSString *completeString=@"";
    for(int i=0;i<[entries count];i++)
    {
        NSLog(@"PRODUCT: %@",[entries objectAtIndex:i]);
        completeString = [entries componentsJoinedByString:@", "];
        NSLog(@"PRODUCT FINAL: %@",completeString);
    }
    
    
    
    _categoryTextField.text=joinedString;
    if([completeString isEqualToString:joinedString])
    {
        _categoryTextField.text=@"All";
    }
    
    
    
    
    
    [_categoryTextField resignFirstResponder];
    
    
    
    
}




- (void)cancelTouched:(UIBarButtonItem *)sender
{
    [_categoryTextField resignFirstResponder];
}














/*- (void)cancelTouched:(UIBarButtonItem *)sender
{
    // hide the picker view
    [_mapSearchText resignFirstResponder];
   
}*/

- (void)doneTouched:(UIBarButtonItem *)sender
{
    // hide the picker view
    [_mapSearchText resignFirstResponder];
       // perform some action
}





#pragma mark Textfield delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    CGRect viewFrame = self.view.frame;
    CGRect viewFrame1 = _container.frame;
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        viewFrame.origin.y = -80;
        viewFrame1.origin.y = 80;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        viewFrame.origin.y = -90;
        viewFrame1.origin.y = 90;
    }
    else
    {
        viewFrame.origin.y = -150;//150
        viewFrame1.origin.y = 150;//150
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.5];
    [_container setFrame:viewFrame1];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGRect viewFrame = self.view.frame;
    CGRect viewFrame1 = _container.frame;
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        viewFrame.origin.y = +0;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        viewFrame.origin.y = +0;
    }
    else
    {
        viewFrame.origin.y = +0;
    }
    viewFrame1.origin.y = 0;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.5];
    [_container setFrame:viewFrame1];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}






//#pragma mark - UIPickerViewDataSource
//- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
//{
//    return 1;
//}
//
//- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
//{
//   
//        return [mapArray count];
//    
//    
//}
//
//#pragma mark - UIPickerViewDelegate
//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    NSString *item;
//    MapClass *map=[[MapClass alloc]init];
//    map=[mapArray objectAtIndex:row];
//    item = map.locationName;
//    return item;
//}
//
//- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
//{
//    // perform some action
//    
//    MapClass *map=[[MapClass alloc]init];
//    map=[mapArray objectAtIndex:row];
//    lat=[map.lat floatValue];
//    lon=[map.lon floatValue];
//    location=map.locationName;
//    
//    _mapSearchText.text=map.locationName;
//    
//    [mapView_ clear];
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self mapShow];
//    });
//    
//}






-(void)mapShow
{
    

    self.locationManager = [[CLLocationManager alloc] init];
    
    self.locationManager.delegate = self;
    if(IS_OS_8_OR_LATER){
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                [self.locationManager requestAlwaysAuthorization];
            } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                [self.locationManager  requestWhenInUseAuthorization];
            } else {
                NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
            }
        }
    }
    [self.locationManager startUpdatingLocation];
    
    
    
    
    
    
    
    
    
    
    
//    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
//                                                            longitude:lon
//                                                                 zoom:15];
//    mapView_.camera=camera;
//    //plotting the google map
//    
//    
//    
//    
//    
//    
//    
//    GMSMarker *marker = [[GMSMarker alloc] init];
//    marker.position=CLLocationCoordinate2DMake(lat, lon);
//    marker.title = location;
//    marker.snippet = location;
//    marker.icon=[UIImage imageNamed:@"point-12.png"];
//    marker.map = mapView_;
    
    
}



#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    
    
    
    
    
    lon = [@"88.125642" floatValue];
    lat = [@"22.365487" floatValue];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                            longitude:lon
                                                                 zoom:15];
    mapView_.camera=camera;
    //plotting the google map
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position=CLLocationCoordinate2DMake(lat, lon);
    marker.title = location;
    marker.snippet = @"";//[NSString stringWithFormat:@"%@\n%@",location,@""];
    marker.icon=[UIImage imageNamed:@"point-12.png"];
    marker.map = mapView_;
    

    
    
    
    
    
//    GMSMutablePath *poly = [GMSMutablePath path];
//    [poly addCoordinate:CLLocationCoordinate2DMake(52.506191, 1.83197)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(52.05249, 1.650696)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(51.92225, 1.321106)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(51.996719, 1.219482)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(52.049112, 1.244202)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(52.197507, 1.334839)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(52.519564, 1.801758)];
//    
//    GMSPolygon *polygon = [GMSPolygon polygonWithPath:poly];
//    polygon.fillColor = [UIColor blackColor];
//    polygon.strokeColor = [UIColor greenColor];
//    polygon.strokeWidth = 5;
//    polygon.map = mapView_;
//    
//
//    CLLocationCoordinate2D circleCenter = CLLocationCoordinate2DMake(lat, lon);
//    GMSCircle *circ = [GMSCircle circleWithPosition:circleCenter
//                                             radius:6371000];
//    circ.fillColor=[UIColor colorWithRed:10.0/255 green:20.0/255 blue:30.0/255 alpha:0.1];
//    circ.strokeColor = [UIColor blueColor];
//    circ.strokeWidth = 3;
//    circ.map = mapView_;
    
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    if([defaults valueForKey:@"status"]!=nil && [[defaults valueForKey:@"status"] isEqualToString:@"1"])
    {
    
    CLLocationCoordinate2D circleCenter = CLLocationCoordinate2DMake(lat, lon);
    GMSCircle *circ = [GMSCircle circleWithPosition:circleCenter
                                             radius:[[[defaults valueForKey:@"usersettings"] valueForKey:@"radius"] floatValue]];
    circ.fillColor = [UIColor colorWithRed:96.0/255 green:172.0/255 blue:251.0/255 alpha:0.5];
    circ.strokeColor = [UIColor blueColor];
    circ.strokeWidth = 3;
    circ.map = mapView_;
        
        
        
//    NSLog(@"PATH %d",mapView_.projection.);
        
        
//        GMSPolygon *polygon = [[GMSPolygon alloc] init];
//        polygon.path = [self pathOfNewYorkState];
//        polygon.title = @"New York123";
//        polygon.fillColor = [UIColor colorWithRed:0.25 green:0 blue:0 alpha:0.2f];
//        polygon.strokeColor = [UIColor blackColor];
//        polygon.strokeWidth = 2;
//        polygon.tappable = YES;
//        polygon.map = mapView_;
        
        
        
        
        
    }
    
    
    
    
    
//    lon = [@"88.156642" floatValue];
//    lat = [@"22.3650487" floatValue];
//    
//    
//    GMSMarker *marker1 = [[GMSMarker alloc] init];
//    marker1.position=CLLocationCoordinate2DMake(lat, lon);
//    marker1.title = location;
//    marker1.snippet = [NSString stringWithFormat:@"%@\n%@",location,@"fdfdfd"];
//    marker1.icon=[UIImage imageNamed:@"point-12.png"];
//    marker1.map = mapView_;
    
    
    
    
    
    
//    GMSPolygon *polygon = [[GMSPolygon alloc] init];
//    polygon.path = [self pathOfNewYorkState];
//    polygon.title = @"New York";
//    polygon.fillColor = [UIColor colorWithRed:0.25 green:0 blue:0 alpha:0.2f];
//    polygon.strokeColor = [UIColor blackColor];
//    polygon.strokeWidth = 2;
//    polygon.tappable = YES;
//    polygon.map = mapView_;
    
    
    
    
}



- (GMSPath *)pathOfNewYorkState {
    GMSMutablePath *path = [GMSMutablePath path];
    [path addLatitude:42.5142 longitude:-79.7624];
    [path addLatitude:42.7783 longitude:-79.0672];
    [path addLatitude:42.8508 longitude:-78.9313];
    [path addLatitude:42.9061 longitude:-78.9024];
    [path addLatitude:42.9554 longitude:-78.9313];
    [path addLatitude:42.9584 longitude:-78.9656];
    [path addLatitude:42.9886 longitude:-79.0219];
    [path addLatitude:43.0568 longitude:-79.0027];
    [path addLatitude:43.0769 longitude:-79.0727];
    [path addLatitude:43.1220 longitude:-79.0713];
    [path addLatitude:43.1441 longitude:-79.0302];
    [path addLatitude:43.1801 longitude:-79.0576];
    [path addLatitude:43.2482 longitude:-79.0604];
    [path addLatitude:43.2812 longitude:-79.0837];
    [path addLatitude:43.4509 longitude:-79.2004];
    [path addLatitude:43.6311 longitude:-78.6909];
    [path addLatitude:43.6321 longitude:-76.7958];
    [path addLatitude:43.9987 longitude:-76.4978];
    [path addLatitude:44.0965 longitude:-76.4388];
    [path addLatitude:44.1349 longitude:-76.3536];
    [path addLatitude:44.1989 longitude:-76.3124];
    [path addLatitude:44.2049 longitude:-76.2437];
    [path addLatitude:44.2413 longitude:-76.1655];
    [path addLatitude:44.2973 longitude:-76.1353];
    [path addLatitude:44.3327 longitude:-76.0474];
    [path addLatitude:44.3553 longitude:-75.9856];
    [path addLatitude:44.3749 longitude:-75.9196];
    [path addLatitude:44.3994 longitude:-75.8730];
    [path addLatitude:44.4308 longitude:-75.8221];
    [path addLatitude:44.4740 longitude:-75.8098];
    [path addLatitude:44.5425 longitude:-75.7288];
    [path addLatitude:44.6647 longitude:-75.5585];
    [path addLatitude:44.7672 longitude:-75.4088];
    [path addLatitude:44.8101 longitude:-75.3442];
    [path addLatitude:44.8383 longitude:-75.3058];
    [path addLatitude:44.8676 longitude:-75.2399];
    [path addLatitude:44.9211 longitude:-75.1204];
    [path addLatitude:44.9609 longitude:-74.9995];
    [path addLatitude:44.9803 longitude:-74.9899];
    [path addLatitude:44.9852 longitude:-74.9103];
    [path addLatitude:45.0017 longitude:-74.8856];
    [path addLatitude:45.0153 longitude:-74.8306];
    [path addLatitude:45.0046 longitude:-74.7633];
    [path addLatitude:45.0027 longitude:-74.7070];
    [path addLatitude:45.0007 longitude:-74.5642];
    [path addLatitude:44.9920 longitude:-74.1467];
    [path addLatitude:45.0037 longitude:-73.7306];
    [path addLatitude:45.0085 longitude:-73.4203];
    [path addLatitude:45.0109 longitude:-73.3430];
    [path addLatitude:44.9874 longitude:-73.3547];
    [path addLatitude:44.9648 longitude:-73.3379];
    [path addLatitude:44.9160 longitude:-73.3396];
    [path addLatitude:44.8354 longitude:-73.3739];
    [path addLatitude:44.8013 longitude:-73.3324];
    [path addLatitude:44.7419 longitude:-73.3667];
    [path addLatitude:44.6139 longitude:-73.3873];
    [path addLatitude:44.5787 longitude:-73.3736];
    [path addLatitude:44.4916 longitude:-73.3049];
    [path addLatitude:44.4289 longitude:-73.2953];
    [path addLatitude:44.3513 longitude:-73.3365];
    [path addLatitude:44.2757 longitude:-73.3118];
    [path addLatitude:44.1980 longitude:-73.3818];
    [path addLatitude:44.1142 longitude:-73.4079];
    [path addLatitude:44.0511 longitude:-73.4367];
    [path addLatitude:44.0165 longitude:-73.4065];
    [path addLatitude:43.9375 longitude:-73.4079];
    [path addLatitude:43.8771 longitude:-73.3749];
    [path addLatitude:43.8167 longitude:-73.3914];
    [path addLatitude:43.7790 longitude:-73.3557];
    [path addLatitude:43.6460 longitude:-73.4244];
    [path addLatitude:43.5893 longitude:-73.4340];
    [path addLatitude:43.5655 longitude:-73.3969];
    [path addLatitude:43.6112 longitude:-73.3818];
    [path addLatitude:43.6271 longitude:-73.3049];
    [path addLatitude:43.5764 longitude:-73.3063];
    [path addLatitude:43.5675 longitude:-73.2582];
    [path addLatitude:43.5227 longitude:-73.2445];
    [path addLatitude:43.2582 longitude:-73.2582];
    [path addLatitude:42.9715 longitude:-73.2733];
    [path addLatitude:42.8004 longitude:-73.2898];
    [path addLatitude:42.7460 longitude:-73.2664];
    [path addLatitude:42.4630 longitude:-73.3708];
    [path addLatitude:42.0840 longitude:-73.5095];
    [path addLatitude:42.0218 longitude:-73.4903];
    [path addLatitude:41.8808 longitude:-73.4999];
    [path addLatitude:41.2953 longitude:-73.5535];
    [path addLatitude:41.2128 longitude:-73.4834];
    [path addLatitude:41.1011 longitude:-73.7275];
    [path addLatitude:41.0237 longitude:-73.6644];
    [path addLatitude:40.9851 longitude:-73.6578];
    [path addLatitude:40.9509 longitude:-73.6132];
    [path addLatitude:41.1869 longitude:-72.4823];
    [path addLatitude:41.2551 longitude:-72.0950];
    [path addLatitude:41.3005 longitude:-71.9714];
    [path addLatitude:41.3108 longitude:-71.9193];
    [path addLatitude:41.1838 longitude:-71.7915];
    [path addLatitude:41.1249 longitude:-71.7929];
    [path addLatitude:41.0462 longitude:-71.7517];
    [path addLatitude:40.6306 longitude:-72.9465];
    [path addLatitude:40.5368 longitude:-73.4628];
    [path addLatitude:40.4887 longitude:-73.8885];
    [path addLatitude:40.5232 longitude:-73.9490];
    [path addLatitude:40.4772 longitude:-74.2271];
    [path addLatitude:40.4861 longitude:-74.2532];
    [path addLatitude:40.6468 longitude:-74.1866];
    [path addLatitude:40.6556 longitude:-74.0547];
    [path addLatitude:40.7618 longitude:-74.0156];
    [path addLatitude:40.8699 longitude:-73.9421];
    [path addLatitude:40.9980 longitude:-73.8934];
    [path addLatitude:41.0343 longitude:-73.9854];
    [path addLatitude:41.3268 longitude:-74.6274];
    [path addLatitude:41.3583 longitude:-74.7084];
    [path addLatitude:41.3811 longitude:-74.7101];
    [path addLatitude:41.4386 longitude:-74.8265];
    [path addLatitude:41.5075 longitude:-74.9913];
    [path addLatitude:41.6000 longitude:-75.0668];
    [path addLatitude:41.6719 longitude:-75.0366];
    [path addLatitude:41.7672 longitude:-75.0545];
    [path addLatitude:41.8808 longitude:-75.1945];
    [path addLatitude:42.0013 longitude:-75.3552];
    [path addLatitude:42.0003 longitude:-75.4266];
    [path addLatitude:42.0013 longitude:-77.0306];
    [path addLatitude:41.9993 longitude:-79.7250];
    [path addLatitude:42.0003 longitude:-79.7621];
    [path addLatitude:42.1827 longitude:-79.7621];
    [path addLatitude:42.5146 longitude:-79.7621];
    return path;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        lon = [[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] floatValue];
        lat = [[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] floatValue];
        
        
        [locationManager stopUpdatingLocation];
        
        
        
        
        
        
        
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                                longitude:lon
                                                                     zoom:12];
        mapView_.camera=camera;
        //plotting the google map
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position=CLLocationCoordinate2DMake(lat, lon);
        marker.title = location;
        marker.snippet = @"";//[NSString stringWithFormat:@"%@%@",location,@""];
        marker.icon=[UIImage imageNamed:@"point-12.png"];
        marker.map = mapView_;
        
        
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        
        if([defaults valueForKey:@"status"]!=nil && [[defaults valueForKey:@"status"] isEqualToString:@"1"])
        {
            CLLocationCoordinate2D circleCenter = CLLocationCoordinate2DMake(lat, lon);
            GMSCircle *circ = [GMSCircle circleWithPosition:circleCenter
                                                     radius:[[[defaults valueForKey:@"usersettings"] valueForKey:@"radius"] floatValue]];
            
            circ.fillColor = [UIColor colorWithRed:96.0/255 green:172.0/255 blue:251.0/255 alpha:0.5];
            circ.strokeColor = [UIColor blueColor];
            circ.strokeWidth = 5;
            circ.map = mapView_;
        }
        
        
        
        
        
        
        
    }
}


- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
    NSNumber *number = [marker.userData objectForKey:@"marker_id"];
    
    UIView *newview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 150, 80)];
    newview.backgroundColor=[UIColor clearColor];
    
    UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 150, 80)];
    imgView.image=[UIImage imageNamed:@"MarkerView.png"];
    [newview addSubview:imgView];
    
    
    
    UILabel *usernameLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, 150, 20)];
    UILabel *productLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, 150, 30)];
    productLabel.numberOfLines=5;
    
    [self customizeLabel:usernameLabel:1];
    [self customizeLabel:productLabel:2];
    
    usernameLabel.text=marker.title;
    productLabel.text=marker.snippet;
    
    [newview addSubview:usernameLabel];
    [newview addSubview:productLabel];
    
    
//    [ma addSubview:newview];

    return newview;
}



- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
        
    return NO;
}


-(void)customizeLabel:(UILabel *)customLabel :(int)type
{
    if(type==1)
    {
        customLabel.font=[UIFont systemFontOfSize:13.0];
        [customLabel setTextColor:[UIColor blackColor]];
    }
    else
    {
        customLabel.font=[UIFont systemFontOfSize:10.0];
        [customLabel setTextColor:[UIColor darkGrayColor]];
    }
    
    
    [customLabel setTextAlignment:NSTextAlignmentCenter];
    customLabel.backgroundColor=[UIColor clearColor];
    
}



-(void)startUserTracking
{
    // Listen to the myLocation property of GMSMapView.
    if (mapView_.observationInfo == nil)
    {
        [mapView_ addObserver:self
                   forKeyPath:@"myLocation"
                      options:NSKeyValueObservingOptionNew
                      context:NULL];
        
        // Ask for My Location data after the map has already been added to the UI.
        dispatch_async(dispatch_get_main_queue(), ^{
            mapView_.myLocationEnabled = YES;
        });
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    
    
    CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
    
    mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                     zoom:18];
    
    labelToShowCurrentRadiusValue.text = [NSString stringWithFormat:@"%f , %f", location.coordinate.latitude, location.coordinate.longitude];
}

#pragma mark - SWRevealViewController Delegate Methods



- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        tap.enabled=NO;
        //        table.userInteractionEnabled=TRUE;
    } else {
        tap.enabled=YES;
        //        table.userInteractionEnabled=FALSE;
        
    }
    
    
    
}



-(IBAction)productSearch:(id)sender
{
    [_mapSearchText resignFirstResponder];
    
    
    productList=[[NSMutableArray alloc]init];
    
//    for (int i=0; i<7; i++)
//    {
//        MapSearchClass *mapSearch=[[MapSearchClass alloc]init];
//        mapSearch.product_id=@"35";
//        mapSearch.user_id=@"18";
//        mapSearch.product_name=@"product1";
//        mapSearch.product_type=@"1";
//        mapSearch.product_desc=@"Ghjjhjjkkk";
//        mapSearch.name=@"santu5012";
//        mapSearch.phone_no=@"1234000000122";
//        mapSearch.address=@"kolkata.india";
//        mapSearch.lat=[NSString stringWithFormat:@"23.%d07820",i];
//        mapSearch.lon=@"88.642870";
//        mapSearch.product_category=@"A,B,C";
//        mapSearch.profileimage=@"http://192.168.0.233/Backyard/php/images/profile/892eb170348177f8aa2661260abeb564-avatar.png";
//        
//        
//        [productList addObject:mapSearch];
//        
//        mapSearch=nil;
//
//    }
    
    
    
    
    
    
    
    
    
    
    
    if(!_choiceBtn1.selected && !_choiceBtn2.selected && !_choiceBtn3.selected)
    {
        [appDel.alertview showInfo:self title:@"Product" subTitle:@"Please select at least one availble as" closeButtonTitle:@"OK" duration:0.0f];
    }
    else
    {
        [self productSearchWebService];
    }

    
    
    
    
}


#pragma mark productSearchWebService

-(void)productSearchWebService
{
    NSString *type1,*type2,*type3;
    NSString *choiceString=@"";
    
    if(_choiceBtn1.selected)
    {
        if(![choiceString isEqualToString:@""])
        {
            choiceString=[choiceString stringByAppendingString:@",'0'"];
        }
        else
        {
            choiceString=[choiceString stringByAppendingString:@"'0'"];
        }
    }
    
    
    if(_choiceBtn2.selected)
    {
        if(![choiceString isEqualToString:@""])
        {
            choiceString=[choiceString stringByAppendingString:@",'1'"];
        }
        else
        {
            choiceString=[choiceString stringByAppendingString:@"'1'"];
        }
    }
    
    
    if(_choiceBtn3.selected)
    {
        if(![choiceString isEqualToString:@""])
        {
            choiceString=[choiceString stringByAppendingString:@",'2'"];
        }
        else
        {
            choiceString=[choiceString stringByAppendingString:@"'2'"];
        }
    }
    
    
    
    
    
    
    NSLog(@"choiceString %@",choiceString);
    
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *UserId;
    
    if([[defaults valueForKey:@"status"] isEqualToString:@"1"])
    {
        UserId=[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"];
    }
    else
    {
        UserId=@"";
    }
//    UserId=@"";
    
    NSString *url = [NSString stringWithFormat:@"%@?r=api/Search",appDel.hostUrl];
    
    NSLog(@"ID %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"]);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:_mapSearchText.text,@"searchText",catIdString,@"cat_id",choiceString,@"type",UserId,@"user_id",
                           nil];
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         
         NSLog(@"JSON: %@", responseObject);
         
         if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
         {
             
             
             
//             appDel.alertview showInfo:self title:@"Product" subTitle:@"No product found" closeButtonTitle:@"OK" duration:0.0f];
             
             
             productList=[[NSMutableArray alloc]init];
             for(int i=0;i<[[responseObject valueForKey:@"searchList"] count];i++)
             {
                 
                 
                 
                 
                 
                 MapSearchClass *mapSearch=[[MapSearchClass alloc]init];
                 mapSearch.product_id=[self checkNull:[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"product_id"]];
                 mapSearch.user_id=[self checkNull:[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"user_id"]];
                 mapSearch.product_name=[self checkNull:[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"product_name"]];
                 mapSearch.product_type=[self checkNull:[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"product_type"]];
                 mapSearch.product_desc=[self checkNull:[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"product_desc"]];
                 mapSearch.productQuantity=[self checkNull:[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"quantity"]];
                 mapSearch.product_category=[self checkNull:[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"category"]];
                 
                 
                 mapSearch.name=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"name"]];
                 mapSearch.phone_no=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"phone_no"]];
                 mapSearch.email=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"email"]];
                 mapSearch.address=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"address"]];
                 mapSearch.lat=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"lat"]];
                 mapSearch.lon=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"long"]];
                 mapSearch.profileimage=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"profile_image"]];
                 mapSearch.productOwnerId=[self checkNull:[[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"user_id"]];
                 
                 mapSearch.productImageList=[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"images"];
                 mapSearch.backyardImageList=[[[responseObject valueForKey:@"searchList"] objectAtIndex:i] valueForKey:@"backyard"];
                 
                 
                 
                 
                 [productList addObject:mapSearch];
                 
                 mapSearch=nil;

                 
                 
             }
             
             [self performSegueWithIdentifier:@"productsearchpush" sender:nil];
         }
         else
         {
             [appDel.alertview showInfo:self title:@"Product" subTitle:@"No product found" closeButtonTitle:@"OK" duration:0.0f];
         }
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         NSLog(@"Error: %@", error);
         
         [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
         
         
     }];
    
}


-(NSString *)checkNull :(NSString *)str
{
    NSString *InputString=str;
    if( (InputString == nil) ||(InputString ==(NSString *)[NSNull null])||([InputString isEqual:nil])||([InputString length] == 0)||[allTrim( InputString ) length] == 0||([InputString isEqualToString:@""])||([InputString isEqualToString:@"(NULL)"])||([InputString isEqualToString:@"<NULL>"])||([InputString isEqualToString:@"<null>"]||([InputString isEqualToString:@"(null)"])||([InputString isEqualToString:@""]))
       
       )
        return @"";
    else
        return str ;
    
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"mapnav"]){
        
        TopNavigation *vc = (TopNavigation*)[segue destinationViewController];
        vc.titleString=@"MAP SEARCH";
        if(fromTag!=1)
        {
            vc.fromWhere=3;
        }
        else
        {
            vc.fromWhere=4;
        }
        vc.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"productsearchpush"]){
        
        MapSearch *vc = (MapSearch*)[segue destinationViewController];
        vc.productList=productList;
        
    }
    
}



-(IBAction)mapSaveBtnAction:(id)sender
{
    [self mapSaveWebservice];
}

#pragma mark mapSaveWebservice

-(void)mapSaveWebservice
{
    
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/UpdateMap",appDel.hostUrl];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],@"user_id",[NSString stringWithFormat:@"%f",lat],@"lat",[NSString stringWithFormat:@"%f",lon],@"long",
                           nil];
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        
        NSLog(@"JSON: %@", responseObject);
        
        if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
        {
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            [defaults setObject:[responseObject valueForKey:@"notification"] forKey:@"userdetails"];
//            [defaults synchronize];
            
            [appDel.alertview showInfo:self title:@"Map" subTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]] closeButtonTitle:@"OK" duration:0.0f];
            
        }
        else
        {
            [appDel.alertview showInfo:self title:@"Map" subTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]] closeButtonTitle:@"OK" duration:0.0f];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        NSLog(@"Error: %@", error);
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
        
    }];
    
    
    
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
