//
//  Map.h
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import "ALPickerView.h"
@interface Map : UIViewController<UIGestureRecognizerDelegate,SWRevealViewControllerDelegate,UIPickerViewDataSource, UIPickerViewDelegate,CLLocationManagerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,ALPickerViewDelegate>
{
    AppDelegate *appDel;
    UITapGestureRecognizer *tap;
    NSMutableArray *mapArray;
    float lat,lon;
    NSString *location;
    
    ALPickerView *pickerView;
    NSMutableArray *entries,*catIds;
    NSMutableDictionary *selectionStates;
    NSMutableArray *finalList,*finalIdList;
    NSMutableArray *productList;
}
@property (weak, nonatomic) IBOutlet UIView *container;
@property (nonatomic,retain) IBOutlet UIView *backgroundView,*searchView;
@property (nonatomic,retain) IBOutlet UIScrollView *scroll;
@property (retain,nonatomic) CLLocationManager *locationManager;
@property (nonatomic,retain)IBOutlet UITextField *mapSearchText,*categoryTextField;
@property (nonatomic,retain) IBOutlet UIButton *choiceBtn1,*choiceBtn2,*choiceBtn3;
@property (nonatomic,retain) IBOutlet UIImageView *choiceImg1,*choiceImg2,*choiceImg3;
@property int fromTag;
@end
