//
//  Dashboard.m
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "Dashboard.h"
#import "TopNavigation.h"
#import<GoogleMaps/GoogleMaps.h>
//#import <GoogleMapsM4B/GoogleMaps.h>
#import "UIImageView+WebCache.h"
#import "UIImage+RoundedImage.h"
#import "UIView+Blur.h"
#import "RCBlurredImageView.h"
#import "JWBlurView.h"
#import "SVBlurView.h"
#import <mach/mach.h>
#import <mach/mach_time.h>
#import "UIImageEffects.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "SCLAlertView.h"
#import "CollectionCell.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "CFCoverFlowView.h"
//#import "SBJson.h"
#import "NSObject+SBJson.h"
#import "ImageViewController.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

NSUserDefaults *userdefaults;

@interface Dashboard ()<UIViewControllerTransitioningDelegate,ProfileViewPageDetegale,GMSMapViewDelegate,CFCoverFlowViewDelegate>
{
    GMSMapView *mapView_, *changeMap;
    UIView *viewForSlider;
    UILabel *labelToShowCurrentRadiusValue;
    NSArray*    longScroller_Views;
}
@property (nonatomic, strong) ALAssetsLibrary *specialLibrary;
@property (nonatomic) UIImage *image;
@end

@implementation Dashboard
@synthesize navigationTag;
- (void)viewDidLoad {
    [super viewDidLoad];
    
 
    

    
    
    
    
//    _submitBtn.alpha=0.6;
//    _submitBtn.userInteractionEnabled=FALSE;
    
    mapOnTag=0;
    _addressField.text=@"";
    _goBtn.hidden=YES;
    _nameLabel.returnKeyType = UIReturnKeyDone;
    _emailLabel.returnKeyType = UIReturnKeyDone;
    _phoneLabel.returnKeyType = UIReturnKeyDone;
    
    _passwordField.returnKeyType = UIReturnKeyDone;
    _addressField.returnKeyType = UIReturnKeyDone;
    
    if([ [ UIScreen mainScreen ] bounds ].size.height==480)
    {
        _backgroundView.frame=CGRectMake(0, 0, 320, 568);
        _transparentView.frame=CGRectMake(0, 0, 320, 568);
        
    }
    
    
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    userdefaults = [NSUserDefaults standardUserDefaults];
    
    [_backyardLocationBtn setTitle:[NSString stringWithFormat:@"LOCATION: %@",[[userdefaults valueForKey:@"userdetails"] valueForKey:@"address"]] forState:UIControlStateNormal];
    
    _transparentView.hidden=YES;
    _popupView.hidden=YES;
    _popupView1.hidden=YES;
    
    _popupView.layer.cornerRadius = 8.0;
    _popupView.clipsToBounds = YES;
    
    _popupView1.layer.cornerRadius = 8.0;
    _popupView1.clipsToBounds = YES;
    
//    [self locationToAddress];
    
    
    
    
//    SWRevealViewController *revealViewController = self.revealViewController;
//    if ( revealViewController )
//    {
//        
//        UIButton *button1 = [[UIButton alloc] init];
//        button1.frame=CGRectMake(0,0,26,18);
//        [button1 setBackgroundImage:[UIImage imageNamed: @"menu_icon.png"]
//                           forState:UIControlStateNormal];
//        [button1 addTarget:self.revealViewController action:@selector(revealToggle:)forControlEvents:UIControlEventTouchUpInside];
//        [button1 setBackgroundColor:[UIColor redColor]];
//        UIBarButtonItem *button = [[UIBarButtonItem alloc]
//                                   initWithCustomView:button1];
//        self.navigationItem.LeftBarButtonItem = button;
//        
//        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//    }
//    
//    
//    [_menuBtn addTarget:self.revealViewController action:@selector(revealToggle:)forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    
//    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"mapviewpush"];
//    [self.navigationController pushViewController:controller animated:NO];

    
    
    
//    _saveEditBtn.selected=NO;
//    [_saveEditBtn setTitle:@"EDIT" forState:UIControlStateNormal];
    _emailLabel.userInteractionEnabled=FALSE;
//    _nameLabel.userInteractionEnabled=FALSE;
//    _phoneLabel.userInteractionEnabled=FALSE;
//    _editProfileBtn.userInteractionEnabled=FALSE;
    
    _scroll.frame=CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64);//20
    if([ [ UIScreen mainScreen ] bounds ].size.height==480)
    {
        _scroll.frame=CGRectMake(0, 64, self.view.frame.size.width, 568+64);
        
    }
    
    
//    _scroll.contentSize=CGSizeMake(self.view.frame.size.width, _emailLabel.frame.origin.y+_emailLabel.frame.size.height+100);
    
    
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    _addressLabel.text=[[defaults valueForKey:@"userdetails"] valueForKey:@"address"];
    
    _nameLabel.text=[[defaults valueForKey:@"userdetails"] valueForKey:@"name"];
    _emailLabel.text=[[defaults valueForKey:@"userdetails"] valueForKey:@"email"];
    _phoneLabel.text=[[defaults valueForKey:@"userdetails"] valueForKey:@"phone_no"];
    
    NSString *Imageurl=[[defaults valueForKey:@"userdetails"] valueForKey:@"profile_image"];//@"http://www.newyorker.com/wp-content/uploads/2012/07/chris-nolan.jpg";
    
    NSLog(@"URL: %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"profile_image"]);
    
//    [_topImageView sd_setImageWithURL:[NSURL URLWithString:Imageurl]
//                       placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    
    
    
    
    
    
    
    
    
//    [_topImageView sd_setImageWithURL:[NSURL URLWithString:Imageurl]
//                      placeholderImage:[UIImage imageNamed:@"noimages.png"]
//                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                                 
//
//                                 UIImage *effectImage = [UIImageEffects imageByApplyingLightEffectToImage:_topImageView.image];
//                                 _topImageView.image=effectImage;
//
//                             
//                             }];
    
    
    
    
    
    
    
    
    
    [_circleImageView sd_setImageWithURL:[NSURL URLWithString:Imageurl]
                     placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    
    
    
    NSLog(@"  %f   %f",_circleImageView.frame.size.width,_circleImageView.frame.size.height);
    
    _circleImageView.frame=CGRectMake(_circleImageView.frame.origin.x, _circleImageView.frame.origin.y, _circleImageView.frame.size.width, _circleImageView.frame.size.width);
    
    
    _circleImageView.image =    [UIImage getRoundedRectImageFromImage:_circleImageView.image onReferenceView:_circleImageView withCornerRadius:_circleImageView.frame.size.width/2];
    _circleImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    _circleImageView.layer.cornerRadius = _circleImageView.frame.size.width/2;
    _circleImageView.layer.borderWidth = 2;
    _circleImageView.clipsToBounds = YES;
    
    

   
    
    
//    SVBlurView *blurView = [[SVBlurView alloc] initWithFrame:_topImageView.frame];
////    @property (nonatomic, readwrite) CGFloat blurRadius; // default is 20.0f
////    @property (nonatomic, readwrite) CGFloat saturationDelta; // default is 1.5
////    @property (nonatomic, readwrite) UIColor *tintColor;
//    blurView.saturationDelta=2.0;
//    blurView.blurRadius=20.0f;
//    blurView.tintColor=[UIColor clearColor];
//    [_topImageView addSubview:blurView];
    
    
    
//    UIImage *effectImage = [UIImageEffects imageByApplyingLightEffectToImage:_topImageView.image];
//    _topImageView.image=effectImage;
    

    
//    [_coverFlowView1 removeGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
//        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
        
        
//        [_backyardLocationBtn addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//        [_editProfileBtn addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//        [_container addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//        [_nameLabel addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//        [_phoneLabel addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//        [_emailLabel addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//        [_goToMapBtn addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [_scrollBackground addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
        
        for(UIView *vc in [_scroll subviews])
        {
            
            NSLog(@"vc %@",vc);
            if(![vc isKindOfClass:[_coverFlowView1 class] ])
            {
//                [vc addGestureRecognizer:self.revealViewController.panGestureRecognizer];
            }
        }
    }
    
    SWRevealViewController *revealController = [self revealViewController];
    self.revealViewController.delegate = self;
    tap = [revealController tapGestureRecognizer];
    tap.delegate = self;
    tap.enabled=NO;
    [self.view addGestureRecognizer:tap];
    
    
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard:)];
    [_scroll addGestureRecognizer:gestureRecognizer];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
    [self mapShow];
    });
    
    
    
    
    
//    _coverFlowView1 = [[CFCoverFlowView alloc] initWithFrame:CGRectMake(0, 0, _scrollView.frame.size.width, _scrollView.frame.size.height)];
//    _coverFlowView1.backgroundColor = [UIColor groupTableViewBackgroundColor];
//    _coverFlowView1.pageItemWidth = _scrollView.frame.size.width-50;
//    _coverFlowView1.pageItemCoverWidth = 10.0;//10.0
//    _coverFlowView1.pageItemHeight = _scrollView.frame.size.height;
//    _coverFlowView1.pageItemCornerRadius = 5.0;
//    [_scrollView addSubview:_coverFlowView1];
    
    [self loadBackyardScroll];
    
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gesture shouldReceiveTouch:(UITouch *)touch {
    if (touch.view == [self view]) {
        return YES;
    }
    return NO;
}

-(void) hideKeyBoard:(id) sender
{
    [_nameLabel resignFirstResponder];
    [_emailLabel resignFirstResponder];
    [_phoneLabel resignFirstResponder];
    [_passwordField resignFirstResponder];
    [_addressField resignFirstResponder];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_nameLabel resignFirstResponder];
    [_emailLabel resignFirstResponder];
    [_phoneLabel resignFirstResponder];
    [_passwordField resignFirstResponder];
    [_addressField resignFirstResponder];
    
    
}

-(void)profileLoad
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _nameLabel.text=[[defaults valueForKey:@"userdetails"] valueForKey:@"name"];
    _emailLabel.text=[[defaults valueForKey:@"userdetails"] valueForKey:@"email"];
    _phoneLabel.text=[[defaults valueForKey:@"userdetails"] valueForKey:@"phone_no"];
    
    NSString *Imageurl=[[defaults valueForKey:@"userdetails"] valueForKey:@"profile_image"];//@"http://www.newyorker.com/wp-content/uploads/2012/07/chris-nolan.jpg";
    
    NSLog(@"URL: %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"profile_image"]);
    
    //    [_topImageView sd_setImageWithURL:[NSURL URLWithString:Imageurl]
    //                       placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    
    
    
    
    
    
    
    
//    [_topImageView sd_setImageWithURL:[NSURL URLWithString:Imageurl]
//                     placeholderImage:[UIImage imageNamed:@"noimages.png"]
//                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                                
//                                
//                                UIImage *effectImage = [UIImageEffects imageByApplyingLightEffectToImage:_topImageView.image];
//                                _topImageView.image=effectImage;
//                                
//                                
//                            }];
    
    
    
    
    
    
    
    
    [_circleImageView sd_setImageWithURL:[NSURL URLWithString:Imageurl]
                        placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    
    
    
    NSLog(@"  %f   %f",_circleImageView.frame.size.width,_circleImageView.frame.size.height);
    
    _circleImageView.frame=CGRectMake(_circleImageView.frame.origin.x, _circleImageView.frame.origin.y, _circleImageView.frame.size.width, _circleImageView.frame.size.width);
    
    
    _circleImageView.image =    [UIImage getRoundedRectImageFromImage:_circleImageView.image onReferenceView:_circleImageView withCornerRadius:_circleImageView.frame.size.width/2];
    _circleImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    _circleImageView.layer.cornerRadius = _circleImageView.frame.size.width/2;
    _circleImageView.layer.borderWidth = 2;
    _circleImageView.clipsToBounds = YES;
}

-(void)mapShow
{
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    float lat=[[[defaults valueForKey:@"userdetails"] valueForKey:@"lat"] floatValue];
    float lon=[[[defaults valueForKey:@"userdetails"] valueForKey:@"long"] floatValue];
    NSString *location=[[defaults valueForKey:@"userdetails"] valueForKey:@"address"];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                            longitude:lon
                                                                 zoom:12.0];
    
    //plotting the google map
    
    int diff=self.view.frame.size.height-(_goToMapBtn.frame.origin.y+_goToMapBtn.frame.size.height+10);
    NSLog(@"DIFF:%d",diff);
    
    int mapHeight=0;
    if(diff<=90)
    {
        mapHeight=90;
    }
    else
    {
        mapHeight=diff;
    }
    
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0.0,_goToMapBtn.frame.origin.y+_goToMapBtn.frame.size.height+10 , self.view.frame.size.width, mapHeight) camera:camera];
    mapView_.delegate=self;
    mapView_.settings.myLocationButton = NO;
    mapView_.myLocationEnabled = YES;

    
    
    
    [_scroll addSubview:mapView_];
    
    [_scroll bringSubviewToFront:_transparentView];
    
    _scroll.contentSize=CGSizeMake(self.view.frame.size.width, mapView_.frame.origin.y+mapView_.frame.size.height);
    if([ [ UIScreen mainScreen ] bounds ].size.height==480)
    {
        _scroll.frame=CGRectMake(0, 54, self.view.frame.size.width, 568+54);
        _scroll.contentSize=CGSizeMake(self.view.frame.size.width, mapView_.frame.origin.y+mapView_.frame.size.height+88+110);
        
    }
    
    //Creates a marker at kolkata location.
    
//    NSMutableArray *locationArray=[[NSMutableArray alloc]init];
//    GMSMarker *marker0=[[GMSMarker alloc] init];;
//    marker0.position=CLLocationCoordinate2DMake(22.595769, 88.263639);
//    [locationArray addObject:marker0];
//   
//    
//    for(int i=0;i<[locationArray count];i++)
//    {
//        GMSMarker *marker = [[GMSMarker alloc] init];
//        GMSMarker *marker0 = [[GMSMarker alloc] init];
//        marker0=[locationArray objectAtIndex:i];
//        
//        marker.position = marker0.position;
//        marker.title = @"Howrah";
//        marker.snippet = @"Kolkata";
//        marker.map = mapView_;
//    }
    
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(lat, lon);;
    marker.title = @"My Location";
    marker.snippet = location;
    marker.icon=[UIImage imageNamed:@"point-12.png"];
    marker.map = mapView_;
    
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
    
    UIView *newview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 150, 80)];
    newview.backgroundColor=[UIColor clearColor];
    
    UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 150, 80)];
    imgView.image=[UIImage imageNamed:@"MarkerView.png"];
    [newview addSubview:imgView];
    
    
    
    UILabel *usernameLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, 150, 20)];
    UILabel *productLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, 150, 30)];
    productLabel.numberOfLines=5;
    
    [self customizeLabel:usernameLabel:1];
    [self customizeLabel:productLabel:2];
    
    usernameLabel.text=marker.title;
    productLabel.text=marker.snippet;
    
    [newview addSubview:usernameLabel];
    [newview addSubview:productLabel];
    
    
    //    [ma addSubview:newview];
    
    return newview;
}


#pragma mark GMSMapDelegate


-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
    if(mapOnTag==1)
    {
        double latitude = coordinate.latitude;
        double longitude = coordinate.longitude;
        
        [changeMap clear];
        
//        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
//                                                                longitude:longitude
//                                                                     zoom:8];
        
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(latitude, longitude);;
//        marker.title = @"My Location";
//        marker.snippet = location;
//        [marker setca]
//        changeMap.camera=camera;
        marker.icon=[UIImage imageNamed:@"point-12.png"];
        marker.map = changeMap;
    }
    
    CLLocation *LocationAtual = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    
//    [self latLongToAddress:LocationAtual];
    
    [self getAddressFromLatLon:coordinate.latitude withLongitude:coordinate.longitude];
    
    
    
    NSLog(@"LAT LONG : %f  %f",coordinate.latitude,coordinate.longitude);
    
    //DO SOMETHING HERE WITH THAT INFO
}


-(void)getAddressFromLatLon:(double)pdblLatitude withLongitude:(double)pdblLongitude
{
//    NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps/geo?q=%f,%f&output=csv",pdblLatitude, pdblLongitude];
//    NSError* error;
//    NSString *locationString = [NSString stringWithContentsOfURL:[NSURL URLWithString:urlString] encoding:NSASCIIStringEncoding error:&error];
//    locationString = [locationString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
////    return [locationString substringFromIndex:6];
    
    
    
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=false&key=%@",pdblLatitude, pdblLongitude,appDel.googleApiKey];//AIzaSyBTMbVNi8fLA-dzUQ61L0QbeociKndR8aQ
    NSError* error;
    NSString *locationString = [NSString stringWithContentsOfURL:[NSURL URLWithString:urlString] encoding:NSASCIIStringEncoding error:&error];
    
    NSData *data = [locationString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    NSLog(@"json: %@",json);
    
    if([[json objectForKey:@"results"] count]>0)
    {
    
        NSDictionary *dic = [[json objectForKey:@"results"] objectAtIndex:0];
        NSLog(@"DICT: %@",[dic valueForKey:@"formatted_address"]);
        
        _addressField.text=[dic valueForKey:@"formatted_address"];
        
        latUpdated=[NSString stringWithFormat:@"%f",pdblLatitude];
        lonUpdated=[NSString stringWithFormat:@"%f",pdblLongitude];
    }
}


-(void)latLongToAddress:(CLLocation *)location
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
                       
                       if (error){
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                           
                       }
                       
                       NSLog(@"placemarks=%@",[placemarks objectAtIndex:0]);
                       CLPlacemark *placemark = [placemarks objectAtIndex:0];
                       
                       NSLog(@"placemark.ISOcountryCode =%@",placemark.ISOcountryCode);
                       NSLog(@"placemark.country =%@",placemark.country);
                       NSLog(@"placemark.postalCode =%@",placemark.postalCode);
                       NSLog(@"placemark.administrativeArea =%@",placemark.administrativeArea);
                       NSLog(@"placemark.locality =%@",placemark.locality);
                       NSLog(@"placemark.subLocality =%@",placemark.subLocality);
                       NSLog(@"placemark.subThoroughfare =%@",placemark.subThoroughfare);
                       
                       NSString *locationAddress=[NSString stringWithFormat:@"%@, %@ %@ %@",placemark.administrativeArea,placemark.locality,placemark.subLocality,placemark.country];
                       
                       NSString *zip=placemark.postalCode;
                       
                       NSLog(@"locationAddress %@",locationAddress);
                       
                       _addressField.text=locationAddress;
                       
                   }];
}



-(void)customizeLabel:(UILabel *)customLabel :(int)type
{
    if(type==1)
    {
        customLabel.font=[UIFont fontWithName:@"DINCondensed-Bold" size:15.0];
        [customLabel setTextColor:[UIColor blackColor]];
    }
    else
    {
        customLabel.font=[UIFont fontWithName:@"DINCondensed-Bold" size:13.0];
        [customLabel setTextColor:[UIColor darkGrayColor]];
    }
    
    
    [customLabel setTextAlignment:NSTextAlignmentCenter];
    customLabel.backgroundColor=[UIColor clearColor];
    
}


#pragma mark backyardScrollLoad

-(void)loadBackyardScroll
{
    
//    _backyardScroll.contentSize=CGSizeMake([[userdefaults valueForKey:@"backyard"] count] * _backyardScroll.frame.size.width, _backyardScroll.frame.size.height);
//    [_backyardScroll setContentOffset:CGPointMake(0,0) animated:YES];
//    
//    
//    for(UIView *vc in [_backyardScroll subviews])
//    {
//        if([vc isKindOfClass:[UIImageView class]])
//        {
//            [vc removeFromSuperview];
//        }
//        
//    }
//    
//    
//    
//    for(int i=0; i<[[userdefaults valueForKey:@"backyard"] count];i++)
//    {
//        UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(_backyardScroll.frame.size.width * i, 0, _backyardScroll.frame.size.width, _backyardScroll.frame.size.height)];
//        
//        NSString *imageString=[[[userdefaults valueForKey:@"backyard"] objectAtIndex:i] valueForKey:@"backyard_image"];//@"http://www.newyorker.com/wp-content/uploads/2012/07/chris-nolan.jpg";
//        [imgView sd_setImageWithURL:[NSURL URLWithString:imageString]
//                                  placeholderImage:[UIImage imageNamed:@"noimages.png"]];
//
//        [_backyardScroll addSubview:imgView];
//        
//    }
    
    
    
    
    
    [_coverFlowView1 removeFromSuperview];
    
    
    _coverFlowView1 = [[CFCoverFlowView alloc] initWithFrame:CGRectMake(0, 0, _scrollView.frame.size.width, _scrollView.frame.size.height)];
    _coverFlowView1.delegate=self;
    _coverFlowView1.backgroundColor = [UIColor groupTableViewBackgroundColor];
    _coverFlowView1.pageItemWidth = _scrollView.frame.size.width;//-50
    _coverFlowView1.pageItemCoverWidth = 0.0;//10.0
    _coverFlowView1.pageItemHeight = _scrollView.frame.size.height;
    _coverFlowView1.pageItemCornerRadius = 1.0;
    [_scrollView addSubview:_coverFlowView1];
    
    if([[userdefaults valueForKey:@"backyard"] count]>0)
    {
        _coverFlowView1.userInteractionEnabled=TRUE;
        
        NSMutableArray *backArray=[[NSMutableArray alloc]init];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        for(int i=0;i<[[defaults valueForKey:@"backyard"] count];i++)
        {
            [backArray addObject:[[[defaults valueForKey:@"backyard"] objectAtIndex:i] valueForKey:@"backyard_image"]];
        }
        
        [_coverFlowView1 setPageItemsWithImageNames:backArray];
        
//        [_coverFlowView1 setPageItemsWithImageNames:[userdefaults valueForKey:@"backyard"]];
    }
//    else if([[userdefaults valueForKey:@"backyard"] count]==1)
//    {
//        _coverFlowView1.userInteractionEnabled=FALSE;
//        
//        NSMutableArray *backArray=[[NSMutableArray alloc]init];
//        
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        for(int i=0;i<[[defaults valueForKey:@"backyard"] count];i++)
//        {
//            [backArray addObject:[[[defaults valueForKey:@"backyard"] objectAtIndex:i] valueForKey:@"backyard_image"]];
//        }
//        
//        [_coverFlowView1 setPageItemsWithImageNames:backArray];
//
//    }
    else
    {
        _coverFlowView1.userInteractionEnabled=FALSE;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

- (void)coverFlowImageShow:(NSString *)imageUrl
{
    NSLog(@"URL IMAGE: %@",imageUrl);
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
//    [self.navigationController popToViewController:controller animated:YES];
    controller.imageUrl=imageUrl;
    [self.navigationController pushViewController:controller animated:NO];
}


- (void)coverFlowView:(CFCoverFlowView *)coverFlowView didScrollPageItemToIndex:(NSInteger)index {
    NSLog(@"didScrollPageItemToIndex >>> %@", @(index));
    
    self.pageControl.currentPage = index;
}

- (void)coverFlowView:(CFCoverFlowView *)coverFlowView didSelectPageItemAtIndex:(NSInteger)index {
    NSLog(@"didSelectPageItemAtIndex >>> %@", @(index));
}


//- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender
//{
//    // The key is repositioning without animation
//    if (_backyardScroll.contentOffset.x == 0) {
//        // user is scrolling to the left from image 1 to image 10.
//        // reposition offset to show image 10 that is on the right in the scroll view
//        [_backyardScroll scrollRectToVisible:CGRectMake(([[userdefaults valueForKey:@"backyard"] count]-2) * _backyardScroll.frame.size.width,0,_backyardScroll.frame.size.width,_backyardScroll.frame.size.height) animated:NO];
//    }
//    else if (_backyardScroll.contentOffset.x == ([[userdefaults valueForKey:@"backyard"] count]-1) * _backyardScroll.frame.size.width) {
//        // user is scrolling to the right from image 10 to image 1.
//        // reposition offset to show image 1 that is on the left in the scroll view
//        [_backyardScroll scrollRectToVisible:CGRectMake(_backyardScroll.frame.size.width,0,_backyardScroll.frame.size.width,_backyardScroll.frame.size.height) animated:NO];
//    }
//    
//    
//    
//    
//}


////Moving the scroll view to the desired offset
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView1{
//    
//    if (scrollView1.contentOffset.x <=([[userdefaults valueForKey:@"backyard"] count]-1)*_backyardScroll.frame.size.width) {
//        [_backyardScroll setContentOffset:CGPointMake(([[userdefaults valueForKey:@"backyard"] count]+([[userdefaults valueForKey:@"backyard"] count]-1))*_backyardScroll.frame.size.width, _backyardScroll.frame.size.height)];
//    }
//    else if (scrollView1.contentOffset.x >=(2*([[userdefaults valueForKey:@"backyard"] count]))*_backyardScroll.frame.size.width) {
//        [_backyardScroll setContentOffset:CGPointMake(([[userdefaults valueForKey:@"backyard"] count])*_backyardScroll.frame.size.width, _backyardScroll.frame.size.height)];
//    }
//}
//
//
//- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView1{
//    
//    if (scrollView1.contentOffset.x <=([[userdefaults valueForKey:@"backyard"] count]-1)*_backyardScroll.frame.size.width) {
//        [_backyardScroll setContentOffset:CGPointMake(([[userdefaults valueForKey:@"backyard"] count]+([[userdefaults valueForKey:@"backyard"] count]-1))*_backyardScroll.frame.size.width, _backyardScroll.frame.size.height)];
//    }
//    else if (scrollView1.contentOffset.x >=(2*([[userdefaults valueForKey:@"backyard"] count]))*_backyardScroll.frame.size.width) {
//        [_backyardScroll setContentOffset:CGPointMake(([[userdefaults valueForKey:@"backyard"] count])*_backyardScroll.frame.size.width, _backyardScroll.frame.size.height)];
//    }
//}





#pragma mark collectionview delegates



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSLog(@"COUNT: %d",[[userdefaults valueForKey:@"backyard"] count]);
    
    return [[userdefaults valueForKey:@"backyard"] count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    
    NSString *imageString=[[[userdefaults valueForKey:@"backyard"] objectAtIndex:indexPath.row] valueForKey:@"backyard_image"];//@"http://www.newyorker.com/wp-content/uploads/2012/07/chris-nolan.jpg";
    
    [cell.backYardImageView sd_setImageWithURL:[NSURL URLWithString:imageString]
                       placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    
    
    
    return cell;
}



- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGSize returnSize = CGSizeZero;
    
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        
        returnSize = CGSizeMake(383.0, 164.0);
        
        // iPad
    } else {
        // iPhone
        
        
        
        if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
        {
            returnSize = CGSizeMake(375.0, 237.0);
        }
        else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
        {
            returnSize = CGSizeMake(414.0, 262.0);
        }
        else
        {
            returnSize = CGSizeMake(320.0, 202.0);
        }
    }
    
    return returnSize;
}





-(IBAction)leftSwipeBtnAction:(id)sender
{
    
    if([[userdefaults valueForKey:@"backyard"] count]>0)
    {
        NSArray *visibleItems0 = [_collection indexPathsForVisibleItems];
        NSIndexPath *currentItem0 = [visibleItems0 objectAtIndex:0];
        
        
    //    if([[userdefaults valueForKey:@"backyard"] count] - currentItem0.row>=1)
        if(currentItem0.row>=1)
        {
            NSArray *visibleItems = [_collection indexPathsForVisibleItems];
            NSIndexPath *currentItem = [visibleItems objectAtIndex:0];
            NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item - 1 inSection:currentItem.section];
            [_collection scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
            
            NSLog(@"INDEXPATH: %d",nextItem.row);
        }
    }
}



-(IBAction)rightSwipeBtnAction:(id)sender
{
    
    
    if([[userdefaults valueForKey:@"backyard"] count]>0)
    {
        
        NSArray *visibleItems0 = [_collection indexPathsForVisibleItems];
        NSIndexPath *currentItem0 = [visibleItems0 objectAtIndex:0];
        
        
    //    if([[userdefaults valueForKey:@"backyard"] count] - currentItem0.row>=2)
        if(currentItem0.row<[[userdefaults valueForKey:@"backyard"] count]-1)
        {
        
            NSArray *visibleItems = [_collection indexPathsForVisibleItems];
            NSIndexPath *currentItem = [visibleItems objectAtIndex:0];
            NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item + 1 inSection:currentItem.section];
            [_collection scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
            NSLog(@"INDEXPATH: %d",nextItem.row);
        }
    }
    
    
}


-(IBAction)saveEditBtnAction:(id)sender
{
//    if(_saveEditBtn.selected)
//    {
//        _saveEditBtn.selected=NO;
//        [_saveEditBtn setTitle:@"EDIT" forState:UIControlStateNormal];
//        _emailLabel.userInteractionEnabled=FALSE;
//        _nameLabel.userInteractionEnabled=FALSE;
//        _phoneLabel.userInteractionEnabled=FALSE;
//        _editProfileBtn.userInteractionEnabled=FALSE;
    
        
        [self editProfileWebservice];
        
        
        
        
        
        
        
        
//    }
//    else
//    {
//        _saveEditBtn.selected=YES;
//        [_saveEditBtn setTitle:@"SAVE" forState:UIControlStateSelected];
////        _emailLabel.userInteractionEnabled=TRUE;
//        _nameLabel.userInteractionEnabled=TRUE;
//        _phoneLabel.userInteractionEnabled=TRUE;
//        _editProfileBtn.userInteractionEnabled=TRUE;
//    }
    
}


-(void)editProfileWebservice
{
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/EditProfile",appDel.hostUrl];
    
    NSLog(@"ID %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"]);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],@"Profile[user_id]",_nameLabel.text, @"Profile[name]",_phoneLabel.text,@"Profile[phone_no]"  ,nil];
    
    [manager POST:url parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        
        NSData * imgData;
        
        imgData = UIImagePNGRepresentation(_circleImageView.image);
        
        if(imgData!=NULL){
            
            [formData appendPartWithFileData:imgData name:@"profileImage" fileName:@"avatar.png" mimeType:@"image/png"];
        }
        
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        
        NSLog(@"JSON: %@", responseObject);
        
        if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
        {
            
            
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[responseObject valueForKey:@"profile"] forKey:@"userdetails"];
            [defaults synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"profilepicchange" object:nil userInfo:nil];
            
            [appDel.alertview showInfo:self title:@"Profile Update" subTitle:@"Profile updated successfully" closeButtonTitle:@"OK" duration:0.0f];
        }
        else
        {
            [appDel.alertview showInfo:self title:@"Profile Update" subTitle:[responseObject valueForKey:@"message"] closeButtonTitle:@"OK" duration:0.0f];
        }
        
        
        [self profileLoad];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        NSLog(@"Error: %@", error);
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
        
        
    }];
}

-(IBAction)viewProfileImageBtnAction:(id)sender
{
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    //    [self.navigationController popToViewController:controller animated:YES];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *Imageurl=[[defaults valueForKey:@"userdetails"] valueForKey:@"profile_image"];
    controller.imageUrl=Imageurl;
    [self.navigationController pushViewController:controller animated:NO];
}



-(IBAction)editProfileBtnAction:(id)sender
{
    imagePickerTag=1;
    
    NSString *other1 = @"Choose from Gallery";
    NSString *other2 = @"Take Picture";
    NSString *other3 = @"Delete Picture";
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, other3,nil];
    
    [actionSheet showInView:self.view];
}

-(IBAction)uploadBackyardImageBtnAction:(id)sender
{
    imagePickerTag=2;
    
    NSString *other1 = @"Choose from Gallery";
    NSString *other2 = @"Take Picture";
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,nil];
    
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (buttonIndex) {
        case 0:
            
            if(imagePickerTag==2)
            {
                [self chooseMultipleImagesFromGallery];
            }
            else
            {
                [self chooseFromGallery];
            }
            
            
            break;
        case 1:
            [self takePhoto];
            break;
            
        case 2:
            
//            [self takePhoto];
            
            if(imagePickerTag==1)
            {
            
                _circleImageView.image=[UIImage imageNamed:@"noimages.png"];
                
                [self editProfileWebservice];
            }
            
            
            break;
            
        default:
            break;
    }
}

#pragma mark chooseMultipleImagesFromGallery

-(void)chooseMultipleImagesFromGallery
{
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
    
    elcPicker.maximumImagesCount = 100; //Set the maximum number of images to select to 100
    elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
    elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
    elcPicker.onOrder = YES; //For multiple image selection, display and return order of selected images
    elcPicker.mediaTypes = @[(NSString *)kUTTypeImage]; //Supports image and movie types // (NSString *)kUTTypeMovie
    
    elcPicker.imagePickerDelegate = self;
    
    [self presentViewController:elcPicker animated:YES completion:nil];
}


- (void)displayPickerForGroup:(ALAssetsGroup *)group
{
    ELCAssetTablePicker *tablePicker = [[ELCAssetTablePicker alloc] initWithStyle:UITableViewStylePlain];
    tablePicker.singleSelection = YES;
    tablePicker.immediateReturn = YES;
    
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initWithRootViewController:tablePicker];
    elcPicker.maximumImagesCount = 1;
    elcPicker.imagePickerDelegate = self;
    elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
    elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
    elcPicker.onOrder = NO; //For single image selection, do not display and return order of selected images
    tablePicker.parent = elcPicker;
    
    // Move me
    tablePicker.assetGroup = group;
    [tablePicker.assetGroup setAssetsFilter:[ALAssetsFilter allAssets]];
    
    [self presentViewController:elcPicker animated:YES completion:nil];
}


#pragma mark ELCImagePickerControllerDelegate Methods

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    NSLog(@"info %@",info);
    
    
    images = [NSMutableArray arrayWithCapacity:[info count]];
    for (NSDictionary *dict in info) {
        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                
                
//                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                
                
                
                UIImage *chosenImage = [dict objectForKey:UIImagePickerControllerOriginalImage];
                
//                float ratio = _collection.frame.size.width/_collection.frame.size.height;
//                chosenImage = [self imageWithImage:chosenImage scaledToSize:CGSizeMake(_collection.frame.size.width, _collection.frame.size.height/ratio)];
                
                
                
                [images addObject:chosenImage];
                
                
                
                
                
                
                
            } else {
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
            
            
            
            
        }
    }
    
    if([images count]>0)
    {
        [self uploadBackyardWebservice];
    }
    
    
    
    NSLog(@"image %@",images);
    
    
    
    
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}







#pragma mark- Take photo
-(void)takePhoto{
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        //  picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"device not support." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
        
    }
    
    
    
}

#pragma mark- choose from gallery
-(void)chooseFromGallery{
    
    
    if ([UIImagePickerController  isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        // picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"device not support." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
        
    }
    
    
    
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    UIImage *chosenImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    float ratio = chosenImage.size.width/chosenImage.size.height;
    chosenImage = [self imageWithImage:chosenImage scaledToSize:CGSizeMake(177, 174/ratio)];
    
    if(imagePickerTag==1)
    {
        _image1 = nil;
        _image1 = chosenImage;
        
        //    _topImageView.image=chosenImage;
        _circleImageView.image=chosenImage;
        
        UIImage *effectImage = [UIImageEffects imageByApplyingLightEffectToImage:chosenImage];
//        _topImageView.image=effectImage;
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
    else
    {
        _image2 = nil;
        _image2 = chosenImage;
        
        
        images=[[NSMutableArray alloc]init];
        
        if(_image2 !=nil)
        {
            [images addObject:_image2];
        }
        
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
        if([images count]>0)
        {
        
            [self uploadBackyardWebservice];
        }

    }
    
    
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


#pragma mark uploadBackyardWebservice

-(void)uploadBackyardWebservice
{
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/UploadBackyard",appDel.hostUrl];
    
    NSLog(@"ID %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"]);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],@"Profile[user_id]" ,nil];
    
    [manager POST:url parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        
//        NSData * imgData;
//        
//        imgData = UIImagePNGRepresentation(_image2);
//        
//        if(imgData!=NULL){
//            [formData appendPartWithFileData:imgData name:@"image" fileName:@"avatar.png" mimeType:@"image/png"];
//        }
        
        
        
        
        int i = 0;
        for(UIImage *eachImage in images)
        {
            NSData *imageData = UIImageJPEGRepresentation(eachImage,0.5);
            
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"images[%d]",i] fileName:@"images" mimeType:@"image/jpeg"];//[NSString stringWithFormat:@"%@file%d.jpg",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],i ]
            i++;
        }
        
        
        
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        
        NSLog(@"JSON: %@", responseObject);
        
        if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
        {
            
            
            
            userdefaults = [NSUserDefaults standardUserDefaults];
            [userdefaults setObject:[responseObject valueForKey:@"backyard"] forKey:@"backyard"];
            [userdefaults synchronize];
            
            
            
//            dispatch_async(dispatch_get_main_queue(), ^{
                [_collection reloadData];
            
            [self loadBackyardScroll];
            
//            });
            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [_collection scrollToItemAtIndexPath:0 atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
//            });
            
            
            
            [appDel.alertview showInfo:self title:@"Backyard Image" subTitle:@"Backyard image uploaded successfully" closeButtonTitle:@"OK" duration:0.0f];
        }
        else
        {
            [appDel.alertview showInfo:self title:@"Backyard Image" subTitle:[responseObject valueForKey:@"message"] closeButtonTitle:@"OK" duration:0.0f];
        }
        
        
        [self profileLoad];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        NSLog(@"Error: %@", error);
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
        
        
    }];
}




#pragma mark - Image Scaling
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    
    
    CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
    
    mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                     zoom:12.0];
    
    labelToShowCurrentRadiusValue.text = [NSString stringWithFormat:@"%f , %f", location.coordinate.latitude, location.coordinate.longitude];
    
    
}




#pragma mark - SWRevealViewController Delegate Methods



- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        tap.enabled=NO;
//        table.userInteractionEnabled=TRUE;
    } else {
        tap.enabled=YES;
//        table.userInteractionEnabled=FALSE;
        
    }
    
    
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"dashboardnav"] || [segue.identifier isEqualToString:@"profilenav"]){
        
        TopNavigation *vc = (TopNavigation*)[segue destinationViewController];
        vc.titleString=@"PROFILE";
        vc.fromWhere=3;
        vc.backgroundImageTag=1;
        vc.delegate = self;
        
        
        
        
        
        
        
        
        
    }
    
}

#pragma mark - resize image

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize andOffSet:(CGPoint)offSet{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(offSet.x, offSet.y, newSize.width-offSet.x, newSize.height-offSet.y)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



#pragma mark - Map View (Location Update)

-(IBAction)goToMapBtnAction:(id)sender
{
    mapOnTag=1;
    
    [self setMapViewForChange];
    
    
    
    _transparentView.hidden=NO;
    _popupView.hidden=NO;
    
    [_popupView setFrame:CGRectMake(_popupView.frame.origin.x, self.view.frame.size.height, _popupView.frame.size.width, _popupView.frame.size.height)];
    
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
        
        [_popupView setFrame:CGRectMake(_popupView.frame.origin.x, (self.view.frame.size.height-_popupView.frame.size.height)/2, _popupView.frame.size.width, _popupView.frame.size.height)];
        
        
    } completion:^(BOOL finished) {
        
        
//        [self.view bringSubviewToFront:_transparentView];
        
//        [mapView_ sendSubviewToBack:_transparentView];
//        
//        mapView_.hidden=YES;
        
        
    }];

}

#pragma mark changeMap

-(void)setMapViewForChange
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    float lat=[[[defaults valueForKey:@"userdetails"] valueForKey:@"lat"] floatValue];
    float lon=[[[defaults valueForKey:@"userdetails"] valueForKey:@"long"] floatValue];
    NSString *location=[[defaults valueForKey:@"userdetails"] valueForKey:@"address"];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                            longitude:lon
                                                                 zoom:12.0];
    changeMap = [GMSMapView mapWithFrame:CGRectMake(0.0,0.0,_changeMapView.frame.size.width , _changeMapView.frame.size.height) camera:camera];
    changeMap.delegate=self;
    changeMap.settings.myLocationButton = NO;
    changeMap.myLocationEnabled = YES;
    [_changeMapView addSubview:changeMap];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(lat, lon);;
    marker.title = @"My Location";
    marker.snippet = location;
    marker.icon=[UIImage imageNamed:@"point-12.png"];
    marker.map = changeMap;
}


#pragma mark - Map View (Location Update)

-(IBAction)changePasswordBtnAction:(id)sender
{
    _transparentView.hidden=NO;
    _popupView1.hidden=NO;
    
    [_popupView1 setFrame:CGRectMake(_popupView1.frame.origin.x, self.view.frame.size.height, _popupView1.frame.size.width, _popupView1.frame.size.height)];
    
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
        
        [_popupView1 setFrame:CGRectMake(_popupView1.frame.origin.x, (self.view.frame.size.height-_popupView1.frame.size.height)/2, _popupView1.frame.size.width, _popupView1.frame.size.height)];
        
    } completion:^(BOOL finished) {
        
        
    }];
    
}





-(IBAction)submitBtnAction:(id)sender
{
    [_addressField resignFirstResponder];
    
    if([_addressField.text isEqualToString:@""])
    {
        [appDel.alertview showInfo:self title:@"Update Map" subTitle:@"Please provide the address" closeButtonTitle:@"OK" duration:0.0f];
    }
    else
    {
        [self mapSaveWebservice];
    }
}

-(IBAction)submitBtnAction1:(id)sender
{
    [_passwordField resignFirstResponder];
    
    if([_passwordField.text isEqualToString:@""])
    {
        [appDel.alertview showInfo:self title:@"Update Password" subTitle:@"Please provide a password" closeButtonTitle:@"OK" duration:0.0f];
    }
    else
    {
        [self changePasswordWebservice];
    }
}




- (UIImage *)blurWithCoreImage:(UIImage *)sourceImage
{
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
    
    // Apply Affine-Clamp filter to stretch the image so that it does not
    // look shrunken when gaussian blur is applied
    CGAffineTransform transform = CGAffineTransformIdentity;
    CIFilter *clampFilter = [CIFilter filterWithName:@"CIAffineClamp"];
    [clampFilter setValue:inputImage forKey:@"inputImage"];
    [clampFilter setValue:[NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)] forKey:@"inputTransform"];
    
    // Apply gaussian blur filter with radius of 30
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
    [gaussianBlurFilter setValue:clampFilter.outputImage forKey: @"inputImage"];
    [gaussianBlurFilter setValue:@300 forKey:@"inputRadius"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:gaussianBlurFilter.outputImage fromRect:[inputImage extent]];
    
    // Set up output context.
    UIGraphicsBeginImageContext(self.view.frame.size);
    CGContextRef outputContext = UIGraphicsGetCurrentContext();
    
    // Invert image coordinates
    CGContextScaleCTM(outputContext, 1.0, -1.0);
    CGContextTranslateCTM(outputContext, 0, -self.view.frame.size.height);
    
    // Draw base image.
    CGContextDrawImage(outputContext, self.view.frame, cgImage);
    
    // Apply white tint
    CGContextSaveGState(outputContext);
    CGContextSetFillColorWithColor(outputContext, [UIColor colorWithWhite:1 alpha:0.2].CGColor);
    CGContextFillRect(outputContext, self.view.frame);
    CGContextRestoreGState(outputContext);
    
    // Output image is ready.
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return outputImage;
}


-(void)locationToAddress
{
    
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    float lat=[[[defaults valueForKey:@"userdetails"] valueForKey:@"lat"] floatValue];
    float lon=[[[defaults valueForKey:@"userdetails"] valueForKey:@"long"] floatValue];
    CLLocation *createdLocation = [[CLLocation alloc]initWithLatitude:lat
                                                            longitude:lon];
    [geocoder reverseGeocodeLocation:createdLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
                       
                       if (error){
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                           
                       }
                       
                       NSLog(@"placemarks=%@",[placemarks objectAtIndex:0]);
                       CLPlacemark *placemark = [placemarks objectAtIndex:0];
                       
                       NSLog(@"placemark.ISOcountryCode =%@",placemark.ISOcountryCode);
                       NSLog(@"placemark.country =%@",placemark.country);
                       NSLog(@"placemark.postalCode =%@",placemark.postalCode);
                       NSLog(@"placemark.administrativeArea =%@",placemark.administrativeArea);
                       NSLog(@"placemark.locality =%@",placemark.locality);
                       NSLog(@"placemark.subLocality =%@",placemark.subLocality);
                       NSLog(@"placemark.subThoroughfare =%@",placemark.subThoroughfare);
                       
                       NSString *locationAddress=[NSString stringWithFormat:@"%@ %@ %@ %@",placemark.administrativeArea,placemark.locality,placemark.subLocality,placemark.country];
                       
                       
                       NSLog(@"locationAddress %@",locationAddress);
                       
                       _addressLabel.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",placemark.administrativeArea,placemark.locality,placemark.subLocality,placemark.country,placemark.postalCode];
                       
                   }];
}


#pragma mark Textfield delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    CGRect viewFrame = self.view.frame;
    
    CGRect viewFrame1 = _container.frame;
    
    
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        if(textField==_addressField)
        {
            viewFrame.origin.y = -180;
            viewFrame1.origin.y = 180;
        }
        else
        {
        viewFrame.origin.y = -80;
        viewFrame1.origin.y = 80;
        }
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        if(textField==_addressField)
        {
            viewFrame.origin.y = -180;
            viewFrame1.origin.y = 180;
        }
        else
        {
        viewFrame.origin.y = -90;
        viewFrame1.origin.y = 90;
        }
    }
    else
    {
        if(textField==_addressField)
        {
            viewFrame.origin.y = -180;//180
            viewFrame1.origin.y = 180;//180
        }
        else
        {
        viewFrame.origin.y = -140;//140
        viewFrame1.origin.y = 140;//140
        }
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.5];
    [_container setFrame:viewFrame1];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGRect viewFrame = self.view.frame;
    CGRect viewFrame1 = _container.frame;
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        viewFrame.origin.y = +0;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        viewFrame.origin.y = +0;
    }
    else
    {
        viewFrame.origin.y = +0;
    }
    
    viewFrame1.origin.y = 0;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.5];
    
    [_container setFrame:viewFrame1];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == _addressField)
    {
        
        if([_addressField.text isEqualToString:@""])
        {
            _goBtn.hidden=YES;
        }
        else
        {
            _goBtn.hidden=NO;
        }
        NSLog(@"NSSTRING: %@     %@",textField.text,string);
       
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
//            float lat,lon;
            
            CLLocationCoordinate2D coordinate=[self getLocationFromAddressString:[NSString stringWithFormat:@"%@%@",textField.text,string]];
            latUpdated=[NSString stringWithFormat:@"%f",coordinate.latitude];
            lonUpdated=[NSString stringWithFormat:@"%f",coordinate.longitude];
            
//            latUpdated=[NSString stringWithFormat:@"%f",[self getLocationFromAddressString:textField.text].latitude];
//            lonUpdated=[NSString stringWithFormat:@"%f",[self getLocationFromAddressString:textField.text].longitude];
            
            
            
            
//            lat=[latUpdated floatValue];
//            lon=[lonUpdated floatValue];
            
            
            
            
//            double latitude = [latUpdated floatValue];
//            double longitude = [lonUpdated floatValue];
//            
//            [changeMap clear];
//            
//            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
//                                                                            longitude:longitude
//                                                                                 zoom:20];
//            
//            
//            GMSMarker *marker = [[GMSMarker alloc] init];
//            marker.position = CLLocationCoordinate2DMake(latitude, longitude);;
////            marker.title = @"My Location";
////            marker.snippet = location;
//            changeMap.camera=camera;
//            marker.icon=[UIImage imageNamed:@"point-12.png"];
//            marker.map = changeMap;
            
            
            
            
        });

        
        
        
    }
    
    return YES;
}

-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr {
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    NSString *req = [NSString stringWithFormat:@"https://maps.google.com/maps/api/geocode/json?sensor=true&address=%@&key=%@", esc_addr,appDel.googleApiKey];//AIzaSyBTMbVNi8fLA-dzUQ61L0QbeociKndR8aQ
    
    
    NSString *req =[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?address=%@&sensor=false,type:POST",esc_addr];
    
    
    
    NSDictionary *result = [[NSString stringWithContentsOfURL: [NSURL URLWithString: req] encoding: NSUTF8StringEncoding error: NULL]JSONValue]  ;
    
//    NSError *error;
//    NSData *jsonData = [req dataUsingEncoding:NSUTF8StringEncoding];
//    NSLog(@"result %@",jsonData);
//    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
//    NSDictionary *result = [NSDictionary dictionaryWithContentsOfURL:[NSURL URLWithString: req] ]  ;
    
//    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[req dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    
//    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    
    NSLog(@"result %@",result);
    
    NSString *latString=@"";
    NSString *lngString=@"";
    
    if (result) {
//        NSScanner *scanner = [NSScanner scannerWithString:result];
//        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
//            [scanner scanDouble:&latitude];
//            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
//                [scanner scanDouble:&longitude];
//            }
//        }
        
        NSDictionary *results=[result valueForKey: @"results"];
        NSDictionary *geometryDict = [results valueForKey: @"geometry"];
        NSDictionary *locationDict = [geometryDict valueForKey: @"location"];
        NSArray *latArray = [locationDict valueForKey: @"lat"];
        latString = [latArray lastObject];
        NSArray *lngArray = [locationDict valueForKey: @"lng"];
        lngString = [lngArray lastObject];

        
        
        
        
    }
    
    
    CLLocationCoordinate2D center;
    center.latitude=[latString doubleValue];//latitude;
    center.longitude = [lngString doubleValue];//longitude;
    NSLog(@"View Controller get Location Logitute : %f",center.latitude);
    NSLog(@"View Controller get Location Latitute : %f",center.longitude);
    
    
    
    
    
    
    
    
  
    

    
    
    
    
    return center;
    
}


-(IBAction)goBtnAction:(id)sender
{
    
//    CLLocationCoordinate2D coordinate=[self getLocationFromAddressString:_addressField.text];
//    latUpdated=[NSString stringWithFormat:@"%f",coordinate.latitude];
//    lonUpdated=[NSString stringWithFormat:@"%f",coordinate.longitude];

    
//    _submitBtn.alpha=1.0;
//    _submitBtn.userInteractionEnabled=TRUE;
    
    
    
    
    
    CLLocationCoordinate2D coordinate=[self getLocationFromAddressString:_addressField.text];
    latUpdated=[NSString stringWithFormat:@"%f",coordinate.latitude];
    lonUpdated=[NSString stringWithFormat:@"%f",coordinate.longitude];
    
    
    [self goMethod:latUpdated:lonUpdated];
}


-(void)goMethod:(NSString *)lat :(NSString *)lon
{
    [changeMap clear];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[lat floatValue]
                                                            longitude:[lon floatValue]
                                                                 zoom:12.0];
    
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake([lat floatValue], [lon floatValue]);;
    marker.title = @"My Location";
    marker.snippet = _addressField.text;
    changeMap.camera=camera;
    marker.icon=[UIImage imageNamed:@"point-12.png"];
    marker.map = changeMap;

}





#pragma mark mapSaveWebservice

-(void)mapSaveWebservice
{
    
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/UpdateMap",appDel.hostUrl];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],@"user_id",latUpdated,@"lat",lonUpdated,@"long",_addressField.text,@"address",@"",@"zip",
                           nil];
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        
        NSLog(@"JSON: %@", responseObject);
        
        if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[responseObject valueForKey:@"map"] forKey:@"userdetails"];
            [defaults synchronize];
            
           
            _addressLabel.text=_addressField.text;
            _addressField.text=@"";
            
            
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            alert.shouldDismissOnTapOutside = NO;
            alert.showAnimationType=SlideInToCenter;
            alert.hideAnimationType=SlideOutToCenter;
            [alert alertIsDismissed:^{
                NSLog(@"SCLAlertView dismissed!");
                
                
                
                
//                [_popupView setFrame:CGRectMake(_popupView.frame.origin.x, (self.view.frame.size.height-_popupView.frame.size.height)/2, _popupView.frame.size.width, _popupView.frame.size.height)];
                
                [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
                    
                    
                    
                    
                    
                    [_popupView setFrame:CGRectMake(_popupView.frame.origin.x, self.view.frame.size.height, _popupView.frame.size.width, _popupView.frame.size.height)];
                    
                    
                    
                    
                } completion:^(BOOL finished) {
                    
                    _transparentView.hidden=YES;
                    _popupView.hidden=YES;
                    
                    
                    mapOnTag=0;
                    _addressField.text=@"";
                    _goBtn.hidden=YES;
                    
                    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                    [_backyardLocationBtn setTitle:[NSString stringWithFormat:@"LOCATION: %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"address"]] forState:UIControlStateNormal];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self mapShow];
                    });
                    
                    
                    
                }];
                
            }];
            
            [alert showInfo:self title:@"Map" subTitle:@"Map updated successfully" closeButtonTitle:@"OK" duration:0.0f];
            
        }
        else
        {
            [appDel.alertview showInfo:self title:@"Map" subTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]] closeButtonTitle:@"OK" duration:0.0f];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        NSLog(@"Error: %@", error);
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
        
        
    }];
    
    
    
    
    
    
    
}








#pragma mark changePasswordWebservice

-(void)changePasswordWebservice
{
    
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/ChangePassword",appDel.hostUrl];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],@"User[id]",_passwordField.text,@"User[password]",
                           nil];
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        
        NSLog(@"JSON: %@", responseObject);
        
        if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
        {
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            [defaults setObject:[responseObject valueForKey:@"map"] forKey:@"userdetails"];
//            [defaults synchronize];
            
            
//            _addressLabel.text=_addressField.text;
            _passwordField.text=@"";
            
            
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            alert.shouldDismissOnTapOutside = NO;
            alert.showAnimationType=SlideInToCenter;
            alert.hideAnimationType=SlideOutToCenter;
            [alert alertIsDismissed:^{
                NSLog(@"SCLAlertView dismissed!");
                
                
                
                
                //                [_popupView setFrame:CGRectMake(_popupView.frame.origin.x, (self.view.frame.size.height-_popupView.frame.size.height)/2, _popupView.frame.size.width, _popupView.frame.size.height)];
                
                [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
                    
                    
                    
                    
                    
                    [_popupView1 setFrame:CGRectMake(_popupView1.frame.origin.x, self.view.frame.size.height, _popupView1.frame.size.width, _popupView1.frame.size.height)];
                    
                    
                    
                    
                } completion:^(BOOL finished) {
                    
                    _transparentView.hidden=YES;
                    _popupView1.hidden=YES;
                    
                    
                    
                    
                  
                    
                    
                    
                }];
                
            }];
            
            [alert showInfo:self title:@"Reset Password" subTitle:@"Password updated successfully" closeButtonTitle:@"OK" duration:0.0f];
            
        }
        else
        {
            [appDel.alertview showInfo:self title:@"Reset Password" subTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]] closeButtonTitle:@"OK" duration:0.0f];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        NSLog(@"Error: %@", error);
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
        
        
    }];
    
    
    
    
    
    
    
}



-(IBAction)crossBtnAction:(id)sender
{
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
        
               [_popupView setFrame:CGRectMake(_popupView.frame.origin.x, self.view.frame.size.height, _popupView.frame.size.width, _popupView.frame.size.height)];
        
            } completion:^(BOOL finished) {
                
                _transparentView.hidden=YES;
                _popupView.hidden=YES;
                
                mapOnTag=0;
                _addressField.text=@"";
                _goBtn.hidden=YES;
    }];
}


-(IBAction)crossBtnAction1:(id)sender
{
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
        
        [_popupView1 setFrame:CGRectMake(_popupView1.frame.origin.x, self.view.frame.size.height, _popupView1.frame.size.width, _popupView1.frame.size.height)];
        
    } completion:^(BOOL finished) {
        
        _transparentView.hidden=YES;
        _popupView1.hidden=YES;
    }];
}










- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
