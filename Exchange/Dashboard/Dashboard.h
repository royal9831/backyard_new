//
//  Dashboard.h
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import <CoreImage/CoreImage.h>
#import "AppDelegate.h"
#import "ELCImagePickerHeader.h"
#import "DMCircularScrollView.h"
@class CFCoverFlowView;
@interface Dashboard : UIViewController<UIGestureRecognizerDelegate,SWRevealViewControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,ELCImagePickerControllerDelegate,UINavigationControllerDelegate,UIScrollViewDelegate>
{
    AppDelegate *appDel;
    UITapGestureRecognizer *tap;
    int imagePickerTag;
    NSString *latUpdated, *lonUpdated;
    NSMutableArray *images;
    DMCircularScrollView* longScrollView;
    int mapOnTag;
}
@property (weak, nonatomic) IBOutlet UIView *container, *changeMapView, *scrollBackground;
@property (nonatomic, copy) NSArray *chosenImages;
@property (nonatomic,retain) UIImage *image1,*image2;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn, *goToMapBtn,*saveEditBtn, *editProfileBtn, *goBtn,*backyardLocationBtn, *submitBtn;
@property (weak, nonatomic) IBOutlet UITextField *nameLabel, *phoneLabel,*emailLabel;
@property (nonatomic,retain) IBOutlet UIImageView *topImageView, *circleImageView;
@property (nonatomic,retain) IBOutlet UIScrollView *scroll;
@property int navigationTag;
@property (nonatomic,retain) IBOutlet UICollectionView *collection;
@property (nonatomic,retain) IBOutlet UIView *transparentView, *popupView,*popupView1;
@property (nonatomic,retain) IBOutlet UITextField *addressField, *passwordField;
@property (nonatomic,retain) IBOutlet UILabel *addressLabel;
@property (nonatomic,retain) IBOutlet UIView *backgroundView;
@property (nonatomic,retain) IBOutlet UIScrollView *backyardScroll;
@property (retain, nonatomic) IBOutlet CFCoverFlowView *coverFlowView1;
@property (retain, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic,retain) IBOutlet UIView *scrollView;
@end
