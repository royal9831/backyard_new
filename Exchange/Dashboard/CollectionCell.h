//
//  CollectionCell.h
//  Exchange
//
//  Created by Debayan Ghosh on 07/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionCell : UICollectionViewCell
@property (nonatomic,retain) IBOutlet UIImageView *backYardImageView;
@end
