//
//  ViewController.h
//  Exchange
//
//  Created by Debayan Ghosh on 23/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
@interface ViewController : UIViewController<CLLocationManagerDelegate>
{
    AppDelegate *appDel;
    int navigationTag;
    NSString *lat, *lon;
    NSString *locationAddress,*zip;
    int revealTag;
}
@property (retain,nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UITextField *phoneText;
@property (weak, nonatomic) IBOutlet UIButton *supplierBtn;
@property (weak, nonatomic) IBOutlet UIButton *demandBtn;
@property (weak, nonatomic) IBOutlet UIImageView *radioBtn1, *radioBtn2;
@property (nonatomic,retain) IBOutlet UIView *backgroundView;

@end

