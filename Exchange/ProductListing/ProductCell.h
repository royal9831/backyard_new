//
//  ProductCell.h
//  Exchange
//
//  Created by Debayan Ghosh on 14/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCell : UITableViewCell
@property (nonatomic,retain)IBOutlet UIImageView *imageview1, *imageview2, *imageview3, *imageview4, *imageview5, *arrowImage,*separatorImageView;
@property (nonatomic,retain)IBOutlet UILabel *nameAddressLabel;
@property (nonatomic,retain)IBOutlet UIButton *editBtn, *deleteBtn,*imageBtn1, *imageBtn2, *imageBtn3, *imageBtn4, *imageBtn5;
@end
