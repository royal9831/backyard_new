//
//  ProductListing.m
//  Exchange
//
//  Created by Debayan Ghosh on 14/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "ProductListing.h"
#import "TopNavigation.h"
#import "UIImageView+WebCache.h"
#import "UIImage+RoundedImage.h"
#import "UIImageEffects.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "SCLAlertView.h"
#import "ProductCell.h"
#import "ProductClass.h"
#import "ProductDetails.h"
#import "UploadProduct.h"
#import "ImageViewController.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

NSUserDefaults *userdefaults;
@interface ProductListing ()<UIViewControllerTransitioningDelegate,ProfileViewPageDetegale>

@end

@implementation ProductListing

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    if([ [ UIScreen mainScreen ] bounds ].size.height==480)
    {
        _backgroundView.frame=CGRectMake(0, 0, 320, 568);
        _table.frame=CGRectMake(0, 64, 320, 480-64);
        
    }
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    userdefaults = [NSUserDefaults standardUserDefaults];
    
    
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    SWRevealViewController *revealController = [self revealViewController];
    self.revealViewController.delegate = self;
    tap = [revealController tapGestureRecognizer];
    tap.delegate = self;
    tap.enabled=NO;
    [self.view addGestureRecognizer:tap];
    
    
    
    
    
    [self productListWebservice];
    
    
    
}

-(IBAction)uploadBtnAction:(id)sender
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"uploadproductpush"];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)productListWebservice
{
    
    
    
    productArray=[[NSMutableArray alloc]init];
    
 
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/MyProduct",appDel.hostUrl];
    
    NSLog(@"ID %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"]);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],@"user_id",
                               nil];

//    manager.responseSerializer = [AFJSONResponseSerializer serializer];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    [manager.requestSerializer setValue:@"calvinAndHobbessRock" forHTTPHeaderField:@"X-I do what I want"];
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         
         NSLog(@"JSON: %@", responseObject);
         
         if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
         {
             
             
             productArray=[[NSMutableArray alloc]init];
             for(int i=0;i<[[responseObject valueForKey:@"productList"] count];i++)
             {
                 
                 ProductClass *product=[[ProductClass alloc]init];
                 
                 product.productId=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"product_id"];
                 product.productName=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"product_name"];
                 product.productQuantity=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"quantity"];;
                 product.productDesc=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"product_desc"];
                 
                 
//                 if([[self checkNull:[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"images"]] isEqualToString:@""])
                 
                 if([[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"images"] == [NSNull null])
                 {
                     product.imageList=[[NSMutableArray alloc]init];
                 }
                 else
                 {
                     product.imageList=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"images"];
                 }
                 
                 product.productTime=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"date_added"];
                 product.productType=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"product_type"];
                 product.productAmount=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"amount"];
                 product.categoriesName=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"category"];
                 product.categoriesIds=[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"catid"];
                 
                 
                 product.username=[[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"name"];
                 product.useraddress=[[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"address"];
                 product.email=[[[[responseObject valueForKey:@"productList"] objectAtIndex:i] valueForKey:@"owner"] valueForKey:@"email"];
                 
                 
                 
                 [productArray addObject:product];
                 
                 
                 product=nil;
                 
                 
                 
             }
             
             [_table reloadData];
         }
         else
         {
             [appDel.alertview showInfo:self title:@"Products" subTitle:@"No product available" closeButtonTitle:@"OK" duration:0.0f];
         }
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         NSLog(@"Error: %@", error);
         
         [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
         
         
     }];
    

    
    
    
    
    
    
    
}
             
             
             
 -(NSString *)checkNull :(NSString *)str
 {
     NSString *InputString=str;
     if( (InputString == nil) ||(InputString ==(NSString *)[NSNull null])||([InputString isEqual:nil])||([InputString length] == 0)||[allTrim( InputString ) length] == 0||([InputString isEqualToString:@""])||([InputString isEqualToString:@"(NULL)"])||([InputString isEqualToString:@"<NULL>"])||([InputString isEqualToString:@"<null>"]||([InputString isEqualToString:@"(null)"])||([InputString isEqualToString:@""]))
        
        )
         return @"";
     else
         return str ;
     
 }


-(void)makeRoundImage:(UIImageView *)imageView
{
    imageView.image =    [UIImage getRoundedRectImageFromImage:imageView.image onReferenceView:imageView withCornerRadius:imageView.frame.size.width/2];
    imageView.layer.borderColor = [UIColor whiteColor].CGColor;
    imageView.layer.cornerRadius = imageView.frame.size.width/2;
    imageView.layer.borderWidth = 0;//2
    imageView.clipsToBounds = YES;
}


#pragma mark tableview delegates


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [productArray count];
}
-(UITableViewCell *)tableView: (UITableView *)tableView cellForRowAtIndexPath: (NSIndexPath *)indexPath{

    
    
    static  NSString *CellIdentifier = @"productcell";
    ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[ProductCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    ProductClass *product=[[ProductClass alloc]init];
    product=[productArray objectAtIndex:indexPath.row];
    
    NSString *name=product.username;
    NSString *address=product.useraddress;
    
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    
    NSLog(@"[product.imageList count] %d",[product.imageList count]);
    
    for(int i=0;i<[product.imageList count];i++)
    {
        if([[[product.imageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[product.imageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[product.imageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    
    if([product.imageList count]>0)
    {
    
    NSString *url2=[noncoverimages objectAtIndex:0];
    NSString *url3=[noncoverimages objectAtIndex:1];
    NSString *url4=[noncoverimages objectAtIndex:2];
    NSString *url5=[noncoverimages objectAtIndex:3];
    
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@  %@",name,address]];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0,name.length)];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(name.length+1,address.length+1)];
    [cell.nameAddressLabel setAttributedText:string];
    
    [cell.imageview1 sd_setImageWithURL:[NSURL URLWithString:url1]
                        placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    [cell.imageview2 sd_setImageWithURL:[NSURL URLWithString:url2]
                       placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    [cell.imageview3 sd_setImageWithURL:[NSURL URLWithString:url3]
                       placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    [cell.imageview4 sd_setImageWithURL:[NSURL URLWithString:url4]
                       placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    [cell.imageview5 sd_setImageWithURL:[NSURL URLWithString:url5]
                       placeholderImage:[UIImage imageNamed:@"noimages.png"]];
        
        
        
        
        
        
        
        
        [cell.imageBtn1 addTarget:self action:@selector(imageBtn1Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.imageBtn1.tag=indexPath.row;
        
        [cell.imageBtn2 addTarget:self action:@selector(imageBtn2Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.imageBtn2.tag=indexPath.row;
        
        [cell.imageBtn3 addTarget:self action:@selector(imageBtn3Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.imageBtn3.tag=indexPath.row;
        
        [cell.imageBtn4 addTarget:self action:@selector(imageBtn4Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.imageBtn4.tag=indexPath.row;
        
        [cell.imageBtn5 addTarget:self action:@selector(imageBtn5Action:) forControlEvents:UIControlEventTouchUpInside];
        cell.imageBtn5.tag=indexPath.row;
        
        
        
        
        
    
    }
    
    if(indexPath.row%2==0)
    {
        cell.backgroundColor=[UIColor colorWithRed:206.0/255 green:206.0/255 blue:206.0/255 alpha:1.0];
    }
    else
    {
        cell.backgroundColor=[UIColor colorWithRed:223.0/255 green:223.0/255 blue:223.0/255 alpha:1.0];
    }
    
    
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
      
        
        cell.imageview1.frame=CGRectMake(10, 52, 91, 91);
        cell.imageview2.frame=CGRectMake(110, 74, 54, 54);
        cell.imageview3.frame=CGRectMake(172, 74, 54, 54);
        cell.imageview4.frame=CGRectMake(236, 74, 54, 54);
        cell.imageview5.frame=CGRectMake(299, 74, 54, 54);
        cell.arrowImage.frame=CGRectMake(360, 91, 10, 20);
        
        cell.imageBtn1.frame=CGRectMake(10, 52, 91, 91);
        cell.imageBtn2.frame=CGRectMake(110, 74, 54, 54);
        cell.imageBtn3.frame=CGRectMake(172, 74, 54, 54);
        cell.imageBtn4.frame=CGRectMake(236, 74, 54, 54);
        cell.imageBtn5.frame=CGRectMake(299, 74, 54, 54);
        
        cell.nameAddressLabel.frame=CGRectMake(112, 30, 141, 21);
        
        cell.editBtn.frame=CGRectMake(112, 162, 75, 28);
        cell.deleteBtn.frame=CGRectMake(214, 162, 75, 28);
        
        cell.separatorImageView.frame=CGRectMake(0, 223-1, self.view.frame.size.width, 1);
        
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        
        
        
        
        
        
        
        
        
        cell.imageview1.frame=CGRectMake(11, 52, 100, 100);
        cell.imageview2.frame=CGRectMake(121, 74, 60, 60);
        cell.imageview3.frame=CGRectMake(190, 74, 60, 60);
        cell.imageview4.frame=CGRectMake(260, 74, 60, 60);
        cell.imageview5.frame=CGRectMake(330, 74, 60, 60);
        cell.arrowImage.frame=CGRectMake(398, 91, 11, 22);
        
        
        cell.imageBtn1.frame=CGRectMake(11, 52, 100, 100);
        cell.imageBtn2.frame=CGRectMake(121, 74, 60, 60);
        cell.imageBtn3.frame=CGRectMake(190, 74, 60, 60);
        cell.imageBtn4.frame=CGRectMake(260, 74, 60, 60);
        cell.imageBtn5.frame=CGRectMake(330, 74, 60, 60);
        
        cell.nameAddressLabel.frame=CGRectMake(125, 30, 279, 21);
        
        cell.editBtn.frame=CGRectMake(124, 172, 83, 28);
        cell.deleteBtn.frame=CGRectMake(236, 172, 83, 28);
        
        
        cell.separatorImageView.frame=CGRectMake(0, 223-1, self.view.frame.size.width, 1);
    
    }
    else
    {
        cell.imageview1.frame=CGRectMake(9, 39, 78, 78);
        cell.imageview2.frame=CGRectMake(94, 58, 46, 46);
        cell.imageview3.frame=CGRectMake(147, 58, 46, 46);
        cell.imageview4.frame=CGRectMake(201, 58, 46, 46);
        cell.imageview5.frame=CGRectMake(255, 58, 46, 46);
        cell.arrowImage.frame=CGRectMake(306, 69, 9, 17);
        
        
        cell.imageBtn1.frame=CGRectMake(9, 39, 78, 78);
        cell.imageBtn2.frame=CGRectMake(94, 58, 46, 46);
        cell.imageBtn3.frame=CGRectMake(147, 58, 46, 46);
        cell.imageBtn4.frame=CGRectMake(201, 58, 46, 46);
        cell.imageBtn5.frame=CGRectMake(255, 58, 46, 46);
        
        cell.nameAddressLabel.frame=CGRectMake(95, 20, 191, 21);
        
        cell.editBtn.frame=CGRectMake(96, 133, 64, 24);
        cell.deleteBtn.frame=CGRectMake(183, 133, 64, 24);
        
        cell.separatorImageView.frame=CGRectMake(0, 190-1, self.view.frame.size.width, 1);
    }
    
    
    
    
    
    
    [self makeRoundImage:cell.imageview1];
    [self makeRoundImage:cell.imageview2];
    [self makeRoundImage:cell.imageview3];
    [self makeRoundImage:cell.imageview4];
    [self makeRoundImage:cell.imageview5];
    
    
    
    
    
    
    
//    cell.firstCharLabel.frame = frame1;
//    
//    
//    
//    JobObject *job=[[JobObject alloc]init];
//    job=[jobArray objectAtIndex:indexPath.row];
//    
//    cell.nameLabel.text=job.title;
//    cell.locationLabel.text=job.location;
//    cell.firstCharLabel.text=[[job.title substringToIndex:1] uppercaseString];
//    
//    
//    
//
    
    
    
    [cell.editBtn addTarget:self action:@selector(editBtnAction:)forControlEvents:UIControlEventTouchUpInside];
    cell.editBtn.tag=indexPath.row;
    [cell.deleteBtn addTarget:self action:@selector(deleteBtnAction:)forControlEvents:UIControlEventTouchUpInside];
    cell.deleteBtn.tag=indexPath.row;
    
    
    
    

    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ProductClass *product=[[ProductClass alloc]init];
    product=[productArray objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"productdetailspush" sender:product];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        return 223.0;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        return 223.0;
    }
    else
    {
        return 190.0;
    }
}



-(IBAction)imageBtn1Action:(id)sender
{
    ProductClass *product=[[ProductClass alloc]init];
    product=[productArray objectAtIndex:[sender tag]];
    

    
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    
    NSLog(@"[product.imageList count] %d",[product.imageList count]);
    
    for(int i=0;i<[product.imageList count];i++)
    {
        if([[[product.imageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[product.imageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[product.imageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    controller.imageUrl=url1;
    
    
    [self.navigationController pushViewController:controller animated:NO];
    
    
}

-(IBAction)imageBtn2Action:(id)sender
{
    ProductClass *product=[[ProductClass alloc]init];
    product=[productArray objectAtIndex:[sender tag]];
    
    
    
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    
    NSLog(@"[product.imageList count] %d",[product.imageList count]);
    
    for(int i=0;i<[product.imageList count];i++)
    {
        if([[[product.imageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[product.imageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[product.imageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    controller.imageUrl=[noncoverimages objectAtIndex:0];
    
    
    [self.navigationController pushViewController:controller animated:NO];
    
    
}


-(IBAction)imageBtn3Action:(id)sender
{
    ProductClass *product=[[ProductClass alloc]init];
    product=[productArray objectAtIndex:[sender tag]];
    
    
    
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    
    NSLog(@"[product.imageList count] %d",[product.imageList count]);
    
    for(int i=0;i<[product.imageList count];i++)
    {
        if([[[product.imageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[product.imageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[product.imageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    controller.imageUrl=[noncoverimages objectAtIndex:1];
    
    
    [self.navigationController pushViewController:controller animated:NO];
    
    
}

-(IBAction)imageBtn4Action:(id)sender
{
    ProductClass *product=[[ProductClass alloc]init];
    product=[productArray objectAtIndex:[sender tag]];
    
    
    
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    
    NSLog(@"[product.imageList count] %d",[product.imageList count]);
    
    for(int i=0;i<[product.imageList count];i++)
    {
        if([[[product.imageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[product.imageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[product.imageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    controller.imageUrl=[noncoverimages objectAtIndex:2];
    
    
    [self.navigationController pushViewController:controller animated:NO];
    
    
}

-(IBAction)imageBtn5Action:(id)sender
{
    ProductClass *product=[[ProductClass alloc]init];
    product=[productArray objectAtIndex:[sender tag]];
    
    
    
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    
    NSLog(@"[product.imageList count] %d",[product.imageList count]);
    
    for(int i=0;i<[product.imageList count];i++)
    {
        if([[[product.imageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[product.imageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[product.imageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    controller.imageUrl=[noncoverimages objectAtIndex:3];
    
    
    [self.navigationController pushViewController:controller animated:NO];
    
    
}


-(IBAction)deleteBtnAction:(id)sender
{
    
    
    ProductClass *product=[[ProductClass alloc]init];
    product=[productArray objectAtIndex:[sender tag]];
    
    
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/DeleteProduct",appDel.hostUrl];
    
    NSLog(@"ID %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"]);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:product.productId,@"product_id",
                           nil];
    
 
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         
         NSLog(@"JSON: %@", responseObject);
         
         if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
         {
             
             [appDel.alertview showInfo:self title:@"Products" subTitle:@"Product deleted successfully" closeButtonTitle:@"OK" duration:0.0f];
             
             
             [productArray removeObjectAtIndex:[sender tag]];
             [_table reloadData];
         }
         else
         {
             [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Product deleted failed" closeButtonTitle:@"OK" duration:0.0f];
         }
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         [SVProgressHUD dismiss];
         self.view.userInteractionEnabled=YES;
         
         NSLog(@"Error: %@", error);
         
         [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
         
         
     }];

    
    
}




-(IBAction)editBtnAction:(id)sender
{
    
    ProductClass *product=[[ProductClass alloc]init];
    product=[productArray objectAtIndex:[sender tag]];
    
    UploadProduct *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"uploadproductpush"];
    controller.fromWhaereTag=2;
    controller.product=product;
    controller.delegate=(id)self;
    controller.uploadTag=1;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"productlistnav"]){
        
        TopNavigation *vc = (TopNavigation*)[segue destinationViewController];
        vc.titleString=@"PRODUCT LISTING";
        vc.fromWhere=3;
        vc.backgroundImageTag=1;
        vc.delegate = self;
        
        
    }
    else if([segue.identifier isEqualToString:@"productdetailspush"])
    {
        ProductDetails *vc = (ProductDetails*)[segue destinationViewController];
        
        ProductClass *product=[[ProductClass alloc]init];
        product=sender;
        vc.product=product;
        vc.delegate=(id)self;
        NSLog(@"product:%@",product.productName);
    }
    
}



-(void)objectWhenPop:(id)productObject
{
    NSLog(@"productObject: %@",productObject);
    
    for(int i=0;i<[productArray count];i++)
    {
        
        ProductClass *product1=[[ProductClass alloc]init];
        product1=[productArray objectAtIndex:i];
        
        if([product1.productId isEqualToString:[productObject valueForKey:@"product_id"]])
        {
            ProductClass *product=[[ProductClass alloc]init];
            
            product.productId=[productObject valueForKey:@"product_id"];
            product.productName=[productObject valueForKey:@"product_name"];
            product.productQuantity=[productObject valueForKey:@"quantity"];;
            product.productDesc=[productObject valueForKey:@"product_desc"];
            product.imageList=[productObject valueForKey:@"images"];
            product.productTime=[productObject valueForKey:@"date_added"];
            product.productType=[productObject valueForKey:@"product_type"];
            product.productAmount=[productObject valueForKey:@"amount"];
            product.categoriesName=[productObject valueForKey:@"category"];
            product.categoriesIds=[productObject valueForKey:@"catid"];
            
            
            product.username=[[productObject valueForKey:@"owner"] valueForKey:@"name"];
            product.useraddress=[[productObject valueForKey:@"owner"] valueForKey:@"address"];
            product.email=[[productObject valueForKey:@"owner"] valueForKey:@"email"];
            
            
            [productArray replaceObjectAtIndex:i withObject:product];
            
            product=nil;
            
            
        }
        
        
        product1=nil;
        
        
    }
    
    [_table reloadData];
}




-(void)objectWhenPop2:(ProductClass*)productObject
{
    NSLog(@"productObject: %@",productObject);
    
    
    for(int i=0;i<[productArray count];i++)
    {
        
        ProductClass *product1=[[ProductClass alloc]init];
        product1=[productArray objectAtIndex:i];
        
        if([product1.productId isEqualToString:productObject.productId])
        {
            
            
            [productArray replaceObjectAtIndex:i withObject:productObject];
            
            
            
        }
        
        
        product1=nil;
        
        
    }
    
    [_table reloadData];}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
