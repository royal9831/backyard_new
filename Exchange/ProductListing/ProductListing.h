//
//  ProductListing.h
//  Exchange
//
//  Created by Debayan Ghosh on 14/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SWRevealViewController.h"

@interface ProductListing : UIViewController<UIGestureRecognizerDelegate,SWRevealViewControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    AppDelegate *appDel;
    UITapGestureRecognizer *tap;
    NSMutableArray *productArray;
}
@property (nonatomic,retain) IBOutlet UITableView *table;
@property (nonatomic,retain) IBOutlet UIView *backgroundView;
@end
