//
//  BackyardCollectionCell.h
//  Exchange
//
//  Created by Debayan Ghosh on 26/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "CollectionCell.h"

@interface BackyardCollectionCell : CollectionCell
@property(nonatomic,retain) IBOutlet UIImageView *backYardImageView;
@property(nonatomic,retain) IBOutlet UIButton *backYardImageBtn;
@end
