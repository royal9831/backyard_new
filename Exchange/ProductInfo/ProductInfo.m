//
//  ProductInfo.m
//  Exchange
//
//  Created by Debayan Ghosh on 26/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "ProductInfo.h"
#import "MapSearch.h"
#import "TopNavigation.h"
#import<GoogleMaps/GoogleMaps.h>
//#import <GoogleMapsM4B/GoogleMaps.h>
#import "MapClass.h"
#import "SCLAlertView.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "UIImage+RoundedImage.h"
#import "CategoryObject.h"
#import "MapSearchClass.h"
#import "BackyardCollectionCell.h"
#import "ImageViewController.h"
#import "MapSearchListCell.h"
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))


#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
@interface ProductInfo ()<UIViewControllerTransitioningDelegate,ProfileViewPageDetegale>

@end

@implementation ProductInfo
@synthesize search;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    
    
    
    
    
    
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    
    if([userdefaults valueForKey:@"status"]!=nil && [[userdefaults valueForKey:@"status"] isEqualToString:@"1"])
    {
        
        
    }
    else
    {
//        _intiateBtn.hidden=YES;
    }
    
    
    
    _userNameText.returnKeyType = UIReturnKeyDone;
    _emailText.returnKeyType = UIReturnKeyDone;
    _phonenumberText.returnKeyType = UIReturnKeyDone;
    _productNameText.returnKeyType = UIReturnKeyDone;
    
    NSLog(@"SEARCH: %@",search);
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if([ [ UIScreen mainScreen ] bounds ].size.height==480)
    {
        _backgroundView.frame=CGRectMake(0, 0, 320, 568);
        _transparentView.frame=CGRectMake(0, 0, 320, 568);
        _scroll.frame=CGRectMake(0, 64, 320, 568-64);
        
        _scroll.contentSize=CGSizeMake(self.view.frame.size.width, _imageView1.frame.origin.y+_imageView1.frame.size.height+170);
    }
    
    
    
    _transparentView.hidden=YES;
    _popupView.hidden=YES;
    _popupView.layer.cornerRadius = 8.0;
    _popupView.clipsToBounds = YES;
    
    
    _label1.text=search.name;
    _label2.text=search.address;
    _label3.text=search.name;
    _label4.text=search.phone_no;
    _label5.text=search.email;
    
    
    
    
    
    
    NSString *productType=@"";
    if([search.product_type isEqualToString:@"0"])
    {
        productType=@"FREE";
        [_intiateBtn setTitle:@"INITIATE FREE" forState:UIControlStateNormal];
    }
    else if([search.product_type isEqualToString:@"1"])
    {
        productType=@"SWAP";
        [_intiateBtn setTitle:@"INITIATE SWAP" forState:UIControlStateNormal];
    }
    else if([search.product_type isEqualToString:@"2"])
    {
        productType=@"SALE";
        [_intiateBtn setTitle:@"INITIATE SALE" forState:UIControlStateNormal];
    }
    
    
    _label6.text=search.product_name;
    _label7.text=search.product_category;
    _label8.text=productType;
    _label9.text=search.productQuantity;
    _label10.text=search.product_desc;
    
    
    
    
    
    
    
    [_profileImageView sd_setImageWithURL:[NSURL URLWithString:search.profileimage]
                         placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    
    
    
    
    _profileImageView.image =    [UIImage getRoundedRectImageFromImage:_profileImageView.image onReferenceView:_profileImageView withCornerRadius:_profileImageView.frame.size.width/2];
    _profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    _profileImageView.layer.cornerRadius = _profileImageView.frame.size.width/2;
    _profileImageView.layer.borderWidth = 2;
    _profileImageView.clipsToBounds = YES;
    
    
    
    
    
    
    

    
    
    
    
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    for(int i=0;i<[search.productImageList count];i++)
    {
        if([[[search.productImageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    [self makeRoundImage:_productCoverImageView];
    [self makeRoundImage:_imageView1];
    [self makeRoundImage:_imageView2];
    [self makeRoundImage:_imageView3];
    [self makeRoundImage:_imageView4];
    
    NSString *url2=[noncoverimages objectAtIndex:0];
    NSString *url3=[noncoverimages objectAtIndex:1];
    NSString *url4=[noncoverimages objectAtIndex:2];
    NSString *url5=[noncoverimages objectAtIndex:3];
    
    
    [_productCoverImageView sd_setImageWithURL:[NSURL URLWithString:url1]
                   placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    [_imageView1 sd_setImageWithURL:[NSURL URLWithString:url2]
                   placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    [_imageView2 sd_setImageWithURL:[NSURL URLWithString:url3]
                   placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    [_imageView3 sd_setImageWithURL:[NSURL URLWithString:url4]
                   placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    [_imageView4 sd_setImageWithURL:[NSURL URLWithString:url5]
                   placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    
    
    


}


-(IBAction)profileImageBtn:(id)sender
{
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    controller.imageUrl=search.profileimage;
    [self.navigationController pushViewController:controller animated:NO];
}

-(IBAction)productCoverImageBtn:(id)sender
{
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    
    
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    for(int i=0;i<[search.productImageList count];i++)
    {
        if([[[search.productImageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    
    controller.imageUrl=url1;
    [self.navigationController pushViewController:controller animated:NO];
}

-(IBAction)imageBtn1:(id)sender
{
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    for(int i=0;i<[search.productImageList count];i++)
    {
        if([[[search.productImageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    NSString *url2=[noncoverimages objectAtIndex:0];
    controller.imageUrl=url2;
    [self.navigationController pushViewController:controller animated:NO];
   
}

-(IBAction)imageBtn2:(id)sender
{
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    for(int i=0;i<[search.productImageList count];i++)
    {
        if([[[search.productImageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    NSString *url3=[noncoverimages objectAtIndex:1];
    controller.imageUrl=url3;
    [self.navigationController pushViewController:controller animated:NO];
}

-(IBAction)imageBtn3:(id)sender
{
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    for(int i=0;i<[search.productImageList count];i++)
    {
        if([[[search.productImageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    NSString *url4=[noncoverimages objectAtIndex:2];
    controller.imageUrl=url4;
    [self.navigationController pushViewController:controller animated:NO];
}

-(IBAction)imageBtn4:(id)sender
{
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    
    NSMutableArray *noncoverimages=[[NSMutableArray alloc]init];
    NSString *url1;
    for(int i=0;i<[search.productImageList count];i++)
    {
        if([[[search.productImageList objectAtIndex:i] valueForKey:@"is_cover"] isEqualToString:@"1"])
        {
            url1=[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"];
        }
        else
        {
            [noncoverimages addObject:[[search.productImageList objectAtIndex:i] valueForKey:@"img_name"]];
        }
    }
    
    NSString *url5=[noncoverimages objectAtIndex:3];
    controller.imageUrl=url5;
    [self.navigationController pushViewController:controller animated:NO];
}



-(void)makeRoundImage:(UIImageView *)imageView
{
    imageView.image =    [UIImage getRoundedRectImageFromImage:imageView.image onReferenceView:imageView withCornerRadius:imageView.frame.size.width/2];
    imageView.layer.borderColor = [UIColor whiteColor].CGColor;
    imageView.layer.cornerRadius = imageView.frame.size.width/2;
    imageView.layer.borderWidth = 2;
    imageView.clipsToBounds = YES;
}




-(IBAction)initiateSaleBtnAction:(id)sender
{
    if([search.product_type isEqualToString:@"1"])
    {
        
        
        NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
        
        if([userdefaults valueForKey:@"status"]!=nil && [[userdefaults valueForKey:@"status"] isEqualToString:@"1"])
        {
            _userNameText.text=[[userdefaults valueForKey:@"userdetails"] valueForKey:@"name"];
            _emailText.text=[[userdefaults valueForKey:@"userdetails"] valueForKey:@"email"];
            _phonenumberText.text=[[userdefaults valueForKey:@"userdetails"] valueForKey:@"phone_no"];
            
            _userNameText.userInteractionEnabled=NO;
            _emailText.userInteractionEnabled=NO;
//            _phonenumberText.userInteractionEnabled=NO;
        }
        else
        {
            _userNameText.hidden=YES;
            _userNameImageview.hidden=YES;
            
        }
        
        
        
        
        
        
        
        
        
//        _userNameText.hidden=YES;
//        _userNameImageview.hidden=YES;
        
        _transparentView.hidden=NO;
        _popupView.hidden=NO;
        
        [_popupView setFrame:CGRectMake(_popupView.frame.origin.x, self.view.frame.size.height, _popupView.frame.size.width, _popupView.frame.size.height)];
        
        [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
            
            [_popupView setFrame:CGRectMake(_popupView.frame.origin.x, (self.view.frame.size.height-_popupView.frame.size.height)/2, _popupView.frame.size.width, _popupView.frame.size.height)];
            
        } completion:^(BOOL finished) {
            
            
        }];
    }
    else
    {
        
        NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
        if([userdefaults valueForKey:@"status"]!=nil && [[userdefaults valueForKey:@"status"] isEqualToString:@"1"])
        {
            
        [self initiateWebService];
        
        }
        else
        {
            
            _userNameText.hidden=YES;
            _userNameImageview.hidden=YES;
        
        _transparentView.hidden=NO;
        _popupView.hidden=NO;
        
        [_popupView setFrame:CGRectMake(_popupView.frame.origin.x, self.view.frame.size.height, _popupView.frame.size.width, _popupView.frame.size.height)];
        
        [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
            
            [_popupView setFrame:CGRectMake(_popupView.frame.origin.x, (self.view.frame.size.height-_popupView.frame.size.height)/2, _popupView.frame.size.width, _popupView.frame.size.height)];
            
        } completion:^(BOOL finished) {
            
            
        }];
        }
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_userNameText resignFirstResponder];
    [_emailText resignFirstResponder];
    [_phonenumberText resignFirstResponder];
    [_productNameText resignFirstResponder];
    
    
    
}

-(IBAction)submitBtnAction:(id)sender
{
    [_userNameText resignFirstResponder];
    [_emailText resignFirstResponder];
    [_phonenumberText resignFirstResponder];
    [_productNameText resignFirstResponder];
    
    
    
    if([_emailText.text isEqualToString:@""] || [_phonenumberText.text isEqualToString:@""] )
    {
        [appDel.alertview showInfo:self title:@"Initiate Sale" subTitle:@"Please provide email id and phone number" closeButtonTitle:@"OK" duration:0.0f];
    }
    else
    {
        [self initiateWebService];
    }
}



-(IBAction)crossBtnAction:(id)sender
{
    [_userNameText resignFirstResponder];
    [_emailText resignFirstResponder];
    [_phonenumberText resignFirstResponder];
    [_productNameText resignFirstResponder];
    
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
        
        [_popupView setFrame:CGRectMake(_popupView.frame.origin.x, self.view.frame.size.height, _popupView.frame.size.width, _popupView.frame.size.height)];
        
    } completion:^(BOOL finished) {
        
        _transparentView.hidden=YES;
        _popupView.hidden=YES;
    }];
}

-(void)initiateWebService
{
    
    int temp=[[NSDate date] timeIntervalSince1970];
    NSString *timestamp=[NSString stringWithFormat:@"%d",temp];
    NSLog(@"timestamp %@",timestamp);
    
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/Initiate",appDel.hostUrl];
    
    NSLog(@"ID %@",[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"]);
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *param;
    if([defaults valueForKey:@"status"]!=nil && [[defaults valueForKey:@"status"] isEqualToString:@"1"])
    {
        
        NSLog(@"OWNER ID: %@",search.productOwnerId);
        
        param = [NSDictionary dictionaryWithObjectsAndKeys:[[defaults valueForKey:@"userdetails"] valueForKey:@"user_id"],@"RequestList[user_id]",search.product_id,@"RequestList[product_id]",_productNameText.text,@"RequestList[swap_name]",@"",@"RequestList[swap_quantity]",_emailText.text,@"RequestList[email]",search.productOwnerId,@"RequestList[owner_id]",timestamp,@"RequestList[request_date]",
                               nil];
        
    }
    else
    {
        param = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"RequestList[user_id]",search.product_id,@"RequestList[product_id]",_productNameText.text,@"RequestList[swap_name]",@"",@"RequestList[swap_quantity]",_emailText.text,@"User[email]",_phonenumberText.text,@"Profile[phone_no]",appDel.token,@"Profile[device_token]",@"",@"Profile[lat]",@"",@"Profile[long]",search.productOwnerId,@"RequestList[owner_id]",timestamp,@"RequestList[request_date]",
                               nil];
    }

    
    
    
    
    
    
    
    
//    RequestList[email], Profile[long], Profile[lat], Profile[device_token], Profile[phone_no]
    
    
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        
        NSLog(@"JSON: %@", responseObject);
        
        if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
        {
            
            
            
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            alert.shouldDismissOnTapOutside = NO;
            alert.showAnimationType=SlideInToCenter;
            alert.hideAnimationType=SlideOutToCenter;
            [alert alertIsDismissed:^{
                NSLog(@"SCLAlertView dismissed!");
                
                [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1 options:UIViewAnimationOptionCurveLinear animations:^{
                    
                    [_popupView setFrame:CGRectMake(_popupView.frame.origin.x, self.view.frame.size.height, _popupView.frame.size.width, _popupView.frame.size.height)];
                    
                } completion:^(BOOL finished) {
                    
                    _transparentView.hidden=YES;
                    _popupView.hidden=YES;
                }];

                
                
            }];
            
            [alert showInfo:self title:@"Initiate" subTitle:@"Initiate successfully completed" closeButtonTitle:@"OK" duration:0.0f];
            
        }
        else
        {
            [appDel.alertview showInfo:self title:@"Initiate" subTitle:@"This user's email address already exists. Please provide a new email id" closeButtonTitle:@"OK" duration:0.0f];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        NSLog(@"Error: %@", error);
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
        
        
    }];

    
    
    
    
    
    
    
    

}

#pragma mark collectionview delegates



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    
    return [search.backyardImageList count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    BackyardCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        cell.backYardImageView.frame=CGRectMake(0, 0, 63.0, 63.0);
        cell.backYardImageBtn.frame=CGRectMake(0, 0, 63.0, 63.0);
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        cell.backYardImageView.frame=CGRectMake(0, 0, 70.0, 70.0);
        cell.backYardImageBtn.frame=CGRectMake(0, 0, 70.0, 70.0);
        
    }
    else
    {
        cell.backYardImageView.frame=CGRectMake(0, 0, 54.0, 54.0);
        cell.backYardImageBtn.frame=CGRectMake(0, 0, 54.0, 54.0);
    }
    
    
    NSString *imageString=[[search.backyardImageList objectAtIndex:indexPath.row] valueForKey:@"backyard_image"];//@"http://www.newyorker.com/wp-content/uploads/2012/07/chris-nolan.jpg";
    
    [cell.backYardImageView sd_setImageWithURL:[NSURL URLWithString:imageString]
                              placeholderImage:[UIImage imageNamed:@"noimages.png"]];
    
    [self makeRoundImage:cell.backYardImageView];
    
    
    
    
    
    
    [cell.backYardImageBtn addTarget:self action:@selector(backyardImageBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.backYardImageBtn.tag=indexPath.row;
    
    
    
    
    return cell;
}




- (IBAction)backyardImageBtnAction:(id)sender {
    
    ImageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"viewimage"];
    
    NSString *imageString=[[search.backyardImageList objectAtIndex:[sender tag]] valueForKey:@"backyard_image"];
    controller.imageUrl=imageString;
    [self.navigationController pushViewController:controller animated:NO];
    
    
}





- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGSize returnSize = CGSizeZero;
    
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        
//        returnSize = CGSizeMake(383.0, 164.0);
        returnSize = CGSizeMake(54.0, 54.0);
        
        // iPad
    } else {
        // iPhone
        
        
        
        if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
        {
            returnSize = CGSizeMake(63.0, 63.0);
        }
        else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
        {
            returnSize = CGSizeMake(70.0, 70.0);
        }
        else
        {
            returnSize = CGSizeMake(54.0, 54.0);
        }
    }
    
    return returnSize;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        return 28.0;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        return 30.0;
    }
    else
    {
        return 24.0;
    }
    
    
}

- (IBAction)callBtnAction:(id)sender {
    
    [self callMethod:search.phone_no];
    
    
}
- (IBAction)emailBtnAction:(id)sender {
    
    [self emailMethod:search.email];
}

-(void)callMethod:(NSString *)str
{
    NSString *phNo = str;
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        alert.shouldDismissOnTapOutside = YES;
        
        [alert alertIsDismissed:^{
            NSLog(@"SCLAlertView dismissed!");
        }];
        
        [alert showInfo:self title:@"INFO" subTitle:@"Call facility is not available!!!" closeButtonTitle:@"OK" duration:0.0f];
    }
}

-(void)emailMethod:(NSString *)str
{
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
        {
            [self displayComposerSheet:str];
            //            [self launchMailAppOnDevice];
        }
        else
        {
            [self launchMailAppOnDevice];
        }
    }
    else
    {
        [self launchMailAppOnDevice];
    }
}

-(void)displayComposerSheet:(NSString *)str
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    [picker setSubject:@"Backyard"];
    NSArray *toRecipients = [NSArray arrayWithObject:str];
    [picker setToRecipients:toRecipients];
    NSString *emailBody =@"";
    [picker setMessageBody:emailBody isHTML:NO];
    [picker.navigationBar setTintColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0]];
    //	[self presentModalViewController:picker animated:YES];
    [self.navigationController presentViewController:picker animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //	message.text = @"Result: canceled";
            break;
        case MFMailComposeResultSaved:
            //message.text = @"Result: saved";
            break;
        case MFMailComposeResultSent:
            //message.text = @"Result: sent";
            break;
        case MFMailComposeResultFailed:
            //message.text = @"Result: failed";
            break;
        default:
            //message.text = @"Result: not sent";
            break;
    }
    //	[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)launchMailAppOnDevice
{
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    alert.shouldDismissOnTapOutside = YES;
    
    [alert alertIsDismissed:^{
        NSLog(@"SCLAlertView dismissed!");
    }];
    
    [alert showInfo:self title:@"INFO" subTitle:@"Please configure your mail" closeButtonTitle:@"OK" duration:0.0f];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"productinfonav"]){
        
        TopNavigation *vc = (TopNavigation*)[segue destinationViewController];
        vc.titleString=@"PRODUCT DETAILS";
        vc.fromWhere=4;
        vc.delegate = self;
    }
}



#pragma mark Textfield delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    CGRect viewFrame = self.view.frame;
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        viewFrame.origin.y = -80;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        viewFrame.origin.y = -90;
    }
    else
    {
        viewFrame.origin.y = -140;
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.5];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    CGRect viewFrame = self.view.frame;
    
    if(IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    {
        viewFrame.origin.y = +0;
    }
    else if(IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    {
        viewFrame.origin.y = +0;
    }
    else
    {
        viewFrame.origin.y = +0;
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.5];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
