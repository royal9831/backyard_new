//
//  ProductInfo.h
//  Exchange
//
//  Created by Debayan Ghosh on 26/08/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapSearchClass.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "AppDelegate.h"
@interface ProductInfo : UIViewController<MFMailComposeViewControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    AppDelegate *appDel;
}
@property (nonatomic,retain) MapSearchClass *search;
@property (nonatomic,retain) IBOutlet UIView *backgroundView;
@property (nonatomic,retain) IBOutlet UILabel *label1,*label2,*label3,*label4,*label5;
@property (nonatomic,retain) IBOutlet UILabel *label6,*label7,*label8,*label9,*label10;

@property (nonatomic,retain) IBOutlet UIImageView *profileImageView, *userNameImageview;
@property (nonatomic,retain) IBOutlet UIImageView *productCoverImageView;
@property (nonatomic,retain) IBOutlet UIScrollView *scroll;
@property (nonatomic,retain) IBOutlet UIImageView *imageView1,*imageView2,*imageView3,*imageView4;
@property (nonatomic,retain) IBOutlet UICollectionView *collection;
@property (nonatomic,retain) IBOutlet UIView *transparentView,*popupView;
@property (nonatomic,retain) IBOutlet UITextField *userNameText, *emailText, *phonenumberText, *productNameText;

@property (nonatomic,retain) IBOutlet UIButton *intiateBtn, *profileImageBtn, *productCoverBtn, *imageBtn1, *imageBtn2, *imageBtn3, *imageBtn4;
@end
