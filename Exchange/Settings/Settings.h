//
//  Settings.h
//  Exchange
//
//  Created by Debayan Ghosh on 31/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "AppDelegate.h"
@interface Settings : UIViewController<UIGestureRecognizerDelegate,SWRevealViewControllerDelegate,UIPickerViewDataSource, UIPickerViewDelegate>
{
    UITapGestureRecognizer *tap;
    NSMutableArray *preferenceArray,*categoryArray, *quantityArray, *locationArray;
    AppDelegate *appDel;
    int pickerTag;
}
@property (nonatomic,retain) IBOutlet UILabel *sliderLabel;
@property (nonatomic,retain)IBOutlet UISlider * customSlider;
@property (weak, nonatomic) IBOutlet UIButton *onBtn;
@property (weak, nonatomic) IBOutlet UIButton *offBtn;
@property (weak, nonatomic) IBOutlet UIImageView *radioBtn1, *radioBtn2;
@property (nonatomic, strong) IBOutlet UITextField *preferenceTextField, *categoryText, *quantityText, *locationText;
@end
