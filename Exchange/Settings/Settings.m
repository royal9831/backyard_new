//
//  Settings.m
//  Exchange
//
//  Created by Debayan Ghosh on 31/07/15.
//  Copyright (c) 2015 Manas Garai. All rights reserved.
//

#import "Settings.h"
#import "TopNavigation.h"
#import "SCLAlertView.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
UIPickerView *pickerView;
NSString *prefId,*catId,*quantityId,*locaId,*radius,*notification;
@interface Settings ()<UIViewControllerTransitioningDelegate,ProfileViewPageDetegale>

@end

@implementation Settings

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    NSLog(@"INFO: %@",[[defaults1 valueForKey:@"usersettings"] valueForKey:@"category_id"]);
    
    appDel=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    SWRevealViewController *revealController = [self revealViewController];
    self.revealViewController.delegate = self;
    tap = [revealController tapGestureRecognizer];
    tap.delegate = self;
    tap.enabled=NO;
    [self.view addGestureRecognizer:tap];
    

    
    _onBtn.selected=NO;
    _offBtn.selected=NO;
    _onBtn.userInteractionEnabled=TRUE;
    _offBtn.userInteractionEnabled=TRUE;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([[[defaults valueForKey:@"usersettings"] valueForKey:@"status"] isEqualToString:@"1"])
    {
        _radioBtn1.image=[UIImage imageNamed:@"radio-button-on-1.png"];
        _radioBtn2.image=[UIImage imageNamed:@"radio-button-off-1.png"];
    }
    else
    {
        _radioBtn1.image=[UIImage imageNamed:@"radio-button-off-1.png"];
        _radioBtn2.image=[UIImage imageNamed:@"radio-button-on-1.png"];
    }
    
    _preferenceTextField.text=[[defaults valueForKey:@"usersettings"] valueForKey:@"preference_id"];
    _categoryText.text=[[defaults valueForKey:@"usersettings"] valueForKey:@"category_id"];
    _quantityText.text=[[defaults valueForKey:@"usersettings"] valueForKey:@"quantity"];
//    _locationText.text=[[defaults valueForKey:@"usersettings"] valueForKey:@"preference_id"];
    
    
    radius=[[defaults valueForKey:@"usersettings"] valueForKey:@"radius"];
    notification=[[defaults valueForKey:@"usersettings"] valueForKey:@"status"];
    prefId=[[defaults valueForKey:@"usersettings"] valueForKey:@"preference_id"];
    catId=[[defaults valueForKey:@"usersettings"] valueForKey:@"category_id"];
    quantityId=[[defaults valueForKey:@"usersettings"] valueForKey:@"quantity"];
    
    
//    [_preferenceTextField becomeFirstResponder];
    
    
    
    
    preferenceArray=[[NSMutableArray alloc]initWithObjects:@"5",@"10",@"15", nil];
    categoryArray=[[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3", nil];
    quantityArray=[[NSMutableArray alloc]initWithObjects:@"1",@"3",@"5", nil];
    locationArray=[[NSMutableArray alloc]initWithObjects:@"Loc1",@"Loc2",@"Loc3", nil];
    
    
    pickerTag=1;
    
    pickerView= [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    pickerView.showsSelectionIndicator = YES;
    pickerView.dataSource = self;
    pickerView.delegate = self;
    
    // set change the inputView (default is keyboard) to UIPickerView
    _preferenceTextField.inputView = pickerView;
    _categoryText.inputView = pickerView;
    _quantityText.inputView = pickerView;
    _locationText.inputView = pickerView;
    
    // add a toolbar with Cancel & Done button
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleBlackOpaque;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTouched:)];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTouched:)];
    
    // the middle button is to make the Done button align to right
    [toolBar setItems:[NSArray arrayWithObjects:cancelButton, [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneButton, nil]];
    _preferenceTextField.inputAccessoryView = toolBar;
    _categoryText.inputAccessoryView = toolBar;
    _quantityText.inputAccessoryView = toolBar;
    _locationText.inputAccessoryView = toolBar;
    
    
    
    
    
    
    
    
    
    [self setSlider];
}

//-(void)viewWillAppear:(BOOL)animated
//{
//    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
//    NSLog(@"INFO: %@",[[defaults1 valueForKey:@"usersettings"] valueForKey:@"category_id"]);
//}


-(void)setSlider
{
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    float radius=[[[defaults valueForKey:@"usersettings"] valueForKey:@"radius"] floatValue];
    
    
//    _customSlider = [[UISlider alloc] initWithFrame:frame];
    
    // in case the parent view draws with a custom color or gradient, use a transparent color
    _customSlider.backgroundColor = [UIColor clearColor];
    UIImage *stetchLeftTrack = [[UIImage imageNamed:@"slider2.png"]
                                stretchableImageWithLeftCapWidth:5.0 topCapHeight:0.0];
    UIImage *stetchRightTrack = [[UIImage imageNamed:@"slider1.png"]
                                 stretchableImageWithLeftCapWidth:5.0 topCapHeight:0.0];
    [_customSlider setThumbImage: [UIImage imageNamed:@"SettingsBubble1.png"] forState:UIControlStateNormal];   //sliderBtn.png
    [_customSlider setMinimumTrackImage:stetchLeftTrack forState:UIControlStateNormal];
    [_customSlider setMaximumTrackImage:stetchRightTrack forState:UIControlStateNormal];
    _customSlider.minimumValue = 1.0;
    _customSlider.maximumValue = 50.0;
    _customSlider.continuous = YES;
    _customSlider.value = radius;
    
    
    CGRect trackRect = [_customSlider trackRectForBounds:_customSlider.bounds];
    CGRect thumbRect = [_customSlider thumbRectForBounds:_customSlider.bounds
                                               trackRect:trackRect
                                                   value:_customSlider.value];
    
    
    _sliderLabel.center = CGPointMake(thumbRect.origin.x + _customSlider.frame.origin.x + thumbRect.size.width/2,  _customSlider.frame.origin.y+40);
    
    _sliderLabel.text=[NSString stringWithFormat:@"%dM",(int)radius];
    
//    float unit=_customSlider.frame.size.width/50.0;
//    _sliderLabel.frame=CGRectMake(unit*radius, _sliderLabel.frame.origin.y, _sliderLabel.frame.size.width, _sliderLabel.frame.size.height);
    
    [_customSlider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
//    [self.view addSubView:customSlider];
}

-(IBAction)sliderAction:(id)sender
{
    float sliderValue=_customSlider.value;
    NSLog(@"_customSlider %d",(int)sliderValue);
    
    radius=[NSString stringWithFormat:@"%d",(int)sliderValue];
    
    
    
    CGRect trackRect = [_customSlider trackRectForBounds:_customSlider.bounds];
    CGRect thumbRect = [_customSlider thumbRectForBounds:_customSlider.bounds
                                             trackRect:trackRect
                                                 value:_customSlider.value];
    
    
    _sliderLabel.center = CGPointMake(thumbRect.origin.x + _customSlider.frame.origin.x + thumbRect.size.width/2,  _customSlider.frame.origin.y+40);
    
    _sliderLabel.text=[NSString stringWithFormat:@"%dM",(int)sliderValue];
    
//    float unit=_customSlider.frame.size.width/50.0;
//    _sliderLabel.frame=CGRectMake(unit*[radius intValue], _sliderLabel.frame.origin.y, _sliderLabel.frame.size.width, _sliderLabel.frame.size.height);
    
    
}

-(IBAction)saveBtnAction:(id)sender
{
    NSLog(@"Radius: %@\nNotification: %@\nPreference:%@\nCategory: %@\nQuantity:%@",radius,notification,prefId,catId,quantityId);
    
    
    [self settingsWebservice];
    
}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        tap.enabled=NO;
        //        table.userInteractionEnabled=TRUE;
    } else {
        tap.enabled=YES;
        //        table.userInteractionEnabled=FALSE;
        
    }
    
    
    
}




- (IBAction)onBtnAction:(id)sender {
    
    _offBtn.selected=NO;
    _onBtn.selected=YES;
    _offBtn.userInteractionEnabled=TRUE;
    _onBtn.userInteractionEnabled=FALSE;
    _radioBtn1.image=[UIImage imageNamed:@"radio-button-on-1.png"];
    _radioBtn2.image=[UIImage imageNamed:@"radio-button-off-1.png"];
    notification=@"1";
    
    
}
- (IBAction)offBtnAction:(id)sender {
    
    _offBtn.selected=YES;
    _onBtn.selected=NO;
    _offBtn.userInteractionEnabled=FALSE;
    _onBtn.userInteractionEnabled=TRUE;
    _radioBtn1.image=[UIImage imageNamed:@"radio-button-off-1.png"];
    _radioBtn2.image=[UIImage imageNamed:@"radio-button-on-1.png"];
    notification=@"0";
    
}





- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"settingsnav"]){
        
        TopNavigation *vc = (TopNavigation*)[segue destinationViewController];
        vc.titleString=@"SETTINGS";
        vc.fromWhere=3;
        vc.delegate = self;
    }
    
}


- (void)cancelTouched:(UIBarButtonItem *)sender
{
    // hide the picker view
    [_preferenceTextField resignFirstResponder];
    [_categoryText resignFirstResponder];
    [_quantityText resignFirstResponder];
    [_locationText resignFirstResponder];
}

- (void)doneTouched:(UIBarButtonItem *)sender
{
    // hide the picker view
    [_preferenceTextField resignFirstResponder];
    [_categoryText resignFirstResponder];
    [_quantityText resignFirstResponder];
    [_locationText resignFirstResponder];
    
    // perform some action
}




#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if(pickerTag==1)
    {
        return [preferenceArray count];
    }
    else if (pickerTag==2)
    {
        return [categoryArray count];
    }
    else if (pickerTag==3)
    {
        return [quantityArray count];
    }
    else
    {
        return [locationArray count];
    }
    
    
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *item;
    
    if(pickerTag==1)
    {
        item = [preferenceArray objectAtIndex:row];
    }
    else if (pickerTag==2)
    {
        item = [categoryArray objectAtIndex:row];
    }
    else if (pickerTag==3)
    {
        item = [quantityArray objectAtIndex:row];
    }
    else
    {
        item = [locationArray objectAtIndex:row];
    }
    
    return item;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // perform some action
    
    
    if(pickerTag==1)
    {
        _preferenceTextField.text=[preferenceArray objectAtIndex:row];
//        prefId=[preferenceArray objectAtIndex:row];
    }
    else if (pickerTag==2)
    {
        _categoryText.text=[categoryArray objectAtIndex:row];
        catId=[categoryArray objectAtIndex:row];
    }
    else if (pickerTag==3)
    {
        _quantityText.text=[quantityArray objectAtIndex:row];
        quantityId=[quantityArray objectAtIndex:row];
    }
    else
    {
        _locationText.text=[locationArray objectAtIndex:row];
    }
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == _preferenceTextField)
    {
        pickerTag=1;
    }
    else if(textField == _categoryText)
    {
        pickerTag=2;
    }
    else if(textField == _quantityText)
    {
        pickerTag=3;
    }
    else
    {
        pickerTag=4;
    }
    
    [pickerView reloadAllComponents];
}



#pragma mark SettingsService

-(void)settingsWebservice
{
    
    
    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:6.0];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD showWithStatus:@"Loading.."];
    self.view.userInteractionEnabled=NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [NSString stringWithFormat:@"%@?r=api/ChangeSetting",appDel.hostUrl];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:                           prefId,@"NotificationPreference[preference_id]",notification,@"NotificationPreference[status]",catId,@"NotificationPreference[category_id]",quantityId,@"NotificationPreference[quantity]",radius,@"NotificationPreference[radius]",
//                           nil];
    
    
    
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:                           prefId,@"NotificationPreference[preference_id]",notification,@"NotificationPreference[status]",@"",@"NotificationPreference[category_id]",@"",@"NotificationPreference[quantity]",radius,@"NotificationPreference[radius]",
                           nil];
    
    
    [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        //mindtree
        
        NSLog(@"JSON: %@", responseObject);
        
        if([[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"status"]] isEqualToString:@"1"])
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[responseObject valueForKey:@"notification"] forKey:@"usersettings"];
            [defaults synchronize];
            
            [appDel.alertview showInfo:self title:@"Settings" subTitle:@"Settings has been changed" closeButtonTitle:@"OK" duration:0.0f];
            
        }
        else
        {
            [appDel.alertview showInfo:self title:@"Settings" subTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]] closeButtonTitle:@"OK" duration:0.0f];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled=YES;
        
        NSLog(@"Error: %@", error);
        
        [appDel.alertview showInfo:self title:@"INFO" subTitle:@"Network error" closeButtonTitle:@"OK" duration:0.0f];
        
        
    }];
    
    
    
    
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
